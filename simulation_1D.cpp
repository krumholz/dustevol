// Implementation of the simulation_1D class
#include <cmath>
#include <iostream>
#include "ppm.H"
#include "simulation_1D.H"
#include "utils.H"

using namespace std;

// Constructor
simulation_1D::simulation_1D(dustdisk_1D &_dd):
  nx(_dd.grd->nx), dx(_dd.grd->dx), dd(&_dd) {

  // Allocate workspace
  ppm.resize(3*nx);
  tau.resize(nx);
  flux.resize(nx+1);
}


// Routine to do the diffusion step
void simulation_1D::advance_diffusion(const double dt) {

  // Step 1: create vectors for the RHS and the data with ghost zones
  dvec rho_c_grow(nx+2);
  dvec rhs(nx+2);
  for (vecsz i=0; i<nx; i++) rho_c_grow[i+1] = dd->rho_c[i];
  rho_c_grow[0] = rho_c_grow[1];
  rho_c_grow[nx+1] = rho_c_grow[nx];

  // Step 2: fill RHS vector using diffusive fluxes at old time
#if defined(_OPENMP)
#  pragma omp parallel for
#endif
  for (vecsz i=1; i<=nx; i++) {
    rhs[i] = rho_c_grow[i] + (1.0 - Theta) * dt *
      ( rho_c_grow[i+1] + rho_c_grow[i-1] - 2*rho_c_grow[i] ) / SQR(dx);
  }

  // Step 3: solve the matrix equation describing diffusion using the
  // Thomas algorithm; note that this is particularly simple because
  // the above- and below-diagonal elements of the matrix are all just
  // -Theta * dt/dx^2, and the on-diagonal elements are all 1 +
  // 2*Theta * dt/dx^2; this algorithm is unfortunately inherently serial
  if (Theta != 0.0) {
    dvec a(nx+2, -Theta * dt/SQR(dx));
    dvec c(nx+2, -Theta * dt/SQR(dx));
    dvec b(nx+2, 1.0 + 2 * Theta * dt/SQR(dx));
    b[0] = b[nx+1] = Theta * dt/SQR(dx);
    for (vecsz i=1; i<nx+2; i++) {
      double w = a[i] / b[i-1];
      b[i] = b[i] - w*c[i-1];
      rhs[i] = rhs[i] - w*rhs[i-1];
    }
    rho_c_grow[nx+1] = rhs[nx+1] / b[nx+1];
    for (vecsz i=nx; i>0; i--)
      rho_c_grow[i] = (rhs[i] - c[i]*rho_c_grow[i+1]) / b[i];
  } else {
    for (vecsz i=1; i<nx+2; i++) rho_c_grow[i] = rhs[i];
  }
  
  // Step 4: copy back to original holder
  for (vecsz i=0; i<nx; i++) dd->rho_c[i] = rho_c_grow[i+1];
}


// Routine to compute PPM coefficients
void simulation_1D::set_ppm_coef() {

  // Do reconstruction for interior cells  
#if defined(_OPENMP)
#  pragma omp parallel for
#endif
  for (vecsz i=1; i<nx-1; i++) {
    const double *x = dd->grd->x_c.data() + i - 1;
    const double xb[3] = {
      (SQR(dd->grd->x_e[i]) - SQR(dd->grd->x_e[i-1])) / (2.0*dx),
      (SQR(dd->grd->x_e[i+1]) - SQR(dd->grd->x_e[i])) / (2.0*dx),
      (SQR(dd->grd->x_e[i+2]) - SQR(dd->grd->x_e[i+1])) / (2.0*dx)
    };
    const double x2b[3] = {
      (CUBE(dd->grd->x_e[i]) - CUBE(dd->grd->x_e[i-1])) / (3.0*dx),
      (CUBE(dd->grd->x_e[i+1]) - CUBE(dd->grd->x_e[i])) / (3.0*dx),
      (CUBE(dd->grd->x_e[i+2]) - CUBE(dd->grd->x_e[i+1])) / (3.0*dx)
    };
    const double *q = dd->rho_c.data() + i - 1;
    double *coef = ppm.data() + 3*i;
    get_ppm_coef(x, xb, x2b, q, coef);
  }

  // Handle first radial zone using piecewise linear reconstruction,
  // requiring continuity at x_{1/2}. Limit the slope to ensure that
  // x_{-1/2} is non-negative.
  double xh = dd->grd->x_e[1];
  double rhoh = ppm[3] + ppm[4] * xh +
    ppm[5] * xh*xh;
  ppm[2] = 0.0;
  ppm[1] = (dd->rho_c[0] - rhoh) / (dd->grd->x_c[0] - xh);
  ppm[0] = dd->rho_c[0] - ppm[1] * dd->grd->x_c[0];
  if (ppm[0] + ppm[1]*dd->grd->x_e[0] < 0) {
    ppm[1] = 2*dd->rho_c[0] / dd->grd->dx;
    ppm[0] = dd->rho_c[0] - ppm[1] * dd->grd->x_c[0];
  }

  // Handle last radial zone similar to first radial zone
  xh = dd->grd->x_e[nx-1];
  vecsz p0 = 3*(nx-1);
  vecsz p1 = 3*(nx-2);
  rhoh = ppm[p1] + ppm[p1+1] * xh +
    ppm[p1+2] * xh*xh;
  ppm[p0+2] = 0.0;
  ppm[p0+1] = (dd->rho_c[nx-1] - rhoh) / (dd->grd->x_c[nx-1] - xh);
  ppm[p0] = dd->rho_c[nx-1] - ppm[p0+1] * dd->grd->x_c[nx-1];
  if (ppm[p0] + ppm[p0+1]*dd->grd->x_e[nx] < 0) {
    ppm[p0+1] = -2*dd->rho_c[nx-1] / dd->grd->dx;
    ppm[p0] = dd->rho_c[nx-1] - ppm[p0+1] * dd->grd->x_c[nx-1];
  }
}


// Routine to compute optical depths
void simulation_1D::set_tau() {
  
  // Get optical depths; doing this in parallel requires chunking the
  // operation, since the optical depth is a cumulative quantitiy
  //#if defined(_OPENMP)
#if defined(_OPENMP)
  dvec tau_sum;
#  pragma omp parallel
  {
    vecsz nth = omp_get_num_threads();
    vecsz myth = omp_get_thread_num();
    vecsz chunksize = nx / nth;
    vecsz start = chunksize * myth;
    vecsz end = chunksize * (myth+1);
    if (end > nx) end = nx;

    // Set sum accumulator size based on number of threads; only one
    // processor should to this
    if (nth > 1 && myth == 0) tau_sum.resize(nth-1);
#pragma omp barrier

    // Get optical depth through first cell in each chunk
    tau[start] = dd->rho_c[start] * dd->grd->dx;

    // Sum local chunks
    for (vecsz i=start+1; i<end; i++) {
      tau[i] = tau[i-1] + dd->rho_c[i] * dd->grd->dx;
    }
    
    // Set a barrier to force all threads to finish before combining
    // sums from different processors
#pragma omp barrier

    // Processor 0 sums the end points of the chunks
#pragma omp single
    {
      if (nth > 1) {
	tau_sum[0] = tau[chunksize-1];
	for (vecsz i=1; i<nth-1; i++) {
	  tau_sum[i] = tau_sum[i-1] + tau[(i+1)*chunksize-1];
	}
      }
    }
    
    // Now each processor adds the accumulated sum to the cells in its
    // chunk; this is done in parallel
    if (myth != 0) {
      for (vecsz i=start; i<end; i++) tau[i] += tau_sum[myth-1];
    }
  }

#else

  // The much simpler serial algorithm
  tau[0] = tau[0] = dd->rho_c[0] * dd->grd->dx;
  for (vecsz i=1; i<nx; i++) {
    tau[i] = tau[i-1] + dd->rho_c[i] * dd->grd->dx;
  }
#endif
}


// Routine to compute fluxes
void simulation_1D::set_flux(const double dt) {

  // Loop over cell outer faces
#if defined(_OPENMP)
#  pragma omp parallel for
#endif
  for (vecsz i=0; i<nx; i++) {

    // Get velocity at cell face
    double v = dd->chi * exp(-tau[i]);

    // Get position of contact discontinuity in donor cell; note that,
    // in this problem, v can only be positive
    double x_e = dd->grd->x_e[i+1];
    double x_con = x_e - v*dt;

    // Store mass per unit time crossing cell edge
    flux[i+1] = (ppm[3*i] * (x_e - x_con) +
		 ppm[3*i+1] * (SQR(x_e) - SQR(x_con)) / 2.0 +
		 ppm[3*i+2] * (CUBE(x_e) - CUBE(x_con)) / 3.0) / dt;
  }
}




// Routine to do the advection step
double simulation_1D::advance_advection(const double dt) {

  // Step 1: save starting state
  dvec rho_c_0(dd->rho_c);

  // Step 2: compute PPM coefficients, optical depths, and fluxes
  set_ppm_coef();
  set_tau();
  set_flux(dt);
  double vmax = dd->chi * exp(-tau[0]);

  // Step 3: do a full time step update
#if defined(_OPENMP)
  bool bad_step = false;
#  pragma omp parallel for reduction(||:bad_step)
#endif
  for (vecsz i=0; i<nx; i++) {
    dd->rho_c[i] += dt * (flux[i] - flux[i+1]) / dd->grd->dx;
    if (dd->rho_c[i] <= 0.0) {
#if defined(_OPENMP)
      bad_step = true;
#else
      return -1.0;
#endif
    }
  }
#if defined(_OPENMP)
  if (bad_step) return -1.0;
#endif

  // Step 4: re-calculate fluxes for new state
  set_ppm_coef();
  set_tau();
  set_flux(dt);
  vmax = vmax < dd->chi * exp(-tau[0]) ? dd->chi * exp(-tau[0]) : vmax;
  
  // Step 5: set final state
#if defined(_OPENMP)
#  pragma omp parallel for
#endif
  for (vecsz i=0; i<nx; i++) {
    dd->rho_c[i] = 0.5 * rho_c_0[i] + 0.5 * dd->rho_c[i]
      + 0.5 * dt * (flux[i] - flux[i+1]) / dd->grd->dx;
    if (dd->rho_c[i] <= 0.0) {
#if defined(_OPENMP)
      bad_step = true;
#else
      return -1.0;
#endif
    }
  }
#if defined (_OPENMP)
  if (bad_step) return -1.0;
#endif

  // Step 6: return maximum velocity  
  return vmax;
}


// Advance routine
double simulation_1D::advance(const double dt) {

  // Step 1: save state in case we need to bail out
  dvec rho_c_save = dd->rho_c;

  // Step 1: do diffusion advance for half step
  advance_diffusion(dt/2.0);

  // Step 2: do advection advance
  double vmax = advance_advection(dt);
  if (vmax < 0.0) {
    dd->rho_c = rho_c_save;
    return -1.0;
  }

  // Step 3: do second diffusion advance for half step
  advance_diffusion(dt/2.0);
  
  // Step 4: estimate new time step
  return cfl * dd->grd->dx/vmax;
}


// Run routine
double simulation_1D::run(const double tstop, const dvec tsave,
			  ddvec &hist, ivec &shifthist,
			  const double dt0) {
  
  // Initialize status variables
  bool save_hist = false;
  bool last_step = false;
  
  // If initial output time is zero, copy initial state to history
  hist.resize(0);
  vecsz histptr = 0;
  if (tsave.size() > 0) {
    if (tsave[0] == 0.0) {
      hist.push_back(dd->rho_c);
      shifthist.push_back(0);
      histptr++;
    }
  }

  // Set initial time step
  double dt;
  if (dt0 > 0.0) dt = dt0;
  else {

    // Estimate time step by calculating initial max velocity
    set_ppm_coef();        // Get PPM coefficients
    set_tau();             // Do ray trace to get optical depths
    double vmax = dd->chi * exp(-tau[0]);    // Max velocity
    dt = cfl * dd->grd->dx/vmax;

  }

  // Begin advancing the calculation
  while (!last_step && stepctr < max_step) {

    // Adjust time step to avoid overshooting next output or final
    // time, and flag if we're at the end of the simulation or will be
    // saving the state this time step
    if (t+dt >= tstop) last_step = true;
    if (tsave.size() > histptr)
      if (t+dt > tsave[histptr]) save_hist = true;
    if (last_step) dt = tstop - t;
    else if (save_hist) dt = tsave[histptr] - t;

    // Try an advance, retrying if we get negative density; if time
    // step becomes too small due to repeated negative densities, exit
    // the main loop
    double dt_new;
    bool step_succeeded = false;
    while (!step_succeeded) {

      // Try advance    
      dt_new = advance(dt);
      if (dt_new > 0) step_succeeded = true;
      else step_succeeded = false;
      
      // Reduce time step if advance fails; bail out if time step gets
      // too small
      if (!step_succeeded) {
	dt /= 2.0;
	save_hist = false;
	last_step = false;
	if (dt < dt_min) break;
      }
    }

    // If using a floor, shift cells below floor off the grid
    if (rho_floor > 0.0) dd->do_shift(rho_floor);

    // Update time, making sure we hit output times exactly
    if (last_step) t = tstop;
    else if (save_hist) t = tsave[histptr];
    else t += dt;
    
    // Save history if we are doing so this step
    if (save_hist)  {
      hist.push_back(dd->rho_c);
      shifthist.push_back(dd->shift);
      histptr++;
      save_hist = false;
    }
    
    // Update status
    if ((verb == MED_VERBOSITY && stepctr % 10000 == 0) ||
	(verb == HIGH_VERBOSITY)) {
      cout << "simulation_1d::run step = " << stepctr
	   << ", advanced to t = " << t
	   << "; completed dt = " << dt
	   << ", next dt = " << dt_new
	   << endl;
    }      

    // Update time step and step counter
    dt = dt_new;
    stepctr++;

  }

  // Print final status
  if (last_step && verb != NO_VERBOSITY)
    cout << "simulation_1D::run done in " << stepctr
	 << " steps, t = " << t << endl;
  else if (!last_step && verb != NO_VERBOSITY)
    cout << "simulation_1D::run aborted after " << stepctr
	 << " steps, t = " << t
	 << ", dt = " << dt << endl;
  
  // Return final time step
  return dt;
}
