// Implementation of the simulation class

#include <cmath>
#include <iostream>
#include "simulation.H"
#include "ppm.H"

using namespace std;

typedef Eigen::Triplet<double> trip;
typedef vector< Eigen::Triplet<double> > tripVec;

//////////////////////////////////////////////////////////////////////
// Initialization
//////////////////////////////////////////////////////////////////////

// Constructor
simulation::simulation(dustdisk &_dd) :
  nr(_dd.nr), nmu(_dd.nmu), na(_dd.na), dd(&_dd) {

  // Allocate memory
  init_vectors();

  // Build the dust diffusion matrices
  build_dust_mat();

  // Set up the matrix solver
  solver.analyzePattern(dmat[0]);

  // Set a mild tolerance on the solver we'll use for Anderson
  // acceleration; since this is just a convergence accelerator,
  // there's no need to pay for a highly-accurate solution
  lscg.setTolerance(1.0e-6);
}

// Method to set vectors to the correct sizes
void simulation::init_vectors() {
  ppm_coef_r.resize(3*nr*nmu*na);
  ppm_coef_mu.resize(3*nr*nmu*na);
  tau_re.resize((nr+1)*nmu);
  tau_mue.resize(nr*(nmu+1));
  flux_re.resize((nr+1)*nmu*na);
  flux_mue.resize(nr*(nmu+1)*na);
  rhs_old.resize(nr*nmu*na);
  rhs.resize(nr*nmu*na);
  hist.resize(nr*nmu*na, anderson_ord+2);
  resid.resize(nr*nmu*na+1, anderson_ord+1);
  dmat.resize(na);
  mat.resize(na);
  for (vecsz k=0; k<na; k++) {
    vecsz nrred = nr-2 > 0 ? nr-2 : 1;
    vecsz nmured = nmu-2 > 0 ? nmu-2 : 1;
    dmat[k].resize(nr*nmu, nr*nmu);
    dmat[k].reserve(nr*nmu + 2*nrred*nmu + 2*nr*nmured);
    mat[k].resize(nr*nmu, nr*nmu);
    mat[k].reserve(nr*nmu + 2*nrred*nmu + 2*nr*nmured);
  }
  identity.resize(nr*nmu, nr*nmu);
  identity.setIdentity();
}

//////////////////////////////////////////////////////////////////////
// Method to build the dust diffusion matrices
//////////////////////////////////////////////////////////////////////
void simulation::build_dust_mat() {

  // Create of triplets we will use to build diffusion matrix; note
  // that we do not bother threading this code because the 
  for (vecsz k=0; k<na; k++) {

    // Allocate space for triplets with 5-point stencil
    tripVec t;
    t.reserve(5*nr*nmu);

    // Loop over cells
    for (vecsz j=0; j<nmu; j++) {
      for (vecsz i=0; i<nr; i++) {

	// Cell index shorthands
	const vecsz ic = IC(i,j,nr);
	const vecsz icrm = ICRM(i,j,nr);
	const vecsz icrp = ICRP(i,j,nr);
	const vecsz icmum = ICMUM(i,j,nr);
	const vecsz icmup = ICMUP(i,j,nr);
	const vecsz ierm = IERM(i,j,nr);
	const vecsz ierp = IERP(i,j,nr);
	const vecsz iemum = IEMUM(i,j,nr);
	const vecsz iemup = IEMUP(i,j,nr);

	// Compute stopping time and diffusion coefficients on cell
	// edges; note that diffusion coefficient is set to zero at
	// domain edges
	double Derm, Derp, Demum, Demup;
	if (i != 0 && !r_flux_off) {
	  double Tserm = dd->Omega_re[ierm] * dd->rho_d * dd->a[k] /
	    (dd->rhog_re[ierm] * dd->cs_re[ierm] + constants::min);
	  Derm = dd->alpha * SQR(dd->cs_re[ierm]) /
	    (dd->Omega_re[ierm] * (1.0+SQR(Tserm)));
	} else {
	  Derm = 0.0;
	}
	if (i != nr-1 && !r_flux_off) {
	  double Tserp = dd->Omega_re[ierp] * dd->rho_d * dd->a[k] /
	    (dd->rhog_re[ierp] * dd->cs_re[ierp] + constants::min);
	  Derp = dd->alpha * SQR(dd->cs_re[ierp]) /
	    (dd->Omega_re[ierp] * (1.0+SQR(Tserp)));
	} else {
	  Derp = 0.0;
	}
	if (j != 0) {
	  double Tsemum = dd->Omega_mue[iemum] * dd->rho_d * dd->a[k] /
	    (dd->rhog_mue[iemum] * dd->cs_mue[iemum] + constants::min);
	  Demum = dd->alpha * SQR(dd->cs_mue[iemum]) /
	    (dd->Omega_mue[iemum] * (1.0+SQR(Tsemum)));
	} else {
	  Demum = 0.0;
	}
	if (j != nmu-1) {
	  double Tsemup = dd->Omega_mue[iemup] * dd->rho_d * dd->a[k] /
	    (dd->rhog_mue[iemup] * dd->cs_mue[iemup] + constants::min);
	  Demup = dd->alpha * SQR(dd->cs_mue[iemup]) /
	    (dd->Omega_mue[iemup] * (1.0+SQR(Tsemup)));
	} else {
	  Demup = 0.0;
	}

	// Add elements of sparse matrix representing finite
	// difference stencil; note that if we add multiple triplets
	// representing the same cell (as we will here), they will be
	// summed when the matrix is built

	// Derivative in +r direction
	if (i != nr-1) {
	  double fac = Derp * dd->grd->dxdr_e[i+1] * dd->grd->A_r[ierp] /
	    (dd->grd->V[ic] * dd->grd->dx);
	  t.push_back(trip(ic, icrp,
			   -fac*dd->rhog_re[ierp] /
			   (dd->rhog_c[icrp] + constants::min)));
	  t.push_back(trip(ic, ic,
			   fac*dd->rhog_re[ierp] /
			   (dd->rhog_c[ic] + constants::min)));
	}

	// Derivative in -r direction
	if (i != 0) {
	  double fac = Derm * dd->grd->dxdr_e[i] * dd->grd->A_r[ierm] /
	    (dd->grd->V[ic] * dd->grd->dx);
	  t.push_back(trip(ic, icrm,
			   -fac*dd->rhog_re[ierm] /
			   (dd->rhog_c[icrm] + constants::min)));
	  t.push_back(trip(ic, ic,
			   fac*dd->rhog_re[ierm] /
			   (dd->rhog_c[ic]+ + constants::min)));
	}

	// Derivative in +mu direction
	if (j != nmu-1) {
	  double fac = Demup * dd->grd->A_mu[iemup] *
	    sqrt(1-SQR(dd->grd->mu_e[j+1])) /
	    (dd->grd->V[ic] * dd->grd->dmu * dd->grd->r_c[i]);
	  t.push_back(trip(ic, icmup,
			   -fac*dd->rhog_mue[iemup] /
			   (dd->rhog_c[icmup] + constants::min)));
	  t.push_back(trip(ic, ic,
			   fac*dd->rhog_mue[iemup] /
			   (dd->rhog_c[ic] + constants::min)));
	}

	// Derivative in -mu direction
	if (j != 0) {
	  double fac = Demum * dd->grd->A_mu[iemum] *
	    sqrt(1-SQR(dd->grd->mu_e[j])) /
	    (dd->grd->V[ic] * dd->grd->dmu * dd->grd->r_c[i]);
	  t.push_back(trip(ic, icmum,
			   -fac*dd->rhog_mue[iemum] /
			   (dd->rhog_c[icmum] + constants::min)));
	  t.push_back(trip(ic, ic,
			   fac*dd->rhog_mue[iemum] /
			   (dd->rhog_c[ic] + constants::min)));
	}	
      }
    }

    // Initialize sparse matrix with triplet list
    dmat[k].setFromTriplets(t.begin(), t.end());
  }
}


//////////////////////////////////////////////////////////////////////
// Method to calculate PPM coefficients on the grid
//////////////////////////////////////////////////////////////////////
void simulation::set_ppm_coef() {

  // Loop over species
  for (vecsz k=0; k<na; k++) {

    // Loop over angle, and parallelize. Each thread works on one or
    // more radial skewers, which are adjacent in memory.
#if defined(_OPENMP)
#  pragma omp parallel for
#endif
    for (vecsz j=0; j<nmu; j++) {

      // Get radial coefficients for interior cells
      for (vecsz i=1; i<nr-1; i++) {
	get_ppm_coef(dd->grd->x_c.data()+i-1,
		     dd->grd->xbar.data()+i-1,
		     dd->grd->x2bar.data()+i-1,
		     dd->rho_c.data()+DC(i-1,j,k,nr,nmu),
		     ppm_coef_r.data()+PC(i,j,k,nr,nmu));
      }

      // Get angular coefficients for interior cells
      if (j != 0 && j != nmu-1) {
	for (vecsz i=0; i<nr; i++) {
	  double rho_c[3];
	  rho_c[0] = dd->rho_c[DC(i,j-1,k,nr,nmu)];
	  rho_c[1] = dd->rho_c[DC(i,j,k,nr,nmu)];
	  rho_c[2] = dd->rho_c[DC(i,j+1,k,nr,nmu)];
	  get_ppm_coef(dd->grd->mu_c.data()+j-1,
		       dd->grd->mubar.data()+j-1,
		       dd->grd->mu2bar.data()+j-1,
		       rho_c,
		       ppm_coef_mu.data()+PC(i,j,k,nr,nmu));
	}
      }

      // Radial coefficients for the first radial zone (zones with
      // i=0): use piecewise linear reconstruction, requiring that rho
      // be continuous at x_{1/2}. However, limit the slope to ensure
      // that rho remains non-negative at x_{-1/2}. This doesn't
      // matter for the purposes of advection, since we never advect
      // through the inner boundary, but it is important to avoid
      // negative optical depths.
      double xh = dd->grd->x_e[1];
      vecsz d0 = DC(0,j,k,nr,nmu);
      vecsz p0 = PC(0,j,k,nr,nmu);
      vecsz p1 = PC(1,j,k,nr,nmu);
      double rhoh = ppm_coef_r[p1] +
	ppm_coef_r[p1+1] * xh +
	ppm_coef_r[p1+2] * xh*xh;
      ppm_coef_r[p0+2] = 0.0;
      ppm_coef_r[p0+1] =
	(dd->rho_c[d0] - rhoh) / (dd->grd->x_c[0] - xh);
      ppm_coef_r[p0] = dd->rho_c[d0] -
	ppm_coef_r[p0+1] * dd->grd->xbar[0];
      if (ppm_coef_r[p0] + ppm_coef_r[p0+1]*dd->grd->x_e[0] < 0) {
	ppm_coef_r[p0+1] = 2*dd->rho_c[d0] / dd->grd->dx;
	ppm_coef_r[p0] = dd->rho_c[d0] -
	  ppm_coef_r[p0+1] * dd->grd->xbar[0];
      }

      // Radial coefficients for the last radial zones (i=nr-1), which
      // we treat in the same way as the first radial zones.
      xh = dd->grd->x_e[nr-1];
      d0 = DC(nr-1,j,k,nr,nmu);
      p0 = PC(nr-1,j,k,nr,nmu);
      p1 = PC(nr-2,j,k,nr,nmu);
      rhoh = ppm_coef_r[p1] +
	ppm_coef_r[p1+1] * xh +
	ppm_coef_r[p1+2] * xh*xh;
      ppm_coef_r[p0+2] = 0.0;
      ppm_coef_r[p0+1] =
	(dd->rho_c[d0] - rhoh) / (dd->grd->x_c[nr-1] - xh);
      ppm_coef_r[p0] = dd->rho_c[d0] -
	ppm_coef_r[p0+1] * dd->grd->xbar[nr-1];
      if (ppm_coef_r[p0] + ppm_coef_r[p0+1]*dd->grd->x_e[nr] < 0) {
	ppm_coef_r[p0+1] = -2*dd->rho_c[d0] / dd->grd->dx;
	ppm_coef_r[p0] = dd->rho_c[d0] -
	  ppm_coef_r[p0+1] * dd->grd->xbar[nr-1];
      }

      // Angular coefficients for the first angular zones (j=0); for
      // these cells we use reflecting boundary conditions
      if (j == 0) {
	for (vecsz i=0; i<nr; i++) {
	  double mu_c[3], mubar[3], mu2bar[3], rho_c[3];
	  mu_c[0] = -dd->grd->mu_c[0];
	  mu_c[1] = dd->grd->mu_c[0];
	  mu_c[2] = dd->grd->mu_c[1];
	  mubar[0] = -dd->grd->mubar[0];
	  mubar[1] = dd->grd->mubar[0];
	  mubar[2] = dd->grd->mubar[1];
	  mu2bar[0] = mu2bar[1] = dd->grd->mu2bar[0];
	  mu2bar[2] = dd->grd->mu2bar[1];
	  rho_c[0] = dd->rho_c[DC(i,j,k,nr,nmu)];
	  rho_c[1] = dd->rho_c[DC(i,j,k,nr,nmu)];
	  rho_c[2] = dd->rho_c[DC(i,j+1,k,nr,nmu)];
	  get_ppm_coef(mu_c, mubar, mu2bar, rho_c,
		       ppm_coef_mu.data()+PC(i,j,k,nr,nmu));
	}
      }
    }
  }

  // Angular coefficient for the last angular zones (j = nmu-1). Note
  // that we put this in a separate loop after the main parallel for
  // region, because we need the PPM coefficients at j = nmu-2 before
  // we can figure out the ones at j = nmu-1. Also note that this
  // loop, unlike all the others, is in the i direction instead of the
  // j direction. Thus it is possible that it may not provide much
  // speedup due to memory contention. Since this is just a single
  // angular pencil, though, even if it runs at near serial-speed the
  // hit should be minimal.
  for (vecsz k=0; k<na; k++) {
#if defined(_OPENMP)
#  pragma omp parallel for
#endif
    // Use piecewise linear reconstruction for the high mu zone
    for (vecsz i=0; i<nr; i++) {
      vecsz d0 = DC(i,nmu-1,k,nr,nmu);
      vecsz p0 = PC(i,nmu-1,k,nr,nmu);
      vecsz p1 = PC(i,nmu-2,k,nr,nmu);
      double muh = dd->grd->mu_e[nmu-1];
      double rhoh = ppm_coef_mu[p1] +
	ppm_coef_mu[p1+1] * muh +
	ppm_coef_mu[p1+2] * muh*muh;
      ppm_coef_mu[p0+2] = 0.0;
      ppm_coef_mu[p0+1] = (dd->rho_c[d0] - rhoh) /
	(dd->grd->mubar[nmu-1] - muh);
      ppm_coef_mu[p0] = dd->rho_c[d0] -
	ppm_coef_mu[p0+1] * dd->grd->mubar[nmu-1];
    }
  }
}


//////////////////////////////////////////////////////////////////////
// Method to calculate optical depths
//////////////////////////////////////////////////////////////////////
void simulation::set_tau() {

  // Loop over radial rays, parallelizing in angle
#if defined(_OPENMP)
#  pragma omp parallel for
#endif
  for (vecsz j=0; j<nmu; j++) {

    // Optical depth starts at zero
    tau_re[IERM(0,j,nr)] = 0.0;

    // Walk along ray, adding up optical depth
    for (vecsz i=0; i<nr; i++) {

      // Convenience variables
      vecsz tidx = IERM(i,j,nr);
      double r0 = dd->grd->r_e[i];
      double r1 = dd->grd->r_e[i+1];
      
      // Loop over grain species, integrating over the
      // PPM-reconstructed density as appropriate for the type
      // of radial coordinate we're using.
      double tau_cell = 0.0;
      for (vecsz k=0; k<na; k++) {
	vecsz pidx = PC(i,j,k,nr,nmu);
	double tau_spec = dd->kappa_d[k] *
	  (ppm_coef_r[pidx] * (r1-r0) +
	   ppm_coef_r[pidx+1] * dd->grd->intx[i] +
	   ppm_coef_r[pidx+2] * dd->grd->intx2[i]);
	// Avoid negative tau due to roundoff error in summing PPM
	// coefficients; these tend to be near the machine error
	// limit and thus harmless, but we floor them to zero anyway
	if (tau_spec < 0.0) tau_spec = 0.0;
	tau_cell += tau_spec;
      }
      tau_re[tidx+1] = tau_re[tidx] + tau_cell;
    }
  }

  // Now handle mu edges. We handle these by getting the optical depth
  // to cell centers, then averaging adjacent cells in the mu
  // direction. We do averaging instead of taking a PPM
  // reconstruction because we need the exact interface value, and
  // thus we need continuity at the interface. Finally, note that we
  // can only do this for interior mu edges. We just leave the
  // exterior edges as zero, because we will never actually make use
  // of them due to our boundary condition.
#if defined(_OPENMP)
#  pragma omp parallel for
#endif
  for (vecsz j=1; j<nmu; j++) {

    // Walk in radial direction
    for (vecsz i=0; i<nr; i++) {

      // Convenience variables
      vecsz tL = IERM(i,j-1,nr);
      vecsz tR = IERP(i,j,nr);
      vecsz tc = IEMUM(i,j,nr);
      double r0 = dd->grd->r_e[i];
      double r1 = dd->grd->r_c[i];
      double tau_L = tau_re[tL];
      double tau_R = tau_re[tR];

      // Loop over grain sizes, adding optical depth to cell centers
      for (vecsz k=0; k<na; k++) {
	vecsz pL = PC(i,j-1,k,nr,nmu);
	vecsz pR = PC(i,j,k,nr,nmu);
	tau_L += dd->kappa_d[k] *
	  (ppm_coef_r[pL] * (r1-r0) +
	   ppm_coef_r[pL+1] * dd->grd->intx_c[i] +
	   ppm_coef_r[pL+2] * dd->grd->intx2_c[i]);
	tau_R += dd->kappa_d[k] *
	  (ppm_coef_r[pR] * (r1-r0) +
	   ppm_coef_r[pR+1] * dd->grd->intx_c[i] +
	   ppm_coef_r[pR+2] * dd->grd->intx2_c[i]);
      }
      
      // Average left and right sides to get value at edge
      tau_mue[tc] = 0.5*(tau_L+tau_R);
    }
  }
}


//////////////////////////////////////////////////////////////////////
// Method to calculate fluxes
//////////////////////////////////////////////////////////////////////
double simulation::set_flux(const double dt, const flux_mode mode) {

  // Loop over species
  double dx_over_v = constants::max;
  for (vecsz k=0; k<na; k++) {

    // Calculate fluxes, parallelizing over rays
    double dx_over_v_r = constants::max;
#if defined(_OPENMP)
#  pragma omp parallel for reduction(min:dx_over_v_r)
#endif
    for (vecsz j=0; j<nmu; j++) {
      for (vecsz i=0; i<=nr; i++) {

	// Convenience pointers; these give the indices of the edge
	// we're working on and the cell centers on either side of
	// it, in arrays with various numbers of components
	// ie = index of this cell edge in edge-centered arrays that
	//      are the same for all dust species
	// de = index of this cell edge in edge-centered arrays that
	//      depend on dust size bin (e.g., fluxes)
	// icm = index of cell center in the minus direction
	//       relative to this edge, in an array that doesn't
	//       have a dust index
	// dcm = index of cell center in the minus direction
	//       relative to this edge, in an array that does
	//       have a dust index
	// icp, dcp = same as icm and dcm, but for the cell center
	//            in the plus direction relative to this edge
	// pcm, pcp = indices of cell centers in the minus and plus
	//            direction relative to this edge, in arrays of
	//            PPM coefficients that contain 3 coefficients
	//            per cell per dust species
	vecsz ie = IERM(i,j,nr);
	vecsz de = DERM(i,j,k,nr,nmu);
	vecsz icm = IC(i-1,j,nr);
	vecsz dcm = DC(i-1,j,k,nr,nmu);
	vecsz icp = IC(i,j,nr);
	vecsz dcp = DC(i,j,k,nr,nmu);
	vecsz pcm = PC(i-1,j,k,nr,nmu);
	vecsz pcp = PC(i,j,k,nr,nmu);

	// Set flux to zero to start
	flux_re[de] = 0.0;
	if (r_flux_off) continue;

	// Get local stopping time and Keplerian speed; cap stopping
	// time at 1, since the assumptions used to derive the
	// terminal velocity break down if Ts is large, and allowing
	// large Ts can lead to unrealistic velocities
	double Ts =
	  min(dd->rho_d * dd->a[k] /
	      (dd->rhog_re[ie] * dd->cs_re[ie] + constants::min)
	      * dd->Omega_re[ie], 1.0);
	double varpi = dd->grd->r_e[i] * sqrt(1.0 - SQR(dd->grd->mu_c[j]));
	double vK = sqrt(constants::G*dd->M/varpi);

	// Compute advective flux if desired
	if (mode == ADV_ONLY || mode == ADV_DIFF) {

	  // Get local beta
	  double beta = dd->beta_0[k] * exp(-tau_re[ie]);

	  // Get terminal velocities in cylindrical coordinates
	  double v_varpi =
	    (dd->vg_re[ie]/Ts +
	     (beta * sqrt(1.0-SQR(dd->grd->mu_c[j])) - dd->eta_re[ie]) * vK) /
	    (Ts + 1.0/Ts);
	  double v_z = Ts * (beta-1.0) * dd->Omega_re[ie] *
	    dd->grd->r_e[i] * dd->grd->mu_c[j];
	    
	  // Get radial velocity
	  double vr = v_varpi * sqrt(1.0-SQR(dd->grd->mu_c[j])) +
	    v_z * dd->grd->mu_c[j];

	  // Leave the flux as zero if (1) we are at the edge of the
	  // domain and the velocity is into the domain or (2) the
	  // upwind density is below the floor
	  bool zero_flux = false;
	  if (i == 0 && vr >= 0.0) zero_flux = true;
	  else if (i == nr && vr <= 0.0) zero_flux = true;
	  else if (vr > 0.0 && dd->rho_c[dcm] < dd->rho_floor)
	    zero_flux = true;
	  else if (vr < 0.0 && dd->rho_c[dcp] < dd->rho_floor)
	    zero_flux = true;
	  if (!zero_flux) {

	    // Record minimum value of dr / v
	    double dxv;
	    if (i != 0) dxv = (dd->grd->r_e[i] - dd->grd->r_e[i-1]) /
			  (abs(vr) + constants::min);
	    else dxv = (dd->grd->r_e[i+1] - dd->grd->r_e[i]) /
		   (abs(vr) + constants::min);
	    if (dxv < dx_over_v_r)
	      dx_over_v_r = dxv;
	    
	    // Calculate mass / area crossing the cell face from the
	    // PPM reconstruction of the density, rho = c[0] + c[1]*x +
	    // c[2]*x^2. The calculation here is that the contact
	    // between the two zones has moved from r_{i+1/2} to r_c =
	    // r_{i+1/2} - v dt, and the mass per unit area contained
	    // within the region that has crossed the face is
	    // M/A = \int_{r_{i+1/2}}^{r_c} rho dV / A.
	    double re = dd->grd->r_e[i];
	    double dr = vr*dt;
	    if (vr > 0.0) {
	      flux_re[de] = dd->grd->Vsh(re-dr, dr, dd->grd->dmu) /
		dd->grd->A_r[ie] *
		(ppm_coef_r[pcm] +
		 ppm_coef_r[pcm+1] * dd->grd->xV(re-dr, dr) +
		 ppm_coef_r[pcm+2] * dd->grd->x2V(re-dr, dr));
	    } else {
	      flux_re[de] = -dd->grd->Vsh(re, -dr, dd->grd->dmu) /
		dd->grd->A_r[ie] *
		(ppm_coef_r[pcp] +
		 ppm_coef_r[pcp+1] * dd->grd->xV(re, dr) +
		 ppm_coef_r[pcp+2] * dd->grd->x2V(re, dr));
	    }
	  }

	  // Handle special case of outer boundary; here we apply
	  // diode conditions, and set the flux into the domain to
	  // zero
	  if (i == nr && flux_re[de-1] < 0.0) flux_re[de] = 0.0;
	}
	  
	// Compute diffusive flux if requested
	if (mode == DIFF_ONLY || mode == ADV_DIFF) {
	    
	  // Diffuse flux across boundaries is fixed to zero
	  if (i == 0 || i == nr) continue;
	  
	  // Get local diffusion coefficient
	  double dcoef = dd->alpha * SQR(dd->cs_re[ie]) *
	    (varpi/vK) / (1.0 + SQR(Ts));
	    
	  // Get derivative of dust fraction at cell edge; cap
	  // concentrations at unity to avoid generating insane fluxes
	  // in cells where the and dust density are both just
	  // numerical noise. Note that, even though negative
	  // concentrations are unphysical, we allow them to remain
	  // because diffusion tends to damp numerical noise in cells
	  // below the density floor, thereby making the simulation
	  // more stable.
	  double cp = dd->rho_c[dcp] / (dd->rhog_c[icp] + constants::min);
	  double cm = dd->rho_c[dcm] / (dd->rhog_c[icm] + constants::min);
	  if (abs(cp) > 1.0) cp = cp < 0 ? -1. : 1.;
	  if (abs(cm) > 1.0) cm = cm < 0 ? -1. : 1.;
	  double dfd_dx = (cp - cm) / dd->grd->dx;
	    
	  // Add diffusive flux
	  flux_re[de] += -dt * dcoef * dd->rhog_re[ie] * dd->grd->dxdr_e[i] *
	    dfd_dx;
	}
      }
    }
      
    // Calculate angular fluxes, again parallelizing over radial
    // rays
    double dx_over_v_mu = constants::max;
#if defined(_OPENMP)
#  pragma omp parallel for reduction(min:dx_over_v_mu)
#endif
    for (vecsz j=0; j<=nmu; j++) {
	
      // Fluxes along the upper and lower j edges of the domain are
      // zero
      if (j == 0 || j == nmu) {
	for (vecsz i=0; i<nr; i++) {
	  vecsz de = DEMUM(i,j,k,nr,nmu);
	  flux_mue[de] = 0.0;
	}
	continue;
      }
	
      // Procedure for interior edges
      for (vecsz i=0; i<nr; i++) {
	  
	// Convenience pointers: same idea as in the previous block,
	// but now taking differences in the j direction, and using
	// quantities centered on mu faces
	vecsz ie = IEMUM(i,j,nr);
	vecsz de = DEMUM(i,j,k,nr,nmu);
	vecsz icm = IC(i,j-1,nr);
	vecsz dcm = DC(i,j-1,k,nr,nmu);
	vecsz icp = IC(i,j,nr);
	vecsz dcp = DC(i,j,k,nr,nmu);
	vecsz pcm = PC(i,j-1,k,nr,nmu);
	vecsz pcp = PC(i,j,k,nr,nmu);
	  
	// Set flux to zero
	flux_mue[de] = 0.0;
	  
	// Get local stopping time and Keplerian speed
	double Ts =
	  min(dd->rho_d * dd->a[k] /
	      (dd->rhog_mue[ie] * dd->cs_mue[ie] + constants::min)
	      * dd->Omega_mue[ie], 1.0);
	double varpi = dd->grd->r_c[i] * sqrt(1.0 - SQR(dd->grd->mu_e[j]));
	double vK = sqrt(constants::G*dd->M/varpi);
	  
	// Compute advective flux if desired
	if (mode == ADV_ONLY || mode == ADV_DIFF) {
	    
	  // Get local beta
	  double beta = dd->beta_0[k] * exp(-tau_mue[ie]);
	    
	  // Get terminal velocities in cylindrical coordinates
	  double v_varpi =
	    (dd->vg_mue[ie]/Ts +
	     (beta * sqrt(1.0-SQR(dd->grd->mu_e[j])) -
	      dd->eta_mue[ie]) * vK) /
	    (Ts + 1.0/Ts);
	  double v_z = Ts * (beta-1.0) * dd->Omega_mue[ie] *
	    dd->grd->r_c[i] * dd->grd->mu_e[j];
	    	  
	  // Get mu velocity
	  double vmu = -v_varpi * dd->grd->mu_e[j] +
	    v_z * sqrt(1.0-SQR(dd->grd->mu_e[j]));

	  // Only set the flux if the upwind density is above the floor
	  if ((vmu > 0.0 && dd->rho_c[dcm] >= dd->rho_floor) ||
	      (vmu < 0.0 && dd->rho_c[dcp] >= dd->rho_floor)) {

	    // Record minimum value of r dmu / v
	    double dxv = dd->grd->r_c[i] * (dd->grd->mu_e[j+1] -
					    dd->grd->mu_e[j]) /
	      (abs(vmu) + constants::min);
	    if (dxv < dx_over_v_mu)
	      dx_over_v_mu = dxv;

	    // Calculate mu coordinate of contact after time t
	    double mu = dd->grd->mu_e[j] - vmu * dt / dd->grd->r_c[i];

	    // Calculate mass per unit area transported across cell
	    // face; the calculation here is that rho = rho(mu), so
	    // mass transported = int rho(mu) * 2 pi r^2 dr dmu
	    // = (2 pi / 3) (r_{e,i+1}^3 - r_{e,i}^3) \int rho dmu
	    // = V/dmu * \int rho dmu
	    if (vmu > 0.0) {
	      flux_mue[de] +=
		(ppm_coef_mu[pcm] * (dd->grd->mu_e[j] - mu) +
		 ppm_coef_mu[pcm+1] *
		 (SQR(dd->grd->mu_e[j]) - SQR(mu)) / 2. +
		 ppm_coef_mu[pcm+2] *
		 (CUBE(dd->grd->mu_e[j]) - CUBE(mu)) / 3.) *
		(dd->grd->V[icm] / (dd->grd->A_mu[ie] * dd->grd->dmu));
		;
	    } else {
	      flux_mue[de] +=
		-(ppm_coef_mu[pcp] * (mu - dd->grd->mu_e[j]) +
		  ppm_coef_mu[pcp+1] *
		  (SQR(mu) - SQR(dd->grd->mu_e[j])) / 2. +
		  ppm_coef_mu[pcp+2] *
		  (CUBE(mu) - CUBE(dd->grd->mu_e[j])) / 3.) *
		(dd->grd->V[icp] / (dd->grd->A_mu[ie] * dd->grd->dmu));
	    }	    
	  }
	}
	  
	// Compute diffusive flux if desired
	if (mode == DIFF_ONLY || mode == ADV_DIFF) {
	    
	  // Get local diffusion coefficient
	  double dcoef = dd->alpha * SQR(dd->cs_mue[ie]) *
	    (varpi/vK) / (1.0 + SQR(Ts));
	    
	  // Get derivative of dust fraction at cell edge; see
	  // comments on analogous statement for dfd_dx above for why
	  // we evaluate the derivative in this way
	  double cp = dd->rho_c[dcp] / (dd->rhog_c[icp] + constants::min);
	  double cm = dd->rho_c[dcm] / (dd->rhog_c[icm] + constants::min);
	  if (abs(cp) > 1.0) cp = cp < 0 ? -1. : 1.;
	  if (abs(cm) > 1.0) cm = cm < 0 ? -1. : 1.;
	  double dfd_dmu = (cp - cm) / dd->grd->dmu;
	    
	  // Add diffusive flux
	  flux_mue[de] += -dt * dcoef * dd->rhog_mue[ie] *
	    sqrt(1.0-SQR(dd->grd->mu_e[j])) * dfd_dmu / dd->grd->r_c[i];
	}
      }
    }
    
    // Store max velocity
    if (dx_over_v > dx_over_v_r) dx_over_v = dx_over_v_r;
    if (dx_over_v > dx_over_v_mu) dx_over_v = dx_over_v_mu;
  }

  // Return max velocity
  return dx_over_v;
}


//////////////////////////////////////////////////////////////////////
// Standalone methods to compute and return radial and angular
// velocities. These methods basically just copy the fluxing code, but
// use it to fill velocity arrays instead of calculating fluxes. Since
// these routines are only intended to be used for post-process, we
// don't worry about trying to be too efficient.
//////////////////////////////////////////////////////////////////////
void simulation::vr_(dvec &vr) {

  // Set PPM coefficients and optical depths
  set_ppm_coef();
  set_tau();

  // Size array to store result
  vr.resize((nr+1)*nmu*na);
  
  // Loop over species
  for (vecsz k=0; k<na; k++) {

    // Calculate fluxes, parallelizing over rays
#if defined(_OPENMP)
#  pragma omp parallel for
#endif
    for (vecsz j=0; j<nmu; j++) {
      for (vecsz i=0; i<=nr; i++) {

	// Convenience pointers; these give the indices of the edge
	// we're working on and the cell centers on either side of
	// it, in arrays with various numbers of components
	vecsz ie = IERM(i,j,nr);
	vecsz de = DERM(i,j,k,nr,nmu);
	vecsz dcm = DC(i,j-1,k,nr,nmu);
	vecsz dcp = DC(i,j,k,nr,nmu);

	// Get local stopping time and Keplerian speed
	double Ts =
	  min(dd->rho_d * dd->a[k] /
	      (dd->rhog_re[ie] * dd->cs_re[ie] + constants::min)
	      * dd->Omega_re[ie],
	      1.0);
	double varpi = dd->grd->r_e[i] * sqrt(1.0 - SQR(dd->grd->mu_c[j]));
	double vK = sqrt(constants::G*dd->M/varpi);

	// Get local beta
	double beta = dd->beta_0[k] * exp(-tau_re[ie]);

	// Get terminal velocities in cylindrical coordinates
	double v_varpi =
	  (dd->vg_re[ie]/Ts +
	   (beta * sqrt(1.0-SQR(dd->grd->mu_c[j])) - dd->eta_re[ie]) * vK) /
	  (Ts + 1.0/Ts);
	double v_z = Ts * (beta-1.0) * dd->Omega_re[ie] *
	  dd->grd->r_e[i] * dd->grd->mu_c[j];
	    
	// Store radial velocity
	vr[de] = v_varpi * sqrt(1.0-SQR(dd->grd->mu_c[j])) +
	  v_z * dd->grd->mu_c[j];
	  
	// Set radial velocity to zero if we are at the edge of
	// the domain and the velocity is into the domain, or if the
	// upwind cell is floored
	if (i == 0 && vr[de] >= 0.0) vr[de] = 0.0;
	else if (i == nr && vr[de] <= 0.0) vr[de] = 0.0;
	else if (vr[de] > 0.0 && dd->rho_c[dcm] < dd->rho_floor)
	  vr[de] = 0.0;
	else if (vr[de] < 0.0 && dd->rho_c[dcp] < dd->rho_floor)
	  vr[de] = 0.0;
      }
    }
  }
}


void simulation::vmu_(dvec &vmu) {

  // Set PPM coefficients and optical depths
  set_ppm_coef();
  set_tau();

  // Resize output vector
  vmu.resize(nr*(nmu+1)*na);

  // Loop over species
  for (vecsz k=0; k<na; k++) {

    // Calculate fluxes, parallelizing over rays
#if defined(_OPENMP)
#  pragma omp parallel for
#endif
    for (vecsz j=0; j<=nmu; j++) {

      // Velocities along the upper and lower j edges of the domain are
      // zero
      if (j == 0 || j == nmu) {
	for (vecsz i=0; i<nr; i++) {
	  vecsz de = DEMUM(i,j,k,nr,nmu);
	  vmu[de] = 0.0;
	}
	continue;
      }
	
      // Procedure for interior edges
      for (vecsz i=0; i<nr; i++) {
	  
	// Convenience pointers: same idea as in the previous block,
	// but now taking differences in the j direction, and using
	// quantities centered on mu faces
	vecsz ie = IEMUM(i,j,nr);
	vecsz de = DEMUM(i,j,k,nr,nmu);
	vecsz dcm = DC(i,j-1,k,nr,nmu);
	vecsz dcp = DC(i,j,k,nr,nmu);
	  
	// Get local stopping time and Keplerian speed
	double Ts =
	  min(dd->rho_d * dd->a[k] /
	      (dd->rhog_mue[ie] * dd->cs_mue[ie] + constants::min)
	      * dd->Omega_mue[ie], 1.0);
	double varpi = dd->grd->r_c[i] * sqrt(1.0 - SQR(dd->grd->mu_e[j]));
	double vK = sqrt(constants::G*dd->M/varpi);
	  
	// Get local beta
	double beta = dd->beta_0[k] * exp(-tau_mue[ie]);
	    
	// Get terminal velocities in cylindrical coordinates
	double v_varpi =
	  (dd->vg_mue[ie]/Ts +
	   (beta * sqrt(1.0-SQR(dd->grd->mu_e[j])) - dd->eta_mue[ie]) * vK) /
	  (Ts + 1.0/Ts);
	double v_z = Ts * (beta-1.0) * dd->Omega_mue[ie] *
	  dd->grd->r_c[i] * dd->grd->mu_e[j];
	    	  
	// Get mu velocity
	vmu[de] = -v_varpi * dd->grd->mu_e[j] +
	  v_z * sqrt(1.0-SQR(dd->grd->mu_e[j]));

	// Zero velocity if upwind cell is floored
	if ((vmu[de] > 0.0 && dd->rho_c[dcm] < dd->rho_floor) ||
	    (vmu[de] < 0.0 && dd->rho_c[dcp] < dd->rho_floor))
	  vmu[de] = 0.0;
      }
    }
  }
}


//////////////////////////////////////////////////////////////////////
// Set the part of the RHS that depends only on variables at the old
// (previous) time step
//////////////////////////////////////////////////////////////////////
void simulation::set_rhs_old() {

  // Trivial case of Theta = 1
  if (Theta == 1.0) {
    rhs_old = dd->rho_c;
    return;
  }
  
#if defined(_OPENMP)
#  pragma omp parallel
#endif
  {
    // Loop over grain size bins
    for (vecsz k=0; k<na; k++) {

      // Loop over angles and radii
#if defined(_OPENMP)
#  pragma omp parallel for
#endif
      for (vecsz j=0; j<nmu; j++) {
	for (vecsz i=0; i<nr; i++) {

	  // Convenience indices
	  vecsz ic = IC(i,j,nr);
	  vecsz dc = DC(i,j,k,nr,nmu);
	  vecsz ierm = IERM(i,j,nr);
	  vecsz derm = DERM(i,j,k,nr,nmu);
	  vecsz ierp = IERP(i,j,nr);
	  vecsz derp = DERP(i,j,k,nr,nmu);
	  vecsz iemum = IEMUM(i,j,nr);
	  vecsz demum = DEMUM(i,j,k,nr,nmu);
	  vecsz iemup = IEMUP(i,j,nr);
	  vecsz demup = DEMUP(i,j,k,nr,nmu);

	  // Set RHS
	  rhs_old[dc] = dd->rho_c[dc] - (1.0-Theta) / dd->grd->V[ic] *
	    (flux_re[derp] * dd->grd->A_r[ierp] -
	     flux_re[derm] * dd->grd->A_r[ierm] +
	     flux_mue[demup] * dd->grd->A_mu[iemup] -
	     flux_mue[demum] * dd->grd->A_mu[iemum]);

	}
      }
    }
  }
}

// Set the total RHS vector
void simulation::set_rhs() {

#if defined(_OPENMP)
#  pragma omp parallel
#endif
  {
    // Loop over grain size bins
    for (vecsz k=0; k<na; k++) {

      // Loop over angles and radii
#if defined(_OPENMP)
#  pragma omp parallel for
#endif
      for (vecsz j=0; j<nmu; j++) {
	for (vecsz i=0; i<nr; i++) {

	  // Convenience indices
	  vecsz ic = IC(i,j,nr);
	  vecsz dc = DC(i,j,k,nr,nmu);
	  vecsz ierm = IERM(i,j,nr);
	  vecsz derm = DERM(i,j,k,nr,nmu);
	  vecsz ierp = IERP(i,j,nr);
	  vecsz derp = DERP(i,j,k,nr,nmu);
	  vecsz iemum = IEMUM(i,j,nr);
	  vecsz demum = DEMUM(i,j,k,nr,nmu);
	  vecsz iemup = IEMUP(i,j,nr);
	  vecsz demup = DEMUP(i,j,k,nr,nmu);

	  // Set RHS
	  rhs[dc] = rhs_old[dc] - Theta / dd->grd->V[ic] *
	    (flux_re[derp] * dd->grd->A_r[ierp] -
	     flux_re[derm] * dd->grd->A_r[ierm] +
	     flux_mue[demup] * dd->grd->A_mu[iemup] -
	     flux_mue[demum] * dd->grd->A_mu[iemum]);

	}
      }
    }
  }
}


//////////////////////////////////////////////////////////////////////
// Advance routine using a fully implicit method
//////////////////////////////////////////////////////////////////////
double simulation::advance_FIM(const double dt) {

  // Step 1: calculate the advective + diffusive fluxes for the old
  // time state, and use them to set the old-time part of the RHS
  // vector
  if (Theta != 1.0) {
    set_ppm_coef();         // Get PPM coefficients
    set_tau();              // Do ray trace to get optical depths
    set_flux(dt, ADV_DIFF); // Get advective+diffusive fluxes
  }
  set_rhs_old();       // Set old-time part of RHS vector

  // Step 2: make two copies of the current state, one to be saved in
  // case convergence fails and we need to restore it and for the
  // purposes of time step estimation, and a second that goes into a
  // a history array that holds the history of our iterates.
  Eigen::VectorXd rho_c_save = dd->rho_c;
  //Eigen::VectorXd rho_c_last = dd->rho_c;
  hist.col(0) = dd->rho_c;

  // Step 3: set the matrix M for our current time step, and factorize
  // it. Eigen should be able to parallelize this.
  for (vecsz k=0; k<na; k++) {
    mat[k] = identity + dt*Theta*dmat[k];
    solver.factorize(mat[k]);
  }

  // Step 4: begin iteration loop
  vecsz it = 0, histptr = 0;
  dvec max_i_hist, max_j_hist, max_k_hist;
  bool shift_mat = false;
  if (verb == HIGH_VERBOSITY)
    cout << "simulation::advance starting iteration with dt = "
	 << dt << endl;
  while (it < max_iter) {

    // Print status if verbose
    if (verb == HIGH_VERBOSITY)
      cout << "   ... iteration "
	   << it+1 << ":";

    // Step 4a: shift the history and residual arrays if we've filled
    // them up
    if (shift_mat) {
      for (vecsz n=0; n<=anderson_ord; n++) hist.col(n) = hist.col(n+1);
      for (vecsz n=0; n<anderson_ord; n++) resid.col(n) = resid.col(n+1);
    }

    // Step 4b: get advective flux for current guess; note that, on
    // the first iteration, we can skip the steps of computing the PPM
    // coefficients and doing the ray
    if (it != 0) {
      set_ppm_coef();
      set_tau();
    }
    set_flux(dt, ADV_ONLY);

    // Step 4c: set total RHS vector
    set_rhs();

    // Step 4d: solve matrix equation using current RHS vector for
    // each species; bail out of loop if calculation fails to converge
    // at any point
    for (vecsz k=0; k<na; k++) {
      hist.block(nr*nmu*k, histptr+1, nr*nmu, 1)
	= solver.solveWithGuess(rhs.segment(nr*nmu*k, nr*nmu),
				hist.block(nr*nmu*k, histptr, nr*nmu, 1));
      if (solver.info() != Eigen::Success) {
	if (verb == HIGH_VERBOSITY)
	if (k != 0) cout << "                     ";
	  cout << " convergence failed for grain size bin " << k+1
	       << ", bailing out" << endl;
	break;
      } else if (verb == HIGH_VERBOSITY) {
	if (k != 0) cout << "                     ";
	cout << " grain size bin " << k+1
	     << ": matrix solve converged in " << solver.iterations()
	     << " iterations, error = " << solver.error()
	     << endl;
      }
	
    }
    if (solver.info() != Eigen::Success) break;

    // Step 4e: calculate the residuals
    double max_resid = 0.0;
#if defined(_OPENMP)
#  pragma omp parallel for reduction(max:max_resid)
    for (vecsz i=0; i<nr*nmu*na; i++) {
      if (hist(i, histptr+1) > dd->rho_floor &&
	  hist(i, histptr) > dd->rho_floor) {
	resid(i,histptr) = (hist(i, histptr+1) - hist(i, histptr)) /
	  hist(i, histptr+1);
	max_resid = max(max_resid, abs(resid(i,histptr)));
      } else {
	resid(i,histptr) = 0.0;
      }
    }
    if (verb == HIGH_VERBOSITY)
      cout << "   outer loop residual = " << max_resid << endl;
#else
    vecsz max_i = 0, max_j = 0, max_k = 0;
    for (vecsz k=0; k<na; k++) {
      for (vecsz j=0; j<nmu; j++) {
	for (vecsz i=0; i<nr; i++) {
	  vecsz idx = DC(i,j,k,nr,nmu);
	  if (hist(idx, histptr+1) > dd->rho_floor &&
	      hist(idx, histptr) > dd->rho_floor) {
	    resid(idx,histptr) =
	      (hist(idx, histptr+1) - hist(idx, histptr)) /
	      hist(idx, histptr);
	    if (max_resid < abs(resid(idx, histptr))) {
	      max_i = i;
	      max_j = j;
	      max_k = k;
	      max_resid = abs(resid(idx, histptr));
	    }
	  }
	}
      }
    }
    max_i_hist.push_back(max_i);
    max_j_hist.push_back(max_j);
    max_k_hist.push_back(max_k);
    if (verb == HIGH_VERBOSITY)
      cout << "       outer loop residual = " << max_resid
	   << " in cell " << max_i << ", " << max_j
	   << ", grain species " << max_k
	   << "; current guess density = "
	   << hist(DC(max_i,max_j,max_k,nr,nmu), histptr+1)
	   << ", previous guess density = "
	   << hist(DC(max_i,max_j,max_k,nr,nmu), histptr) << endl;
#endif
    
    // Step 4f: check for convergence
    if (max_resid < tol) {

      // We have converged, so shift the last guess into the final
      // array, then exit the loop
      dd->rho_c = hist.col(histptr+1);
      break;

    } else {

      // We have not converged; use Anderson acceleration to generate
      // next iterate
      if (histptr == 0) {
	
	// Can't use Anderson acceleration if we only have one set of
	// residuals
	dd->rho_c = hist.col(histptr+1);
	
      } else {

	// Fill final row of residual array to impose constraint that
	// sum of coefficients is unity
	for (vecsz n=0; n<=histptr; n++)
	  resid(nr*nmu*na, n) = 1e3*max_resid;
	Eigen::VectorXd lls_rhs = Eigen::VectorXd::Zero(nr*nmu*na+1);
	lls_rhs[nr*nmu*na] = 1e3*max_resid;

	// Factorize and solve
	lscg.factorize(resid.leftCols(histptr+1));
	Eigen::VectorXd guess(histptr+1);
	guess.setZero();
	guess[histptr] = 1.0;
	Eigen::VectorXd wgts = lscg.solveWithGuess(lls_rhs, guess);
	if (lscg.info() != Eigen::Success) {
	  // Fallback if the least squares solve barfs
	  wgts.setZero();
	  wgts[histptr] = 1.0;
	}
	
	// Generate next guess from weighted average of previous ones
	dd->rho_c = hist.block(0,1,nr*nmu*na,histptr+1) * wgts;
	  
      }

      // Update counters and prepare lscg solver to use in next
      // iteration
      it++;
      if (histptr < anderson_ord) {
	histptr++;
	lscg.analyzePattern(resid.leftCols(histptr+1));
      } else {
	shift_mat = true;
      }
    }
  }

  // Step 5: make sure that we have in fact converged, and not exited
  // due to a convergence failure in the inner or outer loop. If
  // convergence failed, just shift the old vector back into the new
  // one and return a negative value
  if (it == max_iter || solver.info() != Eigen::Success) {
    dd->rho_c = rho_c_save;
    if (verb == HIGH_VERBOSITY)
      cout << "   failed to converge!" << endl;
    return -1.0;
  }

  // Step 6: we did advance successfully; estimate the new time step
  // based on the difference between the old and new values of rho_c
  double max_diff = 0.0;
#if defined(_OPENMP)
#  pragma omp parallel for reduction(max:max_diff)
  for (vecsz i=0; i<nr*nmu*na; i++) {
    double diff;
    if (dd->rho_c[i] > dd->rho_floor &&
	rho_c_save[i] > dd->rho_floor) {
      diff = abs( (dd->rho_c[i] - rho_c_save[i]) / dd->rho_c[i] );
    } else {
      diff = 0.0;
    }
    if (diff > max_diff) max_diff = diff;
  }
#else
  vecsz max_i = 0, max_j = 0, max_k = 0;
  for (vecsz k=0; k<na; k++) {
    for (vecsz j=0; j<nmu; j++) {
      for (vecsz i=0; i<nr; i++) {
	vecsz idx = DC(i,j,k,nr,nmu);
	double diff;
	if (dd->rho_c[idx] > dd->rho_floor &&
	    rho_c_save[i] > dd->rho_floor) {
	  diff = abs((dd->rho_c[idx] - rho_c_save[idx]) /
		     dd->rho_c[idx]);
	} else {
	  diff = 0.0;
	}
	if (diff > max_diff) {
	  max_diff = diff;
	  max_i = i;
	  max_j = j;
	  max_k = k;
	}
      }
    }
  }
#endif
  double dt_new = dt * drho_max/max_diff;
  if (verb == HIGH_VERBOSITY)
    cout << "simulation::advance converged in " << it+1
	 << " iterations; new time step = " << dt_new
#ifndef _OPENMP
	 << "; for this step, max diff = " << max_diff
	 << ", rho_prev = "
	 << rho_c_save[DC(max_i,max_j,max_k,nr,nmu)]
	 << ", rho_new = "
	 << dd->rho_c[DC(max_i,max_j,max_k,nr,nmu)]
	 << " in cell " << max_i << ", " << max_j
	 << ", grain species " << max_k
#endif
	 << endl;
  return dt_new;
}



//////////////////////////////////////////////////////////////////////
// Diffusion solver; this does a pure diffusion advance over a time
// dt. It turns true on success, false on convergence failure. If Theta =
// 0.5, this solver is 2nd order accurate in time.
//////////////////////////////////////////////////////////////////////
bool simulation::advance_diffusion(const double dt) {

  // Step 1: calculate the diffusive fluxes at the old time use them
  // to fill the RHS vector
  if (Theta == 1.0) rhs = dd->rho_c;
  else {

    // Get flux
    set_flux(dt, DIFF_ONLY);
    
    // Loop over grain size bins to fill RHS
    for (vecsz k=0; k<na; k++) {

      // Loop over angles and radii
#if defined(_OPENMP)
      bool bad_step = false;
#  pragma omp parallel for reduction(||:bad_step)
#endif
      for (vecsz j=0; j<nmu; j++) {
	for (vecsz i=0; i<nr; i++) {

	  // Convenience indices
	  vecsz ic = IC(i,j,nr);
	  vecsz dc = DC(i,j,k,nr,nmu);
	  vecsz ierm = IERM(i,j,nr);
	  vecsz derm = DERM(i,j,k,nr,nmu);
	  vecsz ierp = IERP(i,j,nr);
	  vecsz derp = DERP(i,j,k,nr,nmu);
	  vecsz iemum = IEMUM(i,j,nr);
	  vecsz demum = DEMUM(i,j,k,nr,nmu);
	  vecsz iemup = IEMUP(i,j,nr);
	  vecsz demup = DEMUP(i,j,k,nr,nmu);

	  // Set RHS
	  rhs[dc] = dd->rho_c[dc] - (1.0-Theta) / dd->grd->V[ic] *
	    (flux_re[derp] * dd->grd->A_r[ierp] -
	     flux_re[derm] * dd->grd->A_r[ierm] +
	     flux_mue[demup] * dd->grd->A_mu[iemup] -
	     flux_mue[demum] * dd->grd->A_mu[iemum]);

	  // If time step is too big so RHS is negative, bail out and
	  // force retry
	  if (rhs[dc] < 0.0 && abs(rhs[dc]) > dd->rho_floor) {
#if defined(_OPENMP)
	    bad_step = true;
#else
	    return false;
#endif
	  }	  
	}
      }
#if defined(_OPENMP)
      if (bad_step) return false;
#endif
    }
  }

  // Step 2: set the matrix M for our current time step, and factorize
  // it. Eigen should be able to parallelize this.
  for (vecsz k=0; k<na; k++) {
    mat[k] = identity + dt*Theta*dmat[k];
    solver.factorize(mat[k]);
  }

  // Step 3: do the diffusion solve; Eigen should parallelize this
  for (vecsz k=0; k<na; k++) {
    dd->rho_c.segment(nr*nmu*k, nr*nmu) =
      solver.solveWithGuess(rhs.segment(nr*nmu*k, nr*nmu),
      		    dd->rho_c.segment(nr*nmu*k, nr*nmu));
    if (solver.info() != Eigen::Success) {
      if (verb == HIGH_VERBOSITY)
	cout << "simulation::advance_diffusion: convergence failed "
	     << "in diffusion solve for grain size bin "
	     << k+1 << "!" << endl;
      return false;
    } else if (verb == HIGH_VERBOSITY) {
      cout << "simulation::advance_diffusion: diffusion solve"
	   << " converged in " << solver.iterations()
	   << " iterations for grain size bin " << k+1
	   << "; error = " << solver.error()
	   << endl;
    }
  }

  // Step 4: return success
  return true;
}


//////////////////////////////////////////////////////////////////////
// Advection solver; this does an advection step through time dt,
// using a 2nd order accurate Su-Osher type scheme:
// Q^(*) = Q^(n) + dt * (dQ/dt)^(n)
// Q^(n+1) = (1/2) Q^(n) + (1/2) Q^(*) + (dt/2) (dQ/dt)^(*)
//////////////////////////////////////////////////////////////////////
double simulation::advance_advection(const double dt) {

  // Step 1: save the starting state
  Eigen::VectorXd rho_c_0 = dd->rho_c;
  
  // Step 2: get fluxes from starting state; this requires calculating
  // the PPM coefficients and doing a ray trace to get optical depths
  set_ppm_coef();
  set_tau();
  double dx_over_v = set_flux(dt, ADV_ONLY);

  // Step 3: do a full time step update, and store the results in
  // dd->rho_c; this gives the state Q^(*)
#if defined(_OPENMP)
  bool bad_step = false;
#endif
  // Loop over grain size bins
  for (vecsz k=0; k<na; k++) {

    // Loop over angles and radii
#if defined(_OPENMP)
#  pragma omp parallel for reduction(||:bad_step)
#endif
    for (vecsz j=0; j<nmu; j++) {
      for (vecsz i=0; i<nr; i++) {

	// Convenience indices
	vecsz ic = IC(i,j,nr);
	vecsz dc = DC(i,j,k,nr,nmu);
	vecsz ierm = IERM(i,j,nr);
	vecsz derm = DERM(i,j,k,nr,nmu);
	vecsz ierp = IERP(i,j,nr);
	vecsz derp = DERP(i,j,k,nr,nmu);
	vecsz iemum = IEMUM(i,j,nr);
	vecsz demum = DEMUM(i,j,k,nr,nmu);
	vecsz iemup = IEMUP(i,j,nr);
	vecsz demup = DEMUP(i,j,k,nr,nmu);
	
	// Set RHS
	dd->rho_c[dc] -= 1.0 / dd->grd->V[ic] *
	  (flux_re[derp] * dd->grd->A_r[ierp] -
	   flux_re[derm] * dd->grd->A_r[ierm] +
	   flux_mue[demup] * dd->grd->A_mu[iemup] -
	   flux_mue[demum] * dd->grd->A_mu[iemum]);
	  
	// Bail out if we produce a negative density in a
	// non-floored cell
	if (dd->rho_c[dc] < 0 && rho_c_0[dc] > dd->rho_floor) {
	  if (verb == HIGH_VERBOSITY)
	    cout << "simulation::advance_advection: first advance produced "
		 << "density = " << dd->rho_c[dc]
		 << " in cell " << i << " " << j
		 << ", grain size bin " << k+1
		 << "; starting density = "
		 << dd->rho_c[dc] + 1.0 / dd->grd->V[ic] *
	      (flux_re[derp] * dd->grd->A_r[ierp] -
	       flux_re[derm] * dd->grd->A_r[ierm] +
	       flux_mue[demup] * dd->grd->A_mu[iemup] -
	       flux_mue[demum] * dd->grd->A_mu[iemum])
		 << "; density advected in r = "
		 << 1.0 / dd->grd->V[ic] * flux_re[derm] * dd->grd->A_r[ierm]
		 << ", "
		 << -1.0 / dd->grd->V[ic] * flux_re[derp] * dd->grd->A_r[ierp]
		 << ", in mu = "
		 << 1.0 / dd->grd->V[ic] * flux_mue[demum] *
	      dd->grd->A_mu[iemum]
		 << ", "
		 << -1.0 / dd->grd->V[ic] * flux_mue[demup] *
	      dd->grd->A_mu[iemup]
		 << endl;
#if defined(_OPENMP)
	  bad_step = true;
#else
	  return -1.0;
#endif
	}
      }
    }
#if defined(_OPENMP)
    if (bad_step) return -1.0;
#endif
  }

  // Step 4: re-calculate fluxes using Q^(*)
  set_ppm_coef();
  set_tau();
  double dx_over_v_2 = set_flux(dt, ADV_ONLY);
  dx_over_v = min(dx_over_v, dx_over_v_2);
  
  // Step 5: store final result
  
  // Loop over grain size bins
  for (vecsz k=0; k<na; k++) {

    // Loop over angles and radii
#if defined(_OPENMP)
#  pragma omp parallel for
#endif
    for (vecsz j=0; j<nmu; j++) {
      for (vecsz i=0; i<nr; i++) {

	// Convenience indices
	vecsz ic = IC(i,j,nr);
	vecsz dc = DC(i,j,k,nr,nmu);
	vecsz ierm = IERM(i,j,nr);
	vecsz derm = DERM(i,j,k,nr,nmu);
	vecsz ierp = IERP(i,j,nr);
	vecsz derp = DERP(i,j,k,nr,nmu);
	vecsz iemum = IEMUM(i,j,nr);
	vecsz demum = DEMUM(i,j,k,nr,nmu);
	vecsz iemup = IEMUP(i,j,nr);
	vecsz demup = DEMUP(i,j,k,nr,nmu);

	// Set RHS
	dd->rho_c[dc] = 0.5 * rho_c_0[dc]
	  + 0.5 * dd->rho_c[dc]
	  - 0.5 / dd->grd->V[ic] *
	  (flux_re[derp] * dd->grd->A_r[ierp] -
	   flux_re[derm] * dd->grd->A_r[ierm] +
	   flux_mue[demup] * dd->grd->A_mu[iemup] -
	   flux_mue[demum] * dd->grd->A_mu[iemum]);

	// Bail out if we produce a negative density in a
	// non-floored cell
	if (dd->rho_c[dc] < 0 && rho_c_0[dc] > dd->rho_floor) {
	  if (verb == HIGH_VERBOSITY)
	    cout << "simulation::advance_advection: second advance produced "
		 << "density = " << dd->rho_c[dc]
		 << " in cell " << i << " " << j
		 << ", grain size bin " << k+1
		 << "; density advected in r = "
		 << 1.0 / dd->grd->V[ic] * flux_re[derm] * dd->grd->A_r[ierm]
		 << ", "
		 << -1.0 / dd->grd->V[ic] * flux_re[derp] * dd->grd->A_r[ierp]
		 << ", in mu = "
		 << 1.0 / dd->grd->V[ic] * flux_mue[demum] *
	      dd->grd->A_mu[iemum]
		 << ", "
		 << -1.0 / dd->grd->V[ic] * flux_mue[demup] *
	      dd->grd->A_mu[iemup]
		 << endl;
#if defined (_OPENMP)
	  bad_step = true;
#else
	  return -1.0;
#endif
	}
      }
    }
#if defined (_OPENMP)
    if (bad_step) return -1.0;
#endif
  }

  // Return maximum velocity
  return dx_over_v;
}


//////////////////////////////////////////////////////////////////////
// IMEX advance routine; this routine uses Strang splitting in time;
// we do a diffusion step for half a time step, then an advection step
// for the full time step, then another half diffusion step
//////////////////////////////////////////////////////////////////////
double simulation::advance_IMEX(const double dt) {

  // Step 1: store state in case we need to restore it due to a
  // convergence failure
  Eigen::VectorXd rho_c_save = dd->rho_c;

  // Step 2: do diffusion advance
  if (!advance_diffusion(dt/2.0)) {
    if (verb == HIGH_VERBOSITY)
      cout << "simulation::advance_IMEX failed during first "
	   << "diffusion advance with dt = " << dt << endl;
    dd->rho_c = rho_c_save;
    return -1.;
  }

  // Step 3: do advection advance
  double dx_over_v = advance_advection(dt);
  if (dx_over_v < 0.) {
    if (verb == HIGH_VERBOSITY)
      cout << "simulation::advance_IMEX failed during advection "
	   << "advance with dt = " << dt << endl;
    dd->rho_c = rho_c_save;
    return -1;
  }

  // Step 4: do diffusion advance
  if (!advance_diffusion(dt/2.0)) {
    if (verb == HIGH_VERBOSITY)
      cout << "simulation::advance_IMEX failed during second "
	   << "diffusion advance with dt = " << dt << endl;
    dd->rho_c = rho_c_save;
    return -1.;
  }

  // Step 5: estimate new time step
  double dt_new = cfl * dx_over_v;
  return dt_new;
}


//////////////////////////////////////////////////////////////////////
// Main simulation routine
//////////////////////////////////////////////////////////////////////
double simulation::run(const double tstop, const dvec tsave,
		       vvec &hist, const double dt0) {

  // Initialize status variables
  bool save_hist = false;
  bool last_step = false;
  
  // If initial output time is zero, copy initial state to history
  hist.resize(0);
  vecsz histptr = 0;
  if (tsave.size() > 0) {
    if (tsave[0] == 0.0) {
      hist.push_back(dd->rho_c);
      histptr++;
    }
  }
  
  // Set initial time step
  double dt;
  if (dt0 > 0.0) dt = dt0;
  else {

    // Estimate time step by calculating fluxes in initial state
    set_ppm_coef();        // Get PPM coefficients
    set_tau();             // Do ray trace to get optical depths
    double dx_over_v =
      set_flux(1.0, ADV_DIFF);  // Get advective+diffusive fluxes

    // Set time step based on advance method

    if (meth == IMEX) {

      // IMEX method: find time step implied by maximum velocity
      dt = cfl * dx_over_v;

    } else {

      // FIM method: find minimum time step implied by the rate of
      // change of density
      dt = constants::max;
      for (vecsz k=0; k<na; k++) {
	double dt_k = constants::max;
#if defined(_OPENMP)
#  pragma omp parallel for reduction(min:dt_k)
#endif
	for (vecsz j=0; j<nmu; j++) {
	  for (vecsz i=0; i<nr; i++) {
	    if (dd->rho_c[DC(i,j,k,nr,nmu)] < dd->rho_floor) continue;
	    double drhodt = 1.0/dd->grd->V[IC(i,j,nr)] *
	      abs(dd->grd->A_r[IERM(i,j,nr)]   * flux_re[DERM(i,j,k,nr,nmu)] -
		  dd->grd->A_r[IERP(i,j,nr)]   * flux_re[DERP(i,j,k,nr,nmu)] +
		  dd->grd->A_mu[IEMUM(i,j,nr)] * flux_mue[DEMUM(i,j,k,nr,nmu)] -
		  dd->grd->A_mu[IEMUP(i,j,nr)] * flux_mue[DEMUP(i,j,k,nr,nmu)]);
	    double dt_cell = drho_max*dd->rho_c[DC(i,j,k,nr,nmu)]/drhodt;
	    if (dt_cell  < dt_k)
	      dt_k = dt_cell;
	  }
	}
	dt = min(dt, dt_k);
      }
    }
  }

  // Begin advancing the calculation
  while (!last_step && stepctr < max_step) {

    // Bail out if time step is too small
    if (dt < dt_min) break;
    
    // Adjust time step to avoid overshooting next output or final
    // time, and flag if we're at the end of the simulation or will be
    // saving the state this time step
    double dt_save = dt;
    if (t+dt >= tstop) last_step = true;
    if (tsave.size() > histptr)
      if (t+dt > tsave[histptr]) save_hist = true;
    if (last_step) dt = tstop - t;
    else if (save_hist) dt = tsave[histptr] - t;

    // Try an advance, retrying on convergence failure; if time step
    // becomes too small due to repeated convergence failures, exit
    // the main loop
    double dt_new;
    bool step_succeeded = false;
    while (!step_succeeded) {

      // Try advance
      if (meth == IMEX) {
	dt_new = advance_IMEX(dt);
	if (dt_new > 0) step_succeeded = true;
      } else {
	dt_new = advance_FIM(dt);
	if (dt_new > 0) step_succeeded = true;
      }

      // Report if advance failed
      if (!step_succeeded &&
	  (verb == MED_VERBOSITY || verb == HIGH_VERBOSITY))
	cout << "simulation::run advance failed, retrying"
	     << endl;

      // Reduce time step if advance fails; bail out if time step gets
      // too small
      if (!step_succeeded) {
	dt /= 2.0;
	dt_save /= 2.0;
	save_hist = false;
	last_step = false;
	if (dt < dt_min) break;
      }
      
    }

    // If the step failed, bail out
    if (!step_succeeded) break;

    // Update time, making sure we hit output times exactly
    if (last_step) t = tstop;
    else if (save_hist) t = tsave[histptr];
    else t += dt;

    // Print status
    if (verb != NO_VERBOSITY) {
      cout << "simulation::run step = " << stepctr+1
	   << ", advanced to t = " << t 
	   << "; completed dt = " << dt
	   << ", next dt = " << dt_new;
      if (save_hist) cout << " [history saved]";
      cout << endl;
    }

    // Save history if we are doing so this step
    if (save_hist)  {
      hist.push_back(dd->rho_c);
      histptr++;
      save_hist = false;
    }

    // Throttle time step increase
    if (dt_new/dt_save > max_dt_fac) dt_new = dt_save * max_dt_fac;
      
    // Update time step and step counter
    dt = dt_new;
    stepctr++;
  }

  // Print final status
  if (last_step && verb != NO_VERBOSITY)
    cout << "simulation::run done in " << stepctr
	 << " steps, t = " << t << endl;
  else if (!last_step && verb != NO_VERBOSITY)
    cout << "simulation::run aborted after " << stepctr
	 << " steps, t = " << t
	 << ", dt = " << dt << endl;

  // Return final time step
  return dt;
}
