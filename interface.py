"""
This module defines a python interface to the c++ dustevol code
"""

import numpy as np
import numpy.ctypeslib as npct
import glob
from ctypes import POINTER, c_size_t, c_double, c_int, \
    c_bool, c_void_p, byref

##################################################################
# Define some type shorthands                                    #
##################################################################
array_1d_double = npct.ndpointer(dtype=np.double, ndim=1,
                                 flags="CONTIGUOUS")
array_1d_size = npct.ndpointer(dtype=c_size_t, ndim=1,
                               flags="CONTIGUOUS")
c_enum = c_int      # c enums are always ints
c_double_p = POINTER(c_double)


##################################################################
# Routine to load the c library                                  #
##################################################################
libptr = None

def loadlib(path=None):
    """
    This function attempts to load the c library and define and
    interface to its routines.

    Parameters
       path : string
          path to library; defaults to the current directory

    Returns
       Nothing
    """

    # Refer to the global library holder, so we only load one copy of
    # the library
    global libptr

    # Return library if already loaded
    if libptr is not None:
        return libptr

    # Load library
    if path is None:
        libptr = npct.load_library("dustevol", ".")
    else:
        libptr = npct.load_library("dustevol", path)

    ###############################################################
    # Define interfaces to the c functions
    ###############################################################

    ###############################################################
    # 1D functions
    ###############################################################
    libptr.c_grid_1D_alloc.restype = c_void_p
    libptr.c_grid_1D_alloc.argtypes \
        = [ c_size_t,     # vecsz
            c_double,     # xmin
            c_double ]    # xmax
    libptr.c_grid_1D_free.restype = None
    libptr.c_grid_1D_free.argtypes = [ c_void_p ]
    libptr.c_grid_1D_nx.restype = c_size_t
    libptr.c_grid_1D_nx.argtypes = [ c_void_p ]
    libptr.c_grid_1D_xc_vec.restype = c_double_p
    libptr.c_grid_1D_xc_vec.argtypes = [ c_void_p ]
    libptr.c_grid_1D_xe_vec.restype = c_double_p
    libptr.c_grid_1D_xe_vec.argtypes = [ c_void_p ]
    libptr.c_dustdisk_1D_alloc.restype = c_void_p
    libptr.c_dustdisk_1D_alloc.argtypes \
        = [ c_void_p,     # grd
            c_double ]    # chi
    libptr.c_dustdisk_1D_copyinit.restype = c_void_p
    libptr.c_dustdisk_1D_copyinit.argtypes \
        = [ c_void_p,         # grd
            c_double,         # chi
            array_1d_double ] # rho_c
    libptr.c_dustdisk_1D_free.restype = None
    libptr.c_dustdisk_1D_free.argtypes = [ c_void_p ]
    libptr.c_dustdisk_1D_chi.restype = c_double
    libptr.c_dustdisk_1D_chi.argtypes = [ c_void_p ]
    libptr.c_dustdisk_1D_shift.restype = c_size_t
    libptr.c_dustdisk_1D_shift.argtypes = [ c_void_p ]
    libptr.c_dustdisk_1D_do_shift.restype = None
    libptr.c_dustdisk_1D_do_shift.argtypes = [ c_void_p, c_double ]
    libptr.c_dustdisk_1D_rho_c.restype = c_double_p
    libptr.c_dustdisk_1D_rho_c.argtypes = [ c_void_p ]
    libptr.c_simulation_1D_alloc.restype = c_void_p
    libptr.c_simulation_1D_alloc.argtypes = [ c_void_p ]
    libptr.c_simulation_1D_free.restype = None
    libptr.c_simulation_1D_free.argtypes = [ c_void_p ]
    libptr.c_simulation_1D_Theta.restype = c_double
    libptr.c_simulation_1D_Theta.argtypes = [ c_void_p ]
    libptr.c_simulation_1D_max_step.restype = c_size_t
    libptr.c_simulation_1D_max_step.argtypes = [ c_void_p ]
    libptr.c_simulation_1D_cfl.restype = c_double
    libptr.c_simulation_1D_cfl.argtypes = [ c_void_p ]
    libptr.c_simulation_1D_rho_floor.restype = c_double
    libptr.c_simulation_1D_rho_floor.argtypes = [ c_void_p ]
    libptr.c_simulation_1D_time.restype = c_double
    libptr.c_simulation_1D_time.argtypes = [ c_void_p ]
    libptr.c_simulation_1D_step.restype = c_size_t
    libptr.c_simulation_1D_step.argtypes = [ c_void_p ]
    libptr.c_simulation_1D_set_Theta.restype = None
    libptr.c_simulation_1D_set_Theta.argtypes = [ c_void_p, c_double ]
    libptr.c_simulation_1D_set_max_step.restype = None
    libptr.c_simulation_1D_set_max_step.argtypes = [ c_void_p, c_size_t ]
    libptr.c_simulation_1D_set_cfl.restype = None
    libptr.c_simulation_1D_set_cfl.argtypes = [ c_void_p, c_double ]
    libptr.c_simulation_1D_set_rho_floor.restype = None
    libptr.c_simulation_1D_set_rho_floor.argtypes = [ c_void_p, c_double ]
    libptr.c_simulation_1D_set_time.restype = None
    libptr.c_simulation_1D_set_time.argtypes = [ c_void_p, c_double ]
    libptr.c_simulation_1D_set_step.restype = None
    libptr.c_simulation_1D_set_step.argtypes = [ c_void_p, c_size_t ]
    libptr.c_simulation_1D_verbosity.restype = c_int
    libptr.c_simulation_1D_verbosity.argtypes = [ c_void_p ]
    libptr.c_simulation_1D_set_verbosity.restype = None
    libptr.c_simulation_1D_set_verbosity.argtypes = [ c_void_p, c_int ]
    libptr.c_simulation_1D_set_ppm_coef.restype = None
    libptr.c_simulation_1D_set_ppm_coef.argtypes = [ c_void_p ]
    libptr.c_simulation_1D_set_tau.restype = None
    libptr.c_simulation_1D_set_tau.argtypes = [ c_void_p ]
    libptr.c_simulation_1D_set_flux.restype = None
    libptr.c_simulation_1D_set_flux.argtypes = [ c_void_p, c_double ]
    libptr.c_simulation_1D_get_ppm_coef.restype = c_double_p
    libptr.c_simulation_1D_get_ppm_coef.argtypes = [ c_void_p ]
    libptr.c_simulation_1D_get_tau.restype = c_double_p
    libptr.c_simulation_1D_get_tau.argtypes = [ c_void_p ]
    libptr.c_simulation_1D_get_flux.restype = c_double_p
    libptr.c_simulation_1D_get_flux.argtypes = [ c_void_p ]
    libptr.c_simulation_1D_advance_diffusion.restype = None
    libptr.c_simulation_1D_advance_diffusion.argtypes \
        = [ c_void_p, c_double ]
    libptr.c_simulation_1D_advance_advection.restype = c_double
    libptr.c_simulation_1D_advance_advection.argtypes \
        = [ c_void_p, c_double ]
    libptr.c_simulation_1D_advance.restype = c_double
    libptr.c_simulation_1D_advance.argtypes \
        = [ c_void_p, c_double ]
    libptr.c_simulation_1D_run.restype = c_double
    libptr.c_simulation_1D_run.argtypes \
        = [ c_void_p,         # sim
            c_double,         # tstop
            c_size_t,         # nsave
            array_1d_double,  # tsave
            array_1d_double,  # hist
            array_1d_size,    # shifthist
            c_double          # dt0
        ]

    ###############################################################
    # grid class
    ###############################################################
    
    libptr.c_grid_linear_alloc.restype = c_void_p
    libptr.c_grid_linear_alloc.argtypes \
        = [ c_size_t,     # _nr
            c_size_t,     # _nmu
            c_double,     # _r_min
            c_double,     # _r_max
            c_double ]    # _mu_max
    libptr.c_grid_log_alloc.restype = c_void_p
    libptr.c_grid_log_alloc.argtypes \
        = [ c_size_t,     # _nr
            c_size_t,     # _nmu
            c_double,     # _r_min
            c_double,     # _r_max
            c_double ]    # _mu_max
    libptr.c_grid_powerlaw_alloc.restype = c_void_p
    libptr.c_grid_powerlaw_alloc.argtypes \
        = [ c_size_t,     # _nr
            c_size_t,     # _nmu
            c_double,     # _r_min
            c_double,     # _r_max
            c_double,     # _mu_max
            c_double ]    # _pidx
    libptr.c_grid_free.restype = None
    libptr.c_grid_free.argtypes = [ c_void_p ]  # grd
    libptr.c_grid_gtype.restype = c_int
    libptr.c_grid_gtype.argtypes = [ c_void_p ] # grd
    libptr.c_grid_pidx.restype = c_double
    libptr.c_grid_pidx.argtypes = [ c_void_p ] # grd
    libptr.c_grid_nr.restype = c_size_t
    libptr.c_grid_nr.argtypes = [ c_void_p ] # grd
    libptr.c_grid_nmu.restype = c_size_t
    libptr.c_grid_nmu.argtypes = [ c_void_p ] # grd
    libptr.c_grid_dx.restype = c_double
    libptr.c_grid_dx.argtypes = [ c_void_p ] # grd
    libptr.c_grid_dmu.restype = c_double
    libptr.c_grid_dmu.argtypes = [ c_void_p ] # grd
    libptr.c_grid_r_e_vec.restype = c_double_p
    libptr.c_grid_r_e_vec.argtypes = [ c_void_p ] # grd
    libptr.c_grid_r_c_vec.restype = c_double_p
    libptr.c_grid_r_c_vec.argtypes = [ c_void_p ] # grd
    libptr.c_grid_x_e_vec.restype = c_double_p
    libptr.c_grid_x_e_vec.argtypes = [ c_void_p ] # grd
    libptr.c_grid_x_c_vec.restype = c_double_p
    libptr.c_grid_x_c_vec.argtypes = [ c_void_p ] # grd
    libptr.c_grid_mu_e_vec.restype = c_double_p
    libptr.c_grid_mu_e_vec.argtypes = [ c_void_p ] # grd
    libptr.c_grid_mu_c_vec.restype = c_double_p
    libptr.c_grid_mu_c_vec.argtypes = [ c_void_p ] # grd
    libptr.c_grid_xbar_vec.restype = c_double_p
    libptr.c_grid_xbar_vec.argtypes = [ c_void_p ] # grd
    libptr.c_grid_x2bar_vec.restype = c_double_p
    libptr.c_grid_x2bar_vec.argtypes = [ c_void_p ] # grd
    libptr.c_grid_intx_vec.restype = c_double_p
    libptr.c_grid_intx2_vec.argtypes = [ c_void_p ] # grd
    libptr.c_grid_intx_c_vec.restype = c_double_p
    libptr.c_grid_intx2_c_vec.argtypes = [ c_void_p ] # grd
    libptr.c_grid_mubar_vec.restype = c_double_p
    libptr.c_grid_mubar_vec.argtypes = [ c_void_p ] # grd
    libptr.c_grid_mu2bar_vec.restype = c_double_p
    libptr.c_grid_mu2bar_vec.argtypes = [ c_void_p ] # grd
    libptr.c_grid_dxdr_e_vec.restype = c_double_p
    libptr.c_grid_dxdr_e_vec.argtypes = [ c_void_p ] # grd
    libptr.c_grid_dxdr_c_vec.restype = c_double_p
    libptr.c_grid_dxdr_c_vec.argtypes = [ c_void_p ] # grd
    libptr.c_grid_V_vec.restype = c_double_p
    libptr.c_grid_V_vec.argtypes = [ c_void_p ] # grd
    libptr.c_grid_A_r_vec.restype = c_double_p
    libptr.c_grid_A_r_vec.argtypes = [ c_void_p ] # grd
    libptr.c_grid_A_mu_vec.restype = c_double_p
    libptr.c_grid_A_mu_vec.argtypes = [ c_void_p ] # grd
    
    ###############################################################
    # gasdisk class
    ###############################################################

    libptr.c_gasdisk_alloc.restype = c_void_p
    libptr.c_gasdisk_alloc.argtypes \
        = [ c_void_p,      # grd
            c_double,      # M
            c_double,      # p
            c_double,      # q
            c_double,      # eps
            c_double,      # T0
            c_double,      # alpha
            c_double ]     # Mdot
    libptr.c_gasdisk_free.restype = None
    libptr.c_gasdisk_free.argtypes = [ c_void_p ]   # gd
    libptr.c_gasdisk_M.restype = c_double
    libptr.c_gasdisk_M.argtypes = [ c_void_p ]      # gd
    libptr.c_gasdisk_p.restype = c_double
    libptr.c_gasdisk_p.argtypes = [ c_void_p ]      # gd
    libptr.c_gasdisk_q.restype = c_double
    libptr.c_gasdisk_q.argtypes = [ c_void_p ]      # gd
    libptr.c_gasdisk_eps.restype = c_double
    libptr.c_gasdisk_eps.argtypes = [ c_void_p ]      # gd
    libptr.c_gasdisk_T0.restype = c_double
    libptr.c_gasdisk_T0.argtypes = [ c_void_p ]      # gd
    libptr.c_gasdisk_alpha.restype = c_double
    libptr.c_gasdisk_alpha.argtypes = [ c_void_p ]      # gd
    libptr.c_gasdisk_Mdot.restype = c_double
    libptr.c_gasdisk_Mdot.argtypes = [ c_void_p ]      # gd
    libptr.c_gasdisk_rhog_c_vec.restype = c_double_p
    libptr.c_gasdisk_rhog_c_vec.argtypes = [ c_void_p ] # gd
    libptr.c_gasdisk_rhog_re_vec.restype = c_double_p
    libptr.c_gasdisk_rhog_re_vec.argtypes = [ c_void_p ] # gd
    libptr.c_gasdisk_rhog_mue_vec.restype = c_double_p
    libptr.c_gasdisk_rhog_mue_vec.argtypes = [ c_void_p ] # gd
    libptr.c_gasdisk_cs_re_vec.restype = c_double_p
    libptr.c_gasdisk_cs_re_vec.argtypes = [ c_void_p ] # gd
    libptr.c_gasdisk_cs_mue_vec.restype = c_double_p
    libptr.c_gasdisk_cs_mue_vec.argtypes = [ c_void_p ] # gd
    libptr.c_gasdisk_Omega_re_vec.restype = c_double_p
    libptr.c_gasdisk_Omega_re_vec.argtypes = [ c_void_p ] # gd
    libptr.c_gasdisk_Omega_mue_vec.restype = c_double_p
    libptr.c_gasdisk_Omega_mue_vec.argtypes = [ c_void_p ] # gd
    libptr.c_gasdisk_eta_re_vec.restype = c_double_p
    libptr.c_gasdisk_eta_re_vec.argtypes = [ c_void_p ] # gd
    libptr.c_gasdisk_eta_mue_vec.restype = c_double_p
    libptr.c_gasdisk_eta_mue_vec.argtypes = [ c_void_p ] # gd
    libptr.c_gasdisk_vg_re_vec.restype = c_double_p
    libptr.c_gasdisk_vg_re_vec.argtypes = [ c_void_p ] # gd
    libptr.c_gasdisk_vg_mue_vec.restype = c_double_p
    libptr.c_gasdisk_vg_mue_vec.argtypes = [ c_void_p ] # gd
    libptr.c_gasdisk_model.restype = None
    libptr.c_gasdisk_model.argtypes \
        = [ c_void_p,      # gd
            c_double,      # r
            c_double,      # mu
            c_double_p,    # rho
            c_double_p,    # cs
            c_double_p,    # Omega
            c_double_p,    # eta
            c_double_p ]   # vg
    
    ###############################################################
    # dustdisk class
    ###############################################################

    libptr.c_dustdisk_alloc.restype = c_void_p
    libptr.c_dustdisk_alloc.argtypes \
        = [ c_void_p,         # gd
            c_double,         # L
            c_double,         # rho_d
            array_1d_double,  # a
            array_1d_double,  # f_a
            c_size_t,         # na
            c_double ]        # rho_floor
    libptr.c_dustdisk_copyinit.restype = c_void_p
    libptr.c_dustdisk_copyinit.argtypes \
        = [ array_1d_double,  # rho_c,
            c_void_p,         # gd
            c_double,         # L
            c_double,         # rho_d
            array_1d_double,  # a
            array_1d_double,  # f_a
            c_size_t,         # na
            c_double ]        # rho_floor
    libptr.c_dustdisk_alloc_qd.restype = c_void_p
    libptr.c_dustdisk_alloc_qd.argtypes \
        = [ c_void_p,         # gd
            c_double,         # L
            c_double,         # rho_d
            array_1d_double,  # a
            c_double,         # f_dust
            c_double,         # qd
            c_size_t,         # na
            c_double ]        # rho_floor
    libptr.c_dustdisk_copyinit_qd.restype = c_void_p
    libptr.c_dustdisk_copyinit_qd.argtypes \
        = [ array_1d_double,  # rho_c,
            c_void_p,         # gd
            c_double,         # L
            c_double,         # rho_d
            array_1d_double,  # a
            c_double,         # f_dust
            c_double,         # qd           
            c_size_t,         # na
            c_double ]        # rho_floor            
    libptr.c_dustdisk_free.restype = None
    libptr.c_dustdisk_free.argtypes = [ c_void_p ]
    libptr.c_dustdisk_na.restype = c_size_t
    libptr.c_dustdisk_na.argtypes = [ c_void_p ]
    libptr.c_dustdisk_L.restype = c_double
    libptr.c_dustdisk_L.argtypes = [ c_void_p ]
    libptr.c_dustdisk_rho_d.restype = c_double
    libptr.c_dustdisk_rho_d.argtypes = [ c_void_p ]
    libptr.c_dustdisk_rho_floor.restype = c_double
    libptr.c_dustdisk_rho_floor.argtypes = [ c_void_p ]
    libptr.c_dustdisk_set_floor.restype = None
    libptr.c_dustdisk_set_floor.argtypes = [ c_void_p, c_double ]
    libptr.c_dustdisk_apply_floor.restype = None
    libptr.c_dustdisk_apply_floor.argtypes = [ c_void_p ]
    libptr.c_dustdisk_a.restype = c_double_p
    libptr.c_dustdisk_a.argtypes = [ c_void_p ]
    libptr.c_dustdisk_f_a.restype = c_double_p
    libptr.c_dustdisk_f_a.argtypes = [ c_void_p ]
    libptr.c_dustdisk_kappa_d.restype = c_double_p
    libptr.c_dustdisk_kappa_d.argtypes = [ c_void_p ]
    libptr.c_dustdisk_beta_0.restype = c_double_p
    libptr.c_dustdisk_beta_0.argtypes = [ c_void_p ]
    libptr.c_dustdisk_rho_c.restype = c_double_p
    libptr.c_dustdisk_rho_c.argtypes = [ c_void_p, array_1d_double ]
    libptr.c_dustdisk_proj.restype = None
    libptr.c_dustdisk_proj.argtypes \
        = [ c_void_p, c_size_t,
            array_1d_double, array_1d_double ]
    libptr.c_compute_f_a.restype = None
    libptr.c_compute_f_a.argtypes \
        = [ c_void_p, c_double, c_double, array_1d_double ]

    ###############################################################
    # simulation class
    ###############################################################

    # Allocation and de-allocation
    libptr.c_simulation_alloc.restype = c_void_p
    libptr.c_simulation_alloc.argtypes = [ c_void_p ]
    libptr.c_simulation_free.restype = None
    libptr.c_simulation_free.argtypes = [ c_void_p ]

    # Information about dimensions
    libptr.c_simulation_nr.restype = c_size_t
    libptr.c_simulation_nr.argtypes = [ c_void_p ]
    libptr.c_simulation_nmu.restype = c_size_t
    libptr.c_simulation_nmu.argtypes = [ c_void_p ]
    libptr.c_simulation_na.restype = c_size_t
    libptr.c_simulation_na.argtypes = [ c_void_p ]

    # Query control parameters
    libptr.c_simulation_tol.restype = c_double
    libptr.c_simulation_tol.argtypes = [ c_void_p ]
    libptr.c_simulation_mat_tol.restype = c_double
    libptr.c_simulation_mat_tol.argtypes = [ c_void_p ]
    libptr.c_simulation_max_dt_fac.restype = c_double
    libptr.c_simulation_max_dt_fac.argtypes = [ c_void_p ]
    libptr.c_simulation_dt_min.restype = c_double
    libptr.c_simulation_dt_min.argtypes = [ c_void_p ]
    libptr.c_simulation_Theta.restype = c_double
    libptr.c_simulation_Theta.argtypes = [ c_void_p ]
    libptr.c_simulation_drho_max.restype = c_double
    libptr.c_simulation_drho_max.argtypes = [ c_void_p ]
    libptr.c_simulation_max_iter.restype = c_size_t
    libptr.c_simulation_max_iter.argtypes = [ c_void_p ]
    libptr.c_simulation_max_step.restype = c_size_t
    libptr.c_simulation_max_step.argtypes = [ c_void_p ]
    libptr.c_simulation_verbosity.restype = c_int
    libptr.c_simulation_verbosity.argtypes = [ c_void_p ]
    libptr.c_simulation_r_flux_off.restype = c_bool
    libptr.c_simulation_r_flux_off.argtypes = [ c_void_p ]
    libptr.c_simulation_anderson_ord.restype = c_size_t
    libptr.c_simulation_anderson_ord.argtypes = [ c_void_p ]
    libptr.c_simulation_method.restype = c_enum
    libptr.c_simulation_method.argtypes = [ c_void_p ]
    libptr.c_simulation_cfl.restype = c_double
    libptr.c_simulation_cfl.argtypes = [ c_void_p ]

    # Set control parameters
    libptr.c_simulation_set_tol.restype = None
    libptr.c_simulation_set_tol.argtypes = [ c_void_p, c_double ]
    libptr.c_simulation_set_mat_tol.restype = None
    libptr.c_simulation_set_mat_tol.argtypes = [ c_void_p, c_double ]
    libptr.c_simulation_set_max_dt_fac.restype = None
    libptr.c_simulation_set_max_dt_fac.argtypes = [ c_void_p, c_double ]
    libptr.c_simulation_set_dt_min.restype = None
    libptr.c_simulation_set_dt_min.argtypes = [ c_void_p, c_double ]
    libptr.c_simulation_set_Theta.restype = None
    libptr.c_simulation_set_Theta.argtypes = [ c_void_p, c_double ]
    libptr.c_simulation_set_drho_max.restype = None
    libptr.c_simulation_set_drho_max.argtypes = [ c_void_p, c_double ]
    libptr.c_simulation_set_max_iter.restype = None
    libptr.c_simulation_set_max_iter.argtypes = [ c_void_p, c_size_t ]
    libptr.c_simulation_set_max_step.restype = None
    libptr.c_simulation_set_max_step.argtypes = [ c_void_p, c_size_t ]
    libptr.c_simulation_set_verbosity.restype = None
    libptr.c_simulation_set_verbosity.argtypes = [ c_void_p, c_int ]
    libptr.c_simulation_set_r_flux_off.restype = None
    libptr.c_simulation_set_r_flux_off.argtypes = [ c_void_p, c_bool ]
    libptr.c_simulation_set_anderson_ord.restype = None
    libptr.c_simulation_set_anderson_ord.argtypes = [ c_void_p, c_size_t ]
    libptr.c_simulation_set_method.restype = None
    libptr.c_simulation_set_method.argtypes = [ c_void_p, c_enum ]
    libptr.c_simulation_set_cfl.restype = None
    libptr.c_simulation_set_cfl.argtypes = [ c_void_p, c_double ]
    
    # Data access methods
    libptr.c_simulation_ppm_coef_r.restype = c_double_p
    libptr.c_simulation_ppm_coef_r.argtypes = [ c_void_p ]
    libptr.c_simulation_ppm_coef_mu.restype = c_double_p
    libptr.c_simulation_ppm_coef_mu.argtypes = [ c_void_p ]
    libptr.c_simulation_tau_re.restype = c_double_p
    libptr.c_simulation_tau_re.argtypes = [ c_void_p ]
    libptr.c_simulation_tau_mue.restype = c_double_p
    libptr.c_simulation_tau_mue.argtypes = [ c_void_p ]
    libptr.c_simulation_flux_re.restype = c_double_p
    libptr.c_simulation_flux_re.argtypes = [ c_void_p ]
    libptr.c_simulation_flux_mue.restype = c_double_p
    libptr.c_simulation_flux_mue.argtypes = [ c_void_p ]
    libptr.c_simulation_rhs_old.restype = None
    libptr.c_simulation_rhs_old.argtypes \
        = [ c_void_p, array_1d_double ]
    libptr.c_simulation_rhs.restype = None
    libptr.c_simulation_rhs.argtypes \
        = [ c_void_p, array_1d_double ]

    # Access to the internal clocks
    libptr.c_simulation_time.restype = c_double
    libptr.c_simulation_time.argtypes = [ c_void_p ]
    libptr.c_simulation_set_time.restype = None
    libptr.c_simulation_set_time.argtypes = [ c_void_p, c_double ]
    libptr.c_simulation_step.restype = c_size_t
    libptr.c_simulation_step.argtypes = [ c_void_p ]
    libptr.c_simulation_set_step.restype = None
    libptr.c_simulation_set_step.argtypes = [ c_void_p, c_size_t ]
    
    # Utility methods
    libptr.c_simulation_set_ppm_coef.restype = None
    libptr.c_simulation_set_ppm_coef.argtypes = [ c_void_p ]
    libptr.c_simulation_set_tau.restype = None
    libptr.c_simulation_set_tau.argtypes = [ c_void_p ]
    libptr.c_simulation_set_flux.restype = c_double
    libptr.c_simulation_set_flux.argtypes = [ c_void_p, c_double, c_enum ]

    # Parts of the FIM update
    libptr.c_simulation_set_rhs_old.restype = None
    libptr.c_simulation_set_rhs_old.argtypes = [ c_void_p ]
    libptr.c_simulation_set_rhs.restype = None
    libptr.c_simulation_set_rhs.argtypes = [ c_void_p ]
    libptr.c_simulation_advance_FIM.restype = c_double
    libptr.c_simulation_advance_FIM.argtypes = [ c_void_p, c_double ]

    # Parts of the IMEX update
    libptr.c_simulation_advance_diffusion.restype = c_bool
    libptr.c_simulation_advance_diffusion.argtypes = [ c_void_p, c_double ]
    libptr.c_simulation_advance_advection.restype = c_double
    libptr.c_simulation_advance_advection.argtypes = [ c_void_p, c_double ]
    libptr.c_simulation_advance_IMEX.restype = c_double
    libptr.c_simulation_advance_IMEX.argtypes = [ c_void_p, c_double ]

    # Main simulation run method
    libptr.c_simulation_run.restype = c_double
    libptr.c_simulation_run.argtypes \
        = [ c_void_p, c_double, c_size_t, array_1d_double,
            array_1d_double, c_double ]

    # Utility analysis routines
    libptr.c_simulation_vr.restype = None
    libptr.c_simulation_vr.argtypes = [ c_void_p, array_1d_double ]
    libptr.c_simulation_vmu.restype = None
    libptr.c_simulation_vmu.argtypes = [ c_void_p, array_1d_double ]
    
    # Return library
    return libptr
    
    

    
