# Repository for "Dynamics of small grains in transitional discs" #

This repository contains the source code and simulation parameter
files used in the simulations presented in "Dynamics of small grains
in transitional discs", by Krumholz, Ireland, & Kratter (2019,
submitted to MNRAS). Raw simulation outputs are not included for
reasons of space, but are available upon request from the authors.

### Contents and layout ###

The main directory contains the source code, while the `params`
subdirectory contains the parameter files used to run the simulations
presented in the paper. There are six parameter files included,
corresponding to cases 1 - 6 in the published paper.


### Building the code ###

The core simulation code is written in C++, while the interface is
Python-based. The paradigm is to build the C++ code as a shared object
library, then use the Python interface to run the simulation and
handle I/O. Building the code involves the following steps:

1. Install and compile the gsl and eigen libraries -- see list of
dependencies below for more details.

2. [optional] Edit the makefiles. The main directory contains generic
makefiles appropriate for both linux and mac, but you may need to
modify these to point to the correct locations where you have
installed eigen and the gsl. You can do this by editing
Make.mach.linux-gnu (for linux) or Make.mach.darwin (for mac), by
editing the Mach.config.override file (any platform), or by creating a
machine-specific makefile for your platform: see Make.mach.avatar for
an example of such a makefile.

3. Build the library by doing `make` in the main directory. This
should create a file ``dustevol.so`` (linux-like OS's) or
`dustevol.dylib` (mac-like OS's).

With this step complete, you are ready to run the code.


### Running the code ###

The script `runsim.py` is provided to run the simulations. Usage is
simply

```
python runsim.py parameters.txt
```

where `parameters.txt` is a text file containing the run
parameters. Examples of the format of the parameters file are included
in the ``params`` subdirectory. The `runsim.py` script will
automatically search for existing checkpoints for a given run and
attempt to restart from the latest one. To force a run to restart from
the beginning, overwriting any existing checkpoints, at the flag
`--norestart` to the command line.


### Visualizing and analyzing results ###

When the code runs, it produces checkpoint files; two such files are
produced per output time. Names of checkpoints are formatted as
`basenameNNNNN.npz` and `basename_ddNNNNN.npz`, where `basename` is
the name for the outputs specified in the run parameter file and
`NNNNN` is the checkpoint number. These are standard `npz` files, can
can be read using the standard `numpy.load` routine. However, it is
usually more convenient to use the provided Python tools to interact
with the data. To read a checkpoint file, do

```python
from simulation import simulation
sim = simulation(chkname=basename, chknum=NNNNN)
```

This will create an object of class `simulation`, which can be used to
query the state of the simulation. Help for the `simulation` class is
available via the standard Python docstring utility. A good place to
start with analysis is to use the built-in plotting function to
visualize the state of the simulation. To do so, do

```python
simulation.dd.make_plot()
```

In addition to examining individual snapshots, the repository contains
a script `makeframes.py` that can be used to batch-process an entire
simulation and make still frames suitable for animation into a
movie. Basic usage is

```
python makeframes.py target_dir
```

where `target_dir` is the directory to be processed, but the script
allows a great deal of control over processing; do

```
python makeframes.py --help
```

for the full list of options.


### Dependencies ###

The C++ simulation code requires the following libraries:

* The [GNU scientific library](http://www.gnu.org/software/gsl/)
* [Eigen](http://eigen.tuxfamily.org/index.php)

In addition, the Python wrapper routines require:

* [numpy](http://www.numpy.org/)
* [matplotlib](https://matplotlib.org/)
* [astropy](http://www.astropy.org/)


### Questions, bugs, and getting involved ###

If you have questions about this software, have discovered any bugs, or want to contribute to ongoing development, please contact [Mark Krumholz](http://www.mso.anu.edu.au/~krumholz/), mark.krumholz@anu.edu.au.


### License ###

This software is distributed under the terms of the [GNU General Public License](http://www.gnu.org/copyleft/gpl.html) version 3.0. The text of the license is included in the main directory of the repository as LICENSE.txt.