"""
This module wraps the c++ dustdisk_1D class
"""

import numpy as np
import numpy.ctypeslib as npct
from interface import loadlib
from grid_1D import grid_1D

class dustdisk_1D(grid_1D):
    """
    A class that defines a dust disk in the simplified 1D problem

    Attributes:
       nx : int
          number of cells
       x_e : array
          positions of cell edges
       x_c : array
          positions of cell centers
       dx : float
          grid spacing
       chi : float
          dimensionless dust velocity parameter
       rho_floor : float
          density floor
       rho_c : float
          mean dust density in cell centers
    """

    # Initializer
    def __init__(self, nx, xmax, chi, xmin=0.0, rho_init=None):
        """
        Construct a dustdisk_1D object

        Parameters:
           nx : : int
              number of cells
           xmax : float
              position of rightmost cell edge
           chi : float
              dimensionless dust velocity parameter
           xmin : float
              position of leftmost cell edge
           rho_init : array
              array of initial value of the dust density; if left as
              None, dust density is initially set to unity everywhere
        """

        # Construct the parent grid_1D object
        self.c_grid = None
        self.c_dustdisk = None
        try:
            # python 3 syntax
            super().__init__(nx, xmax, xmin)
        except:
            # python 2 syntax
            super(dustdisk_1D, self).__init__(nx, xmax, xmin)

        # Construct the c++ object
        if rho_init is None:
            self.c_dustdisk = self.lib.c_dustdisk_1D_alloc(
                self.c_grid, chi)
        else:
            self.c_dustdisk = self.lib.c_dustdisk_1D_copyinit(
                self.c_grid, chi, rho_init)

    # Destructor
    def __del__(self):
        if self.c_grid is not None:
            self.lib.c_grid_1D_free(self.c_grid)
            self.c_grid = None
        if self.c_dustdisk is not None:
            self.lib.c_dustdisk_1D_free(self.c_dustdisk)
            self.c_dustdisk = None

    # Function to apply a shift
    def do_shift(self, rho_floor):
        """
        Shift the solution grid left, eliminating any cells with
        density < rho_floor

        Parameters
           rho_floor : float
              floor value below which cells will be shifted off grid

        Returns
           Nothing
        """
        self.lib.c_dustdisk_1D_do_shift(self.c_dustdisk, rho_floor)

    # Data access
    @property
    def chi(self):
        return self.lib.c_dustdisk_1D_chi(self.c_dustdisk)
    @chi.setter
    def chi(self, val):
        raise AttributeError("chi is read-only")
    @property
    def rho_c(self):
        return npct.as_array(
            self.lib.c_dustdisk_1D_rho_c(self.c_dustdisk),
            shape=(self.nx,))
    @rho_c.setter
    def rho_c(self, val):
        raise AttributeError("rho_c is read-only")
    @property
    def shift(self):
        return self.lib.c_dustdisk_1D_shift(self.c_dustdisk)
    @shift.setter
    def shift(self, val):
        raise AttributeError("shift is read-only")
