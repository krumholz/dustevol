// This is a dummy main routine that does nothing. It is provided so
// that users who want to compile and executable instead of using the
// python interface can put things into it.

#include "simulation.H"
#include <iostream>

int main(int argc, char **argv) {
  
  // Dummy commands to avoid tripping compiler warning about unused variables
  (void) argc;
  (void) argv;
  
  // Stuff goes here
  vecsz nr = 2048, nmu = 1024;
  double rmin = 1.5e12, rmax = 7.5e13;
  double mumax = 0.3;
  grid_log grd(nr, nmu, rmin, rmax, mumax);
  double M = 1.99e33;
  double p = 1.5;
  double q = 3./7.;
  double eps = 0.01;
  double T0 = 120.;
  double alpha = 1.0e-4;
  double Mdot = 0.0;
  double L = 3.8e33;
  double rho_d = 1.0;
  dvec a = { 1.0e-5 };
  dvec f_a = { 1.0e-3 };
  dustdisk dd(&grd, M, p, q, eps, T0, alpha, Mdot, L, rho_d, a, f_a, -1.0e-6);
  //dustdisk dd(gd, L, rho_d, a, f_a, 0.0);
  simulation sim(dd);
  sim.set_verbosity(MED_VERBOSITY);
  //dvec tsave = { 0.0, 1e6, 2e6, 3e6, 4e6, 5e6, 7e6, 8e6, 9e6, 1e7 };
  //vvec hist;
  //sim.run(1e7, tsave, hist);
  dvec tsave;
  vvec hist;
  sim.run(1.0e8, tsave, hist, 1.0e3);
}
