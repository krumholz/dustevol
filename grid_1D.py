"""
This module defines a wrapper for the c++ grid_1D class
"""

import numpy as np
import numpy.ctypeslib as npct
from interface import loadlib

class grid_1D(object):
    """
    A class that defines a 1D grid

    Attributes:
       nx : int
          number of cells
       x_e : array
          positions of cell edges
       x_c : array
          positions of cell centers
       dx : float
          grid spacing
    """

    # Initializer
    def __init__(self, nx, xmax, xmin=0.0):
        """
        Construct a 1D grid

        Parameters:
           nx : int
              number of cells
           xmax : float
              position of rightmost cell edge
           xmin : float
              position of leftmost cell edge
        """

        # Initialize pointer
        self.c_grid = None

        # Load library
        self.lib = loadlib()

        # Construct c++ object
        self.c_grid = self.lib.c_grid_1D_alloc(nx, xmin, xmax)

    # Destructor
    def __del__(self):
        if self.c_grid != None:
            self.lib.c_grid_1D_free(self.c_grid)
            self.c_grid = None

    # Data access
    @property
    def nx(self):
        return self.lib.c_grid_1D_nx(self.c_grid)
    @nx.setter
    def nx(self, val):
        raise AttributeError("nx is read-only")
    @property
    def x_c(self):
        return npct.as_array(self.lib.c_grid_1D_xc_vec(self.c_grid),
                             shape=(self.nx,))
    @x_c.setter
    def x_c(self, val):
        raise AttributeError("x_c is read-only")
    @property
    def x_e(self):
        return npct.as_array(self.lib.c_grid_1D_xe_vec(self.c_grid),
                             shape=(self.nx+1,))
    @x_e.setter
    def x_e(self, val):
        raise AttributeError("x_c is read-only")
    @property
    def dx(self):
        return self.x_e[1] - self.x_e[0]
    @dx.setter
    def dx(self, val):
        raise AttributeError("dx is read-only")
