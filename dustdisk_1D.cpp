// Implementation of the dustdisk_1D class

// Includes and namespaces
#include <cmath>
#include <iostream>
#include "dustdisk_1D.H"

using namespace std;

// Constructor
dustdisk_1D::dustdisk_1D(const grid_1D *_grd, const double _chi,
			 const dvec &_rho_c) :
  grd(_grd), chi(_chi) {
  if (_rho_c.size() == 0) {
    rho_c.assign(grd->nx, 1.0);
  } else {
    rho_c = _rho_c;
  }
}

// Shifting method
void dustdisk_1D::do_shift(const double rho_floor) {
  vecsz i=0;
  while (rho_c[i] <= rho_floor) {
    i++;
    if (i == rho_c.size()) break;
  }
  if (i > 0) {
    shift += i;
    for (vecsz j=0; j<rho_c.size()-i; j++) rho_c[j] = rho_c[j+i];
    for (vecsz j=rho_c.size()-i; j<rho_c.size(); j++) rho_c[j] = 1.0;
  }
}
