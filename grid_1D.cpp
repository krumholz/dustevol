// Implementation of the grid_1D class

// Includes and namespaces
#include <cmath>
#include <iostream>
#include "grid_1D.H"

using namespace std;

// Constructor
grid_1D::grid_1D(const vecsz _nx, const double _xmax, const double _xmin) :
  nx(_nx), xmin(_xmin), xmax(_xmax), dx((_xmax-_xmin)/_nx) {
  x_c.resize(nx);
  x_e.resize(nx+1);
  for (vecsz i=0; i<nx; i++) x_c[i] = xmin + (i+0.5) * dx;
  for (vecsz i=0; i<= nx; i++) x_e[i] = xmin + i * dx;
}



