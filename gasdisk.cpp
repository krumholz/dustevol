// Implementation of the gasdisk class

// Includes and namespaces
#include <cmath>
#include <iostream>
#include "gasdisk.H"

using namespace std;

// Constructor from a provided grid object
gasdisk::gasdisk(const grid *_grd, const double _M, const double _p,
		 const double _q, const double _eps, const double _T0,
		 const double _alpha, const double _Mdot) :
  grd(_grd), nr(_grd->nr_()), nmu(_grd->nmu_()),
  M(_M), p(_p), q(_q), eps(_eps), T0(_T0),
  alpha(_alpha), Mdot(_Mdot) {

  // Set vectors to correct size
  init_vectors();

  // Set gas parameters at cell centers; note that we only need the
  // density at cell centers, not any other quantity
#if defined(_OPENMP)
#  pragma omp parallel for
#endif
  for (vecsz j=0; j<nmu; j++) {
    for (vecsz i=0; i<nr; i++) {
      double cs, Omega, eta, vg;
      model(grd->r_c[i], grd->mu_c[j], rhog_c[IC(i,j,nr)],
	    cs, Omega, eta, vg);
    }
  }

  // Set gas parameters at cell radial edges
#if defined(_OPENMP)
#  pragma omp parallel for
#endif
  for (vecsz j=0; j<nmu; j++) {
    for (vecsz i=0; i<nr+1; i++) {
      model(grd->r_e[i], grd->mu_c[j],
	    rhog_re[IERM(i,j,nr)], cs_re[IERM(i,j,nr)],
	    Omega_re[IERM(i,j,nr)], eta_re[IERM(i,j,nr)],
	    vg_re[IERM(i,j,nr)]);
    }
  }

  // Set gas parameters at angular edges
#if defined(_OPENMP)
#  pragma omp parallel for
#endif
  for (vecsz j=0; j<nmu+1; j++) {
    for (vecsz i=0; i<nr; i++) {
      model(grd->r_c[i], grd->mu_e[j],
	    rhog_mue[IEMUM(i,j,nr)], cs_mue[IEMUM(i,j,nr)],
	    Omega_mue[IEMUM(i,j,nr)], eta_mue[IEMUM(i,j,nr)],
	    vg_mue[IEMUM(i,j,nr)]);
    }
  }
}

// Method to set vectors to correct size
void gasdisk::init_vectors() {
  rhog_c.resize(nr * nmu);
  rhog_re.resize((nr+1) * nmu);
  rhog_mue.resize(nr * (nmu+1));
  cs_re.resize((nr+1) * nmu);
  cs_mue.resize(nr * (nmu+1));
  Omega_re.resize((nr+1) * nmu);
  Omega_mue.resize(nr * (nmu+1));
  eta_re.resize((nr+1) * nmu);
  eta_mue.resize(nr * (nmu+1));
  vg_re.resize((nr+1) * nmu);
  vg_mue.resize(nr * (nmu+1));
}


// Method to implement the gas disk model
void gasdisk::model(const double r, const double mu,
		    double &rho, double &cs, double &Omega,
		    double &eta, double &vg) const {
  double varpi = r * sqrt(1.0 - SQR(mu));
  double z = r * mu;
  double T = T0 * pow(varpi/constants::AU, q);
  cs = sqrt(constants::KB*T / (constants::MU*constants::MH));
  double Omega_mid = sqrt(constants::G*M/CUBE(varpi));
  double hg = cs / Omega_mid;
  double Sigma_g = eps * constants::SIGMA_MMSN *
    pow(varpi/constants::AU, p);
  double rho_g_mid = Sigma_g / (sqrt(2.0*M_PI) * hg);
  rho = rho_g_mid * exp(-0.5*SQR(z/hg));
  Omega = Omega_mid * (1.0 + 0.5*SQR(hg/r) * (p - 0.5*(q+3) + q +
					      0.5*q*SQR(z/hg)));
  eta = -SQR(hg/r) * (p - 0.5*(q+3) + q + 0.5*(q+3)*SQR(z/hg));
  vg = -alpha * (hg/r) * cs *
    (3*(p - 0.5*(q+3)) + 2*(q+3) + (5*q+9)/2*SQR(z/hg));
  vg -= Mdot / (2.0*M_PI*r*Sigma_g);
}
