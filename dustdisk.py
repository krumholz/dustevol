"""
This module defines the python wrapper for the c++ dustdisk class
"""

import numpy as np
import numpy.ctypeslib as npct
import matplotlib
import matplotlib.pyplot as plt
import astropy.units as u
from ctypes import c_double
from gasdisk import gasdisk

##################################################################
# Wrapper for the dustdisk class
##################################################################

class dustdisk(gasdisk):
    """
    A class that defines the background gas disk model

    Attributes:
       nr : int
          number of cells in radial direction
       nmu : int
          number of cells in polar angle direction
       dx : float
          grid spacing in the radial coordinate
       dmu : float
          grid spacing in the angular coordinate
       gtype : 'linear' | 'log' | 'powerlaw'
          type of grid
       pidx : float
          index of powerlaw for gtype "powerlaw"; None for other types
       r_c : array
          cell center radial positions
       r_e : array
          cell edge radial positions
       mu_c : array
          cell center polar angles
       mu_e  : array
          cell edge polar angles
       x_c : array
          cell center radial coordinate positions; the mapping x(r)
          depends on the type of grid
       x_e : array
          cell edge radial coordinate positions; the mapping x(r)
          depends on the type of grid
       dxdr_c : array
          value of dx/dr at cell centers
       dxdr_e : array
          value of dx/dr at cell edges
       varpi_c : array, shape (nr, nmu)
          cylndrical radii of all cell centers
       z_c : array, shape (nr, nmu)
          vertical positions all cell centers
       varpi_re : array, shape (nr, nmu)
          cylndrical radii of all cell radial edges
       z_re : array, shape (nr, nmu)
          vertical positions all cell radial edges
       varpi_mue : array, shape (nr, nmu)
          cylndrical radii of all cell polar edges
       z_mue : array, shape (nr, nmu)
          vertical positions all cell polar edges
       M : float
          stellar mass
       p : float
          surface density powerlaw index
       q : float
          temperature powerlaw index
       eps : float
          surface density normalized to MMSN
       T0 : float
          temperature at 1 AU
       alpha : float
          dimensionless viscosity parameter
       rhog_c : 2d array
          gas density at cell centers
       rhog_re : 2d array
          gas density at cell radial edges
       rhog_mue : 2d array
          gas density at cell polar angle edges
       cs_re : 2d array
          sound speed at cell radial edges
       cs_mue : 2d array
          sound speed at cell polar angle edges
       Omega_re : 2d array
          angular velocity at cell radial edges
       Omega_mue : 2d array
          angular velocity at cell polar angle edges
       eta_re : 2d array
          eta parameter at cell radial edges
       eta_mue : 2d array
          eta parameter at cell polar angle edges
       vg_re : 2d array
          gas cylinrical velocity at cell radial edges
       vg_mue : 2d array
          gas cylindrical velocity at cell polar angle edges
       L : float
          luminosity of central star
       rho_d : float
          internal density of dust grains
       na : int
          number of grain size bins
       a : array
          mean size in each grain size bin
       f_a : array
          ratio of total dust mass to total gas mass for grains in
          each size bin
       kappa_d : array
          array of grain specific opacities
       beta_0 : array
          ratio of unshielded radiation pressure to gravity for all
          species
       rho_c : 3d array
          mass density of dust in each cell and size bin
    """

    # Initializer
    def __init__(self, nr=None, nmu=None,
                 r_min=None, r_max=None,
                 mu_max=None, M=None, p=None, q=None, eps=None,
                 T0=None, alpha=None, Mdot=0.0, L=None, rho_d=None,
                 a=None, f_a=None, gtype='linear', pidx=0.5,
                 rho_floor=-1.0e-6, rho_init=None, f_dust=None,
                 qd=-11./6., savefile=None):
        """
        Construct a dust disk

        Parameters
           nr : int
              number of cells in radial direction; mandatory unless
              savefile is set
           nmu : int
              number of cells in polar angle direction; mandatory unless
              savefile is set
           r_min : float
              minimum radius of grid; mandatory unless savefile is set
           r_max : float
              maximum radius of grid; mandatory unless savefile is set
           mu_max : float
              maximum polar angle; mandatory unless savefile is set
           M : float
              stellar mass; mandatory unless savefile is set
           p : float
              surface density powerlaw index; mandatory unless
              savefile is set
           q : float
              temperature powerlaw index; mandatory unless savefile is
              set
           eps : float
              surface density normalized to MMSN; mandatory unless
              savefile is set
           T0 : float
              temperature at 1 AU; mandatory unless savefile is set
           alpha : float
              dimensionless viscosity parameter; mandatory unless
              savefile is set
           Mdot : float
              accretion rate on top of the normal alpha disk rate;
              ignored if a save file is specified
           L : float
              luminosity of central star; mandatory unless savefile is
              set
           a : array
              mean size in each grain size bin; mandatory unless
              savefile is set
           f_a : array
              ratio of total dust mass to total gas mass for grains in
              each size bin; mandatory if f_dust is None and savefile
              is None, ignored otherwise
           gtype : 'linear' | 'log' | 'powerlaw'
              radial grid type; linear means cells edges are arranged
              such that r_edge[i+1] - r_edge[i] = constant,
              log means that cell edges are arranged such that
              r_edge[i+1] / r_edge[i] = constant, powerlaw means that
              cell edges are arranged such that r_edge[i+1]^p -
              r_edge[i]^p = constant for some powerlaw index p;
              mandatory unless savefile is set
           pidx : float
              powerlaw index for powerlaw grid; mandatory if gtype is
              powerlaw and savefile is None, ignored otherwise
           rho_floor : float
              floor density; when evolving the system, no accuracy or
              convergence criteria are applied to cells with density
              below this value; a positive value of rho_floor is
              interpreted as specifying as absolute density floor,
              while a negative value is interpreted as the negative of
              the minimum optical depth through the domain, which
              implicitly specifies the density floor; in other words,
              a value of -1e-6 means that the density floor will be
              set such that, if the density were at the floor value,
              the optical depth through the domain along a radial ray
              would be 1e-6; ignored if savefile is not None
           rho_init : None | array, shape (nr, nmu, na)
              initial density distribution; if specified this array
              will be used to initialize the density field instead of
              the default; ignored if savefile is not None
           f_dust : None | float
              if set to a float, the dust mass fraction in each size
              bin f_a will be set automatically from f_dust and qd,
              and the value of f_a will be ignored; ignored if
              savefile is not None
           qd : float
              index of the dust mass distribution: dn/dm ~ m^qd;
              ignored if savefile is not None
           savefile : file | str
              name of or pointer to a file containing a saved dust
              disk state
        """

        # If we are reconstructing from a saved state, read the data
        if savefile is not None:
            savedata = np.load(savefile, allow_pickle=True)
            nr = savedata['nr']
            nmu = savedata['nmu']
            r_min = savedata['r_min']
            r_max = savedata['r_max']
            mu_max = savedata['mu_max']
            M = savedata['M']
            p = savedata['p']
            q = savedata['q']
            eps = savedata['eps']
            T0 = savedata['T0']
            alpha = savedata['alpha']
            try:
                Mdot = savedata['Mdot']
            except KeyError:
                Mdot = 0.0
            L = savedata['L']
            rho_d = savedata['rho_d']
            a = savedata['a']
            f_a = savedata['f_a']
            # Need this to handle ridiculous python 2 vs 3 string issues
            if savedata['gtype'] == 0:
                gtype = 'linear'
            elif savedata['gtype'] == 1:
                gtype = 'log'
            else:
                gtype = 'powerlaw'
            pidx = savedata['pidx']
            rho_init = savedata['rho_c']

        # Safety check: make sure all data are valid, since if
        # something fails inside the c++ code it may cause a segfault
        # instead of exiting gracefully
        if nr is None or nmu is None or r_min is None or \
           r_max is None or mu_max is None or M is None or \
           p is None or q is None or eps is None or \
           T0 is None or alpha is None or L is None or \
           rho_d is None or a is None or \
           (f_a is None and f_dust is None):
            raise ValueError("invalid safe file or insufficient "
                             "information supplied to construct dustdisk")

        # Construct the parent gasdisk object
        self.c_grid = None
        self.c_gasdisk = None
        try:
            # python 3 syntax
            super().__init__(nr, nmu, r_min, r_max, mu_max,
                             M, p, q, eps, T0, alpha, Mdot, gtype, pidx)
        except:
            # python 2 syntax
            super(dustdisk, self).__init__(nr, nmu, r_min, r_max, mu_max,
                                           M, p, q, eps, T0, alpha, Mdot,
                                           gtype, pidx)

        # Construct a c++ dustdisk using the grid from parent
        if rho_init is None:
            if f_dust is None:
                self.c_dustdisk = self.lib.c_dustdisk_alloc(
                    self.c_gasdisk, L, rho_d, a, f_a, a.size,
                    rho_floor)
            else:
                self.c_dustdisk = self.lib.c_dustdisk_alloc_qd(
                    self.c_gasdisk, L, rho_d, a, f_dust, qd, a.size,
                    rho_floor)
        else:
            if f_dust is None:
                self.c_dustdisk = self.lib.c_dustdisk_copyinit(
                    np.transpose(rho_init).reshape((nr*nmu*a.size,)),
                    self.c_gasdisk, L, rho_d, a, f_a, a.size,
                    rho_floor)
            else:
                self.c_dustdisk = self.lib.c_dustdisk_copyinit_qd(
                    np.transpose(rho_init).reshape((nr*nmu*a.size,)),
                    self.c_gasdisk, L, rho_d, a, f_dust, qd, a.size,
                    rho_floor)

        # The dustdisk constructor makes a copy of the gasdisk it is
        # given, so in order to avoid carrying around an unnecessary
        # copy, delete the c++ gasdisk associated with the parent class, and
        # just point its grid pointer to the object we just
        # created.
        self.lib.c_gasdisk_free(self.c_gasdisk)
        self.c_gasdisk = self.c_dustdisk

    # Destructor
    def __del__(self):
        if self.c_grid is not None:
            self.lib.c_grid_free(self.c_grid)
        if self.c_gasdisk is not None:
            self.lib.c_dustdisk_free(self.c_gasdisk)

    # Method to save the state of a dust disk
    def write_state(self, f):
        """
        Write the state of a dust disk to file

        Parameters
           f : file | str
              file object or file name to which to write

        Returns
           Nothing
        """
        # Need to convert the grid type from string to integer because
        # otherwise python 3 goes insane
        if self.gtype == 'linear':
            gt = 0
        elif self.gtype == 'log':
            gt = 1
        else:
            gt = 2
        np.savez(f, nr=self.nr, nmu=self.nmu,
                 r_min=self.r_e[0], r_max=self.r_e[-1],
                 mu_max=self.mu_e[-1], M=self.M, p=self.p,
                 q=self.q, eps=self.eps,
                 T0=self.T0, alpha=self.alpha, Mdot=self.Mdot,
                 L=self.L,
                 rho_d=self.rho_d, a=self.a, f_a=self.f_a,
                 gtype=np.array([gt]), pidx=self.pidx,
                 rho_floor=self.rho_floor,
                 rho_c=self.rho_c)

    # Utility methods
    def Ts_re(self):
        """
        Compute stopping times for all grain species at cell radial
        edges

        Parameters
           None

        Returns
           Ts : array, shape (nr+1, nmu, na)
              array of stopping times at cell radial edges
        """        
        return np.clip(
            np.multiply.outer(
                self.Omega_re /
                (self.rhog_re * self.cs_re + np.finfo(float).eps),
                self.rho_d * self.a),
            None, 1.0)
    
    def Ts_mue(self):
        """
        Compute stopping times for all grain species at cell angular
        edges

        Parameters
           None

        Returns
           Ts : array, shape (nr, nmu+1, na)
              array of stopping times at cell angular edges
        """        
        return np.clip(
            np.multiply.outer(
                self.Omega_mue /
                (self.rhog_mue * self.cs_mue + np.finfo(float).eps),
                self.rho_d * self.a),
            None, 1.0)

    
    def proj(self, varpi=None):
        """
        Compute projected column densities

        Parameters
           varpi : array | None
              cylindrical radii at which to compute the projection; if
              left as None, defaults to a set of radii that match the
              radii of the grid, out to the largest radius for which
              the column at that radius is not truncated by the
              spherical grid edge

        Returns
           col : array, shape (nvarpi, na)
              array of projected column densities for each dust grain
              size bin
        """
        if varpi is None:
            varpi = self.r_c[self.r_c <=
                             self.r_e[-1]*np.sqrt(1-self.mu_e[-1]**2)]
        col = np.zeros(varpi.size*self.na)
        self.lib.c_dustdisk_proj(self.c_dustdisk, varpi.size,
                               varpi, col)
        return np.transpose(col.reshape((self.na, varpi.size)))

    def compute_f_a(self, f_dust, qd):
        """
        Compute the mass fraction in each grain size bin that
        corresponds to an input total dust mass fraction and dust mass
        distribution index

        Parameters
           f_dust : float
              total dust mass fraction
           qd : float
              index of grain mass distribution, dn/dm ~ m^qd

        Returns
           f_a : array, shape(na)
              mass fraction in each size bin
        """
        _f_a = np.zeros(self.na)
        self.lib.c_compute_f_a(self.c_dustdisk, f_dust, qd, _f_a)
        return _f_a

    def make_plot(self, fignum=None, fname=None, figsize=(4,6),
                  dustbins=None, logrhomin=None, logrhomax=None,
                  title=None, left=None, right=None, top=None,
                  bottom=None, collim=None, rlog=False, mu=False,
                  rlim=None, zlim=None):
        """
        Make a plot containing an image of the dust density
        distribution

        Parameters
           fignum : int | None
              figure number to write; if None, the plot is made into a
              new figure window (unless fname is set)
           fname : str | None
              if set to a value other than None, the image is written
              to the file fname rather than displayed
           figsize : listlike (2)
              size of image; same format as matplotlib figsize option
           dustbins : None | int | listlike | 'sum'
              which grain size bins to show; if None, all bins are
              shown; if an integer, only the grain size bin of the
              specified index is shown; if this is a list or array,
              the indices specified will be shown; if set to 'sum',
              the total density summed over all bins is shown
           logrhomin : None | float
              minimum log dust density on the color scale; defaults to
              density floor
           logrhomax : None | float
              maximum log dust density on the color scale; defaults to
              maximum density in the simulation domain
           title : None | str
              title for plot
           rlog : bool
              if True, radial direction is plotted in logarithmic
              coordinates
           mu : bool
              if True, vertical coordinate in image plots is mu =
              cos^-1 (z/r); if False, vertical coordinate is z
           rlim : listlike (2)
              plot limits in the radial direction
           zlim : listlike (2)
              plot limits in the z (or mu) direction
           collim : listlike (2)
              limits on the column density range

        Returns
           Nothing
        """

        # Open appropriate figure window, or set non-interactive
        # backend
        if fname is not None:
            be_save = matplotlib.get_backend()
            if fname.endswith('.pdf') or fname.endswith('.PDF'):
                matplotlib.use('pdf')
            elif fname.endswith('.ps') or fname.endswith('.PS') or \
                 fname.endswith('.eps') or fname.endswith('.EPS'):
                matplotlib.use('ps')
            elif fname.endswith('.svg') or fname.endswith('.SVG'):
                matplotlib.use('svg')
            else:
                matplotlib.use('agg')            
        if fignum is None:
            fig = plt.figure(figsize=figsize)
        else:
            fig = plt.figure(fignum, figsize=figsize)

        # Clear the figure
        fig.clf()

        # Get list of dust bins we're plotting
        if dustbins is None:
            bins = np.arange(self.na)
            nbin = bins.size
        elif dustbins == 'sum':
            bins = np.arange(self.na)
            nbin = 1
        else:
            bins = np.atleast_1d(dustbins)
            nbin = bins.size

        # Get x and y coordinates
        AU = float((u.AU/u.cm).to(''))
        rr, mumu = np.meshgrid(self.r_e, self.mu_e, indexing='ij')
        xx = rr*np.sqrt(1-mumu**2)/AU
        if rlog:
            xx = np.log10(xx) 
        if not mu:
            zz = rr*mumu/AU
        else:
            zz = mumu
        if not rlog:
            if rlim is None: xlim = [0, np.amax(xx)]
            else: xlim = rlim
        else:
            if rlim is None: xlim = [np.amin(xx), np.amax(xx)]
            else: xlim = [np.log10(rlim[0]), np.log10(rlim[1])]
        if zlim is None: ylim = [0, np.amax(zz)]
        else: ylim = zlim

        # Get min and max
        if logrhomin is None:
            logrhomin = np.log10(self.rho_floor)
        if logrhomax is None:
            logrhomax = np.log10(np.amax(np.sum(self.rho_c, axis=2)))

        # Make plots
        axs = []
        for i in range(nbin):

            # Create axes
            ax = fig.add_subplot(nbin+1,1,i+1)
            axs.append(ax)

            # Get density to plot
            if dustbins == 'sum':
                data = np.sum(self.rho_c, axis=2)
            else:
                data = self.rho_c[:,:,bins[i]]
            data = np.maximum(data, self.rho_floor)
            data = np.log10(data)

            # Make image
            img = ax.pcolormesh(np.transpose(xx), np.transpose(zz),
                                np.transpose(data), vmin=logrhomin,
                                vmax=logrhomax)
            if not mu:
                ax.set_ylabel(r'$z$ [AU]')
            else:
                ax.set_ylabel(r'$\mu$')
            ax.set_xticklabels([])
            ax.set_xlim(xlim)
            ax.set_ylim(ylim)
            if i == 0 and title is not None:
                ax.set_title(title)
            if not mu:
                textcolor = 'k'
            else:
                textcolor = 'w'
            ax.text(0.05, 0.85, r'$a = {:3.1f}$ $\mu$m'.
                    format(1e4*self.a[bins[i]]),
                    transform = ax.transAxes,
                    color=textcolor)

        # Add column density panel
        axcd = fig.add_subplot(nbin+1,1,nbin+1)
        col = self.proj()
        rc = self.r_c[:col.shape[0]]/AU
        if rlog:
            rc = np.log10(rc)
        for i in range(bins.size):
            axcd.plot(rc, np.log10(col[:,bins[i]]),
                      label=r'${:3.1f}$ $\mu$m'.
                      format(1e4*self.a[bins[i]]))
        if dustbins == 'sum':
            axcd.plot(rc, np.log10(np.sum(col, axis=1)),
                      label='Total')
        axcd.legend(ncol=2, prop={'size' : 8})
        axcd.set_xlim(xlim)
        if not rlog:
            axcd.set_xlabel(r'$\varpi$ [AU]')
        else:
            axcd.set_xlabel(r'$\log\,\varpi$ [AU]')
        axcd.set_ylabel(r'$\log\Sigma$ [g cm$^{-2}$]')
        if collim is not None:
            axcd.set_ylim(collim)

        # Adjust spacing
        if left is None:
            left = 0.18
        if right is None:
            right = 0.8
        if top is None:
            top = 0.95
        if bottom is None:
            bottom = 0.1
        fig.subplots_adjust(hspace=0, wspace=0, left=left,
                            right=right, top=top, bottom=bottom)

        # Add colorbar
        plt.draw()
        ll = fig.transFigure.inverted().transform(
            axs[-1].transAxes.transform((1,0)))
        ul = fig.transFigure.inverted().transform(
            axs[0].transAxes.transform((1,1)))
        axcbar = fig.add_axes((ll[0], ll[1], 0.02, ul[1]-ll[1]))
        plt.colorbar(img, cax=axcbar, label=r'$\log\rho$ [g cm$^{-3}$]')

        # Hide overlapping labels
        for ax in axs[1:]:
            plt.setp(ax.get_yticklabels()[-1], visible=False)
        plt.setp(axcd.get_yticklabels()[-1], visible=False)
                
        # Save if requested, then set backend back to original
        if fname is not None:
            plt.savefig(fname)
            matplotlib.use(be_save)

    # Access to data
    @property
    def na(self):
        return self.lib.c_dustdisk_na(self.c_dustdisk)
    @na.setter
    def na(self, val):
        raise AttributeError("na is read-only")
    @property
    def L(self):
        return self.lib.c_dustdisk_L(self.c_dustdisk)
    @L.setter
    def L(self, val):
        raise AttributeError("L is read-only")
    @property
    def rho_d(self):
        return self.lib.c_dustdisk_rho_d(self.c_dustdisk)
    @rho_d.setter
    def rho_d(self, val):
        raise AttributeError("rho_d is read-only")
    @property
    def rho_floor(self):
        return self.lib.c_dustdisk_rho_floor(self.c_dustdisk)
    @rho_floor.setter
    def rho_floor(self, val):
        self.lib.c_dustdisk_set_floor(self.c_dustdisk, val)
    @property
    def a(self):
        return npct.as_array(
            self.lib.c_dustdisk_a(self.c_dustdisk),
            shape=(self.na,))
    @a.setter
    def a(self, val):
        raise AttributeError("a is read-only")
    @property
    def f_a(self):
        return npct.as_array(
            self.lib.c_dustdisk_f_a(self.c_dustdisk),
            shape=(self.na,))
    @f_a.setter
    def f_a(self, val):
        raise AttributeError("f_a is read-only")
    @property
    def kappa_d(self):
        return npct.as_array(
            self.lib.c_dustdisk_kappa_d(self.c_dustdisk),
            shape=(self.na,))
    @kappa_d.setter
    def kappa_d(self, val):
        raise AttributeError("kappa_d is read-only")
    @property
    def beta_0(self):
        return npct.as_array(
            self.lib.c_dustdisk_beta_0(self.c_dustdisk),
            shape=(self.na,))
    @beta_0.setter
    def beta_0(self, val):
        raise AttributeError("beta_0 is read-only")
    @property
    def rho_c(self):
        rc = np.zeros(self.nr*self.nmu*self.na, dtype=c_double)
        self.lib.c_dustdisk_rho_c(self.c_dustdisk, rc)
        rc = np.transpose(rc.reshape(self.na,self.nmu,self.nr))
        return rc
    @rho_c.setter
    def rho_c(self, val):
        raise AttributeError("rho_c is read-only")


