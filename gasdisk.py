"""
This module defines the python wrapper for the c++ gasdisk class
"""

import numpy as np
import numpy.ctypeslib as npct
from grid import grid

##################################################################
# Wrapper for the gasdisk class
##################################################################

class gasdisk(grid):
    """
    A class that defines the background gas disk model

    Attributes:
       nr : int
          number of cells in radial direction
       nmu : int
          number of cells in polar angle direction
       dx : float
          grid spacing in the radial coordinate
       dmu : float
          grid spacing in the angular coordinate
       gtype : 'linear' | 'log' | 'powerlaw'
          type of grid
       pidx : float
          index of powerlaw for gtype "powerlaw"; None for other types
       r_c : array
          cell center radial positions
       r_e : array
          cell edge radial positions
       mu_c : array
          cell center polar angles
       mu_e  : array
          cell edge polar angles
       x_c : array
          cell center radial coordinate positions; the mapping x(r)
          depends on the type of grid
       x_e : array
          cell edge radial coordinate positions; the mapping x(r)
          depends on the type of grid
       dxdr_c : array
          value of dx/dr at cell centers
       dxdr_e : array
          value of dx/dr at cell edges
       varpi_c : array, shape (nr, nmu)
          cylndrical radii of all cell centers
       z_c : array, shape (nr, nmu)
          vertical positions all cell centers
       varpi_re : array, shape (nr, nmu)
          cylndrical radii of all cell radial edges
       z_re : array, shape (nr, nmu)
          vertical positions all cell radial edges
       varpi_mue : array, shape (nr, nmu)
          cylndrical radii of all cell polar edges
       z_mue : array, shape (nr, nmu)
          vertical positions all cell polar edges
       M : float
          stellar mass
       p : float
          surface density powerlaw index
       q : float
          temperature powerlaw index
       eps : float
          surface density normalized to MMSN
       T0 : float
          temperature at 1 AU
       alpha : float
          dimensionless viscosity parameter
       Mdot : float
          accretion rate (on top of that implied by the alpha)
       rhog_c : 2d array
          gas density at cell centers
       rhog_re : 2d array
          gas density at cell radial edges
       rhog_mue : 2d array
          gas density at cell polar angle edges
       cs_re : 2d array
          sound speed at cell radial edges
       cs_mue : 2d array
          sound speed at cell polar angle edges
       Omega_re : 2d array
          angular velocity at cell radial edges
       Omega_mue : 2d array
          angular velocity at cell polar angle edges
       eta_re : 2d array
          eta parameter at cell radial edges
       eta_mue : 2d array
          eta parameter at cell polar angle edges
       vg_re : 2d array
          gas cylinrical velocity at cell radial edges
       vg_mue : 2d array
          gas cylindrical velocity at cell polar angle edges
    """

    # Initializer
    def __init__(self, nr, nmu, r_min, r_max, mu_max, M, p, q, eps,
                 T0, alpha, Mdot=0.0, gtype='linear', pidx=0.5):
        """
        Construct a grid

        Parameters
           nr : int
              number of cells in radial direction
           nmu : int
              number of cells in polar angle direction
           r_min : float
              minimum radius of grid
           r_max : float
              maximum radius of grid
           mu_max : float
              maximum polar angle
           M : float
              stellar mass
           p : float
              surface density powerlaw index
           q : float
              temperature powerlaw index
           eps : float
              surface density normalized to MMSN
           T0 : float
              temperature at 1 AU
           alpha : float
              dimensionless viscosity parameter
           Mdot : float
              accretion rate onto central star; this causes an inward
              velocity to be added to the gas, on top of any velocity
              associated with the alpha disk model
           gtype : 'linear' | 'log' | 'powerlaw'
              radial grid type; linear means cells edges are arranged
              such that r_edge[i+1] - r_edge[i] = constant,
              log means that cell edges are arranged such that
              r_edge[i+1] / r_edge[i] = constant, powerlaw means that
              cell edges are arranged such that r_edge[i+1]^p -
              r_edge[i]^p = constant for some powerlaw index p
           pidx : float
              powerlaw index for powerlaw grid; ignored if gtype is
              not 'powerlaw'
        """

        # Construct the parent grid object
        self.c_grid = None
        self.c_gasdisk = None
        try:
            # python 3 syntax
            super().__init__(nr, nmu, r_min, r_max, mu_max,
                             gtype, pidx)
        except:
            # python 2 syntax
            super(gasdisk, self).__init__(nr, nmu, r_min, r_max, mu_max,
                                          gtype, pidx)

        # Construct a c++ gasdisk using the grid from parent
        self.c_gasdisk = self.lib.c_gasdisk_alloc(
            self.c_grid, M, p, q, eps, T0, alpha, Mdot)

    # Destructor
    def __del__(self):
        if self.c_grid is not None:
            self.lib.c_grid_free(self.c_grid)
        if self.c_gasdisk is not None:
            self.lib.c_gasdisk_free(self.c_gasdisk)
         
    # The model function
    def model(r, mu):
        """
        Calculates gas disk model values at an arbitrary position

        Parameters:
           r : float
              radius
           mu : float
              cosine of polar angle

        Returns:
           rhog : float
              gas density
           cs : float
              sound speed
           Omega : float
              angular velocity
           eta : float
              deviation from Keplerian rotation
           vg : float
              gas radial velocity
        """
        rhog = 0.0
        cs = 0.0
        Omega = 0.0
        eta = 0.0
        vg = 0.0
        self.lib.c_gasdisk_model(
            self.c_gasdisk, r, mu, byref(rhog), byref(cs),
            byref(Omega), byref(eta), byref(vg))        

    # Access to data
    @property
    def M(self):
        return self.lib.c_gasdisk_M(self.c_gasdisk)
    @M.setter
    def M(self, val):
        raise AttributeError("M is read-only")
    @property
    def p(self):
        return self.lib.c_gasdisk_p(self.c_gasdisk)
    @p.setter
    def p(self, val):
        raise AttributeError("p is read-only")
    @property
    def q(self):
        return self.lib.c_gasdisk_q(self.c_gasdisk)
    @q.setter
    def q(self, val):
        raise AttributeError("q is read-only")
    @property
    def eps(self):
        return self.lib.c_gasdisk_eps(self.c_gasdisk)
    @eps.setter
    def eps(self, val):
        raise AttributeError("eps is read-only")
    @property
    def T0(self):
        return self.lib.c_gasdisk_T0(self.c_gasdisk)
    @T0.setter
    def T0(self, val):
        raise AttributeError("T0 is read-only")
    @property
    def alpha(self):
        return self.lib.c_gasdisk_alpha(self.c_gasdisk)
    @alpha.setter
    def alpha(self, val):
        raise AttributeError("alpha is read-only")
    @property
    def Mdot(self):
        return self.lib.c_gasdisk_Mdot(self.c_gasdisk)
    @Mdot.setter
    def Mdot(self, val):
        raise AttributeError("Mdot is read-only")
    @property
    def rhog_c(self):
        return np.transpose(
            npct.as_array(self.lib.c_gasdisk_rhog_c_vec(self.c_gasdisk),
                          shape=(self.nmu,self.nr)))
    @rhog_c.setter
    def rhog_c(self, val):
        raise AttributeError("rhog_c is read-only")
    @property
    def rhog_re(self):
        return np.transpose(
            npct.as_array(self.lib.c_gasdisk_rhog_re_vec(self.c_gasdisk),
                          shape=(self.nmu,self.nr+1)))
    @rhog_re.setter
    def rhog_re(self, val):
        raise AttributeError("rhog_re is read-only")
    @property
    def rhog_mue(self):
        return np.transpose(
            npct.as_array(self.lib.c_gasdisk_rhog_mue_vec(self.c_gasdisk),
                          shape=(self.nmu+1,self.nr)))
    @rhog_mue.setter
    def rhog_mue(self, val):
        raise AttributeError("rhog_mue is read-only")
    @property
    def cs_re(self):
        return np.transpose(
            npct.as_array(self.lib.c_gasdisk_cs_re_vec(self.c_gasdisk),
                          shape=(self.nmu,self.nr+1)))
    @cs_re.setter
    def cs_re(self, val):
        raise AttributeError("cs_re is read-only")
    @property
    def cs_mue(self):
        return np.transpose(
            npct.as_array(self.lib.c_gasdisk_cs_mue_vec(self.c_gasdisk),
                          shape=(self.nmu+1,self.nr)))
    @cs_mue.setter
    def cs_mue(self, val):
        raise AttributeError("cs_mue is read-only")
    @property
    def Omega_re(self):
        return np.transpose(
            npct.as_array(self.lib.c_gasdisk_Omega_re_vec(self.c_gasdisk),
                          shape=(self.nmu,self.nr+1)))
    @Omega_re.setter
    def Omega_re(self, val):
        raise AttributeError("Omega_re is read-only")
    @property
    def Omega_mue(self):
        return np.transpose(
            npct.as_array(self.lib.c_gasdisk_Omega_mue_vec(self.c_gasdisk),
                          shape=(self.nmu+1,self.nr)))
    @Omega_mue.setter
    def Omega_mue(self, val):
        raise AttributeError("Omega_mue is read-only")
    @property
    def eta_re(self):
        return np.transpose(
            npct.as_array(self.lib.c_gasdisk_eta_re_vec(self.c_gasdisk),
                          shape=(self.nmu,self.nr+1)))
    @eta_re.setter
    def eta_re(self, val):
        raise AttributeError("eta_re is read-only")
    @property
    def eta_mue(self):
        return np.transpose(
            npct.as_array(self.lib.c_gasdisk_eta_mue_vec(self.c_gasdisk),
                          shape=(self.nmu+1,self.nr)))
    @eta_mue.setter
    def eta_mue(self, val):
        raise AttributeError("eta_mue is read-only")
    @property
    def vg_re(self):
        return np.transpose(
            npct.as_array(self.lib.c_gasdisk_vg_re_vec(self.c_gasdisk),
                          shape=(self.nmu,self.nr+1)))
    @vg_re.setter
    def vg_re(self, val):
        raise AttributeError("vg_re is read-only")
    @property
    def vg_mue(self):
        return np.transpose(
            npct.as_array(self.lib.c_gasdisk_vg_mue_vec(self.c_gasdisk),
                          shape=(self.nmu+1,self.nr)))
    @vg_mue.setter
    def vg_mue(self, val):
        raise AttributeError("vg_mue is read-only")


