# Main makefile for slug2

# Did the user tell us to use a particular machine? If so, use that.
ifdef MACHINE
     include Make.mach.$(MACHINE)
else
     # Machine not specified, so try to guess
     UNAME              = $(shell uname)
     UNAMEN             = $(shell uname -n)

     # Do we have a makefile that matches the machine name? If so, use
     # that. If not, use a generic makefile depending on the OS.
     ifeq ($(UNAMEN), avatar)
          include Make.mach.avatar
     else ifneq (,$(findstring gadi, $(UNAMEN)))
          include Make.mach.gadi
     else ifeq ($(UNAMEN), morticia.anu.edu.au)
          include Make.mach.morticia
     else ifeq ($(UNAMEN), markk)
          include Make.mach.morticia
     else ifeq ($(UNAME), Linux)
          include Make.mach.linux-gnu
     else ifeq ($(UNAME), Darwin)
          include Make.mach.darwin
     else
          $(info Cannot detect system type. Suggest you specify MACHINE= manually.)
          include Make.mach.generic
     endif
endif

# Set compiler
CXX             = $(MACH_CXX)

# Set optimization mode flags
CXXOPTFLAGS     = $(MACH_CXXOPTFLAGS) $(MACH_C11FLAG) -DNDEBUG -MMD -MP
LDOPTFLAGS      = $(MACH_LDOPTFLAGS) $(MACH_CXXFLAG)
INCFLAGS	= $(MACH_INCFLAGS)

# Set debug mode flags
CXXDEBFLAGS     = $(MACH_CXXDEBFLAGS) $(MACH_C11FLAG) -MMD -MP
LDDEBFLAGS      = $(MACH_LDDEBFLAGS) $(MACH_CXXFLAG)

# Include flags
INCFLAGS = $(MACH_INCFLAGS)
ifdef CXX_INCLUDE_PATH
     INCFLAGS += -I$(subst :, -I,$(CXX_INCLUDE_PATH))
endif
ifdef C_INCLUDE_PATH
     INCFLAGS += -I$(subst :, -I,$(C_INCLUDE_PATH))
endif
ifdef EIGEN_HDR_PATH
     INCFLAGS += -I$(EIGEN_HDR_PATH)
endif
ifdef GSL_HDR_PATH
     INCFLAGS += -I$(GSL_HDR_PATH)
endif

# Link flags
LDLIBFLAGS = $(MACH_LDLIBFLAGS)
ifdef LD_LIBRARY_PATH
     LDLIBFLAGS += -L$(subst :, -L ,$(LD_LIBRARY_PATH))
endif
ifdef LIBRARY_PATH
     LDLIBFLAGS += -L$(subst :, -L ,$(LIBRARY_PATH))
endif

# Add command to link to GSL
LDLIBFLAGS += -lgsl
ifdef GSL_LIB_PATH
     LDLIBFLAGS += -L$(GSL_LIB_PATH)
endif

# Read any user overrides
-include Make.config.override

CXXFLAGS +=  $(INCFLAGS) $(DEFINES)
LDFLAGS  +=  $(LDLIBFLAGS)

# Name for compiled code
LIBNAME     = dustevol$(LIB_EXTENSION)
EXENAME     = dustevol
DEBUGNAME   = dustevol_debug

# Pointers to sources
SOURCES         = $(wildcard *.cpp)
OBJECTS         = $(SOURCES:%.cpp=%.o)
DEPS            = $(SOURCES:%.cpp=%.d)

# Default target
.PHONY: lib exe debug clean
lib: CXXFLAGS += $(CXXOPTFLAGS) $(CLIBFLAG) $(CXXOMPFLAG)
lib: LDFLAGS  += $(LDOPTFLAGS) $(LDOMPFLAG)
lib: $(LIBNAME)

exe: CXXFLAGS += $(CXXOPTFLAGS) $(CXXOMPFLAG)
exe: LDFLAGS  += $(LDOPTFLAGS) $(LDOMPFLAG)
exe: $(EXENAME)

debug: CXXFLAGS += $(CXXDEBFLAGS)
debug: LDFLAGS  += $(LDDEBFLAGS)
debug: $(DEBUGNAME)


# Pointers to sources
SOURCES         = $(wildcard *.cpp)
OBJECTS         = $(SOURCES:%.cpp=%.o)
DEPS            = $(SOURCES:%.cpp=%.d)

# Include dependencies
-include $(DEPS)


$(LIBNAME):     $(OBJECTS)
	$(CXX) $(LDFLAGS) $(DYNLIBFLAG) -o $(LIBNAME) $^

$(EXENAME):	$(OBJECTS)
	$(CXX) $(LDFLAGS) -o $(EXENAME) $^

$(DEBUGNAME):	$(OBJECTS)
	$(CXX) $(LDFLAGS) -o $(DEBUGNAME) $^

clean:
	rm -f $(LIBNAME) $(EXENAME) $(DEBUGNAME) $(OBJECTS) $(DEPS)

