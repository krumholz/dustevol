"""
This module defines the python wrapper for the c++ grid class
"""

import numpy as np
import numpy.ctypeslib as npct
from interface import loadlib

##################################################################
# Wrapper for the grid class
##################################################################

class grid(object):
    """
    A class that defines a computational grid

    Attributes:
       nr : int
          number of cells in radial direction
       nmu : int
          number of cells in polar angle direction
       dx : float
          grid spacing in the radial coordinate
       dmu : float
          grid spacing in the angular coordinate
       gtype : 'linear' | 'log' | 'powerlaw'
          type of grid
       pidx : float
          index of powerlaw for gtype "powerlaw"; None for other types
       r_c : array
          cell center radial positions
       r_e : array
          cell edge radial positions
       mu_c : array
          cell center polar angles
       mu_e  : array
          cell edge polar angles
       x_c : array
          cell center radial coordinate positions; the mapping x(r)
          depends on the type of grid
       x_e : array
          cell edge radial coordinate positions; the mapping x(r)
          depends on the type of grid
       dxdr_c : array
          value of dx/dr at cell centers
       dxdr_e : array
          value of dx/dr at cell edges
       varpi_c : array, shape (nr, nmu)
          cylndrical radii of all cell centers
       z_c : array, shape (nr, nmu)
          vertical positions all cell centers
       varpi_re : array, shape (nr, nmu)
          cylndrical radii of all cell radial edges
       z_re : array, shape (nr, nmu)
          vertical positions all cell radial edges
       varpi_mue : array, shape (nr, nmu)
          cylndrical radii of all cell polar edges
       z_mue : array, shape (nr, nmu)
          vertical positions all cell polar edges
    """

    # Initializer
    def __init__(self, nr, nmu, r_min, r_max, mu_max,
                 gtype='linear', pidx=0.5):
        """
        Construct a grid

        Parameters
           nr : int
              number of cells in radial direction
           nmu : int
              number of cells in polar angle direction
           r_min : float
              minimum radius of grid
           r_max : float
              maximum radius of grid
           mu_max : float
              maximum polar angle
           gtype : 'linear' | 'log' | 'powerlaw'
              radial grid type; linear means cells edges are arranged
              such that r_edge[i+1] - r_edge[i] = constant,
              log means that cell edges are arranged such that
              r_edge[i+1] / r_edge[i] = constant, powerlaw means that
              cell edges are arranged such that r_edge[i+1]^p -
              r_edge[i]^p = constant for some powerlaw index p
           pidx : float
              powerlaw index for powerlaw grid; ignored if gtype is
              not 'powerlaw'
        """

        # Initialize pointer
        self.c_grid = None

        # Load library
        self.lib = loadlib()

        # Construct the c++ object
        if gtype == 'linear':
            self.c_grid = self.lib.c_grid_linear_alloc(
                nr, nmu, r_min, r_max, mu_max)
            self.pidx = None
        elif gtype == 'log':
            self.c_grid = self.lib.c_grid_log_alloc(
                nr, nmu, r_min, r_max, mu_max)
            self.pidx = None
        elif gtype == 'powerlaw' :
            self.c_grid = self.lib.c_grid_powerlaw_alloc(
                nr, nmu, r_min, r_max, mu_max, pidx)
            self.pidx = pidx
        else:
            raise ValueError(
                "gtype must be 'linear', 'log', or 'powerlaw'")

    # Destructor
    def __del__(self):
        if self.c_grid != None:
            self.lib.c_grid_free(self.c_grid)

    # Convenience routines that implement the x->r mapping
    def r2x(self, r):
        """
        Converts r to x

        Parameters:
           r : float or array
              radial position(s) to be converted

        Returns:
           x : float or array
              x coordinates corresponding to input r
        """
        if self.gtype == 'linear':
            return (r-self.r_e[0]) / (self.r_e[1] - self.r_e[0])
        elif self.gtype == 'log':
            return np.log(r/self.r_e[0])
        else:
            return (r/self.r_e[0])**self.pidx

    def x2r(self, x):
        """
        Converts x to 2

        Parameters:
           x : float or array
              x position(s) to be converted

        Returns:
           r : float or array
              r coordinates corresponding to input x
        """
        if self.gtype == 'linear':
            return (self.r_e[1] - self.r_e[0]) * x + self.r_e[0]
        elif self.gtype == 'log':
            return self.r_e[0] * np.exp(x)
        else:
            return self.r_e[0] * x**(1./self.pidx)
            
    # Access to data
    @property
    def nr(self):
        return self.lib.c_grid_nr(self.c_grid)
    @nr.setter
    def nr(self, val):
        raise AttributeError("nr is read-only")
    @property
    def nmu(self):
        return self.lib.c_grid_nmu(self.c_grid)
    @nmu.setter
    def nmu(self, val):
        raise AttributeError("nmu is read-only")
    @property
    def dx(self):
        return self.lib.c_grid_dx(self.c_grid)
    @dx.setter
    def dx(self, val):
        raise AttributeError("dx is read-only")
    @property
    def dmu(self):
        return self.lib.c_grid_dmu(self.c_grid)
    @dmu.setter
    def dmu(self, val):
        raise AttributeError("dmu is read-only")
    @property
    def gtype(self):
        gt = self.lib.c_grid_gtype(self.c_grid)
        if gt == 0:
            return "linear"
        elif gt == 1:
            return "log"
        elif gt == 2:
            return "powerlaw"
        else:
            return "invalid grid type"
    @gtype.setter
    def gtype(self, val):
        raise AttributeError("gtype is read-only")
    @property
    def r_e(self):
        return npct.as_array(self.lib.c_grid_r_e_vec(self.c_grid),
                             shape=(self.nr+1,))
    @r_e.setter
    def r_e(self, val):
        raise AttributeError("r_e is read-only")
    @property
    def r_c(self):
        return npct.as_array(self.lib.c_grid_r_c_vec(self.c_grid),
                             shape=(self.nr,))
    @r_c.setter
    def r_c(self, val):
        raise AttributeError("r_c is read-only")
    @property
    def mu_e(self):
        return npct.as_array(self.lib.c_grid_mu_e_vec(self.c_grid),
                             shape=(self.nmu+1,))
    @mu_e.setter
    def mu_e(self, val):
        raise AttributeError("mu_e is read-only")
    @property
    def mu_c(self):
        return npct.as_array(self.lib.c_grid_mu_c_vec(self.c_grid),
                             shape=(self.nmu,))
    @mu_c.setter
    def mu_c(self, val):
        raise AttributeError("mu_c is read-only")        
    @property
    def x_e(self):
        return npct.as_array(self.lib.c_grid_x_e_vec(self.c_grid),
                             shape=(self.nr+1,))
    @x_e.setter
    def x_e(self, val):
        raise AttributeError("x_e is read-only")
    @property
    def x_c(self):
        return npct.as_array(self.lib.c_grid_x_c_vec(self.c_grid),
                             shape=(self.nr,))
    @x_c.setter
    def x_c(self, val):
        raise AttributeError("x_c is read-only")
    @property
    def dxdr_e(self):
        return npct.as_array(self.lib.c_grid_dxdr_e_vec(self.c_grid),
                             shape=(self.nr+1,))
    @dxdr_e.setter
    def dxdr_e(self, val):
        raise AttributeError("dxdr_e is read-only")
    @property
    def dxdr_c(self):
        return npct.as_array(self.lib.c_grid_dxdr_c_vec(self.c_grid),
                             shape=(self.nr,))
    @dxdr_c.setter
    def dxdr_c(self, val):
        raise AttributeError("dxdr_c is read-only")
    @property
    def xbar(self):
        return npct.as_array(self.lib.c_grid_xbar_vec(self.c_grid),
                             shape=(self.nr,))
    @xbar.setter
    def xbar(self, val):
        raise AttributeError("xbar is read-only")
    @property
    def x2bar(self):
        return npct.as_array(self.lib.c_grid_x2bar_vec(self.c_grid),
                             shape=(self.nr,))
    @x2bar.setter
    def x2bar(self, val):
        raise AttributeError("x2bar is read-only")
    @property
    def intx(self):
        return npct.as_array(self.lib.c_grid_intx_vec(self.c_grid),
                             shape=(self.nr,))
    @intx.setter
    def intx(self, val):
        raise AttributeError("intx is read-only")
    @property
    def intx2(self):
        return npct.as_array(self.lib.c_grid_intx2_vec(self.c_grid),
                             shape=(self.nr,))
    @intx2.setter
    def intx2(self, val):
        raise AttributeError("intx2 is read-only")
    @property
    def intx_c(self):
        return npct.as_array(self.lib.c_grid_intx_c_vec(self.c_grid),
                             shape=(self.nr,))
    @intx_c.setter
    def intx_c(self, val):
        raise AttributeError("intx_c is read-only")
    @property
    def intx2_c(self):
        return npct.as_array(self.lib.c_grid_intx2_c_vec(self.c_grid),
                             shape=(self.nr,))
    @intx2_c.setter
    def intx2_c(self, val):
        raise AttributeError("intx2_c is read-only")
    @property
    def mubar(self):
        return npct.as_array(self.lib.c_grid_mubar_vec(self.c_grid),
                             shape=(self.nmu,))
    @mubar.setter
    def mubar(self, val):
        raise AttributeError("mubar is read-only")
    @property
    def mu2bar(self):
        return npct.as_array(self.lib.c_grid_mu2bar_vec(self.c_grid),
                             shape=(self.nmu,))
    @mu2bar.setter
    def mu2bar(self, val):
        raise AttributeError("mu2bar is read-only")
    @property
    def V(self):
        return np.transpose(
            npct.as_array(self.lib.c_grid_V_vec(self.c_grid),
                          shape=(self.nmu,self.nr)))
    @V.setter
    def V(self, val):
        raise AttributeError("V is read-only")
    @property
    def A_r(self):
        return np.transpose(
            npct.as_array(self.lib.c_grid_A_r_vec(self.c_grid),
                          shape=(self.nmu,self.nr+1)))
    @A_r.setter
    def A_r(self, val):
        raise AttributeError("A_r is read-only")
    @property
    def A_mu(self):
        return np.transpose(
            npct.as_array(self.lib.c_grid_A_mu_vec(self.c_grid),
                          shape=(self.nmu+1,self.nr)))
    @A_mu.setter
    def A_mu(self, val):
        raise AttributeError("A_mu is read-only")

    # Cylindrical coordinate quantities for convenience
    @property
    def varpi_c(self):
        return np.outer(self.r_c, np.sqrt(1-self.mu_c**2))
    @varpi_c.setter
    def varpi_c(self, val):
        raise AttributeError("varpi_c is read-only")
    @property
    def z_c(self):
        return np.outer(self.r_c, self.mu_c)
    @z_c.setter
    def z_c(self, val):
        raise AttributeError("z_c is read-only")
    @property
    def varpi_re(self):
        return np.outer(self.r_e, np.sqrt(1-self.mu_c**2))
    @varpi_re.setter
    def varpi_re(self, val):
        raise AttributeError("varpi_re is read-only")
    @property
    def z_re(self):
        return np.outer(self.r_e, self.mu_c)
    @z_re.setter
    def z_re(self, val):
        raise AttributeError("z_re is read-only")
    @property
    def varpi_mue(self):
        return np.outer(self.r_c, np.sqrt(1-self.mu_e**2))
    @varpi_mue.setter
    def varpi_mue(self, val):
        raise AttributeError("varpi_mue is read-only")
    @property
    def z_mue(self):
        return np.outer(self.r_c, self.mu_e)
    @z_re.setter
    def z_mue(self, val):
        raise AttributeError("z_mue is read-only")
