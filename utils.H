// A collection of utility macros used throughout

#ifndef _utils_H_
#define _utils_H_

// Shorthands for types
#include <vector>
#include <Eigen/SparseCore>
typedef std::vector<double> dvec;
typedef std::vector<std::vector<double> > ddvec;
typedef dvec::size_type vecsz;
typedef std::vector<vecsz> ivec;
typedef Eigen::SparseMatrix<double, Eigen::RowMajor> SpMat;
typedef std::vector<SpMat> vecSpMat;
typedef std::vector<Eigen::VectorXd> vvec;

// Squares and cubes
#define SQR(x) ((x)*(x))
#define CUBE(x) ((x)*(x)*(x))

// Control enums
typedef enum {
  NO_FLUX,      // Sets all fluxes to zero
  ADV_ONLY,     // Advective flux only
  DIFF_ONLY,    // Diffusive flux only
  ADV_DIFF      // Advective plus diffusive flux
} flux_mode;
typedef enum {  // Verbosity levels
  NO_VERBOSITY,
  MED_VERBOSITY,
  HIGH_VERBOSITY
} verbosity;
typedef enum {  // Advance methods
  FIM,
  IMEX
} advance_meth;

// Physical and numerical constants
namespace constants {

  // Physical constants in CGS; CODATA 2014 values, from
  // https://zenodo.org/record/22826#.W7s11RMzbs0
  const double G  = 6.67408e-8;
  const double KB = 1.3806485e-16;
  const double MH = 1.66053904e-24;
  const double C  = 2.99792458e10;

  // Astrophysical constants in CGS, from
  // http://asa.usno.navy.mil/static/files/2014/Astronomical_Constants_2014.pdf
  const double AU = 1.49597870700e13;
  const double MSUN = 1.98847e33;

  // Parameters relevant to minimum mass Solar nebula
  const double MU = 2.33;            // Mean molecular weight, assuming H2
  const double SIGMA_MMSN = 2200.0;  // MMSN surface density at 1 AU

  // Numerical constants
  const double max = std::numeric_limits<double>::max();
  const double min = std::numeric_limits<double>::min();
}

// Index arithmetic macros
// To understand these, note that there are several types of arrays
// used in this code, corresponding to physical quantities at various
// locations and of various types, each with a packing chosen to
// optimize speed.
//
// -- quantities located at cell centers, and where the quantity does
//    not depend on the grain size; there are packed with radial
//    position as the fastest changing dimension, angular as the
//    slowest changing dimension (i.e., points that are neighbors in
//    the radial direction are also neighbors in memory)
// -- quantities located at the radial edges of cells, and where the
//    quantity does not depend on the grain size; packed as the
//    previous quantity
// -- quantities located at the angular edges of cells, and where the
//    quantity does not depend on the grain size; packed as the
//    previous quantity
// -- versions of all of the preceding where the quantity DOES depend
//    on the grain size, and thus there is an extra index; these
//    arrays are packed with radius as the fastest changing dimension,
//    angle as the middle dimension, and grain size bin as the slowest
//    dimension
// -- PPM coefficients located at cell centers that depend on grain size,
//    AND where there are 3 coefficients per grain size bin per cell;
//    these are packed to place the 3 PPM coefficients for a single
//    grain size and cell next to each other in memory, then in
//    radius, then in angle, then in grain size
//
// With this understanding, we can define the naming convention used
// for these macros:
//
// First letter indicates whether this is a quantity that takes a dust
// size bin index and whether it is a PPM coefficient that has 3
// numbers per grain size per cell: I = no dust index, D = dust index,
// P = PPM coefficient
//
// Second letter = E or C, indicating if this is an edge-centered or
// cell centered quantity
//
// Third part indicates if this quantity is displaced from cell
// centers in the radial or angular (mu) direction; R = displaced in
// radial direction, MU = displaced in angular direction, nothing = no
// displacement. 
//
// Fourth part: M or P indicates if the displacement is in the minus
// (M) or plus (P) direction. For edge-centered quantities,
// displacement specifies if the quantity is on the upper or lower
// face of the corresponding cell, while for cell-centered quantities
// it indicates that the quantities is displaced to the adjacent R or
// MU cell center. This is only present if there is a displacement.
//
// Thus for example:
//
// IC = index corresponding to cell i,j in a cell-centered array
// ICRM = index corresponding to cell i-1,j in a cell-centered array
// ICMUP = index corresponding to cell i,j+1 in a cell-centered array
// IERM = index corresponding to cell i-1/2,j in an edge-centered array
// IERP = index corresponding to cell i+1/2,j in an edge-centered array
// IEMUM = index corresponding to cell i,j-1/2 in an edge-centered array
// IEMUP = index corresponding to cell i,j+1/2 in an edge-centered array
// DC = IC, evalauted for dust species k
// DCRM = ICRM, evaluated for dust species k
// ...
// PC = index in PPM coefficient array for first coefficient for cell
//      i,j for dust species k
#define IC(i,j,nr)          ((i) + (j)*(nr))
#define ICRM(i,j,nr)        (((i)-1) + (j)*(nr))
#define ICRP(i,j,nr)        (((i)+1) + (j)*(nr))
#define ICMUM(i,j,nr)       ((i) + ((j)-1)*(nr))
#define ICMUP(i,j,nr)       ((i) + ((j)+1)*(nr))
#define IERM(i,j,nr)        ((i) + (j)*((nr)+1))
#define IERP(i,j,nr)        (((i)+1) + (j)*((nr)+1))
#define IEMUM(i,j,nr)       ((i) + (j)*(nr))
#define IEMUP(i,j,nr)       ((i) + ((j)+1)*(nr))
#define DC(i,j,k,nr,nmu)    ((i) + (j)*(nr) + (k)*(nr)*(nmu))
#define DCRM(i,j,k,nr,nmu)  (((i)-1) + (j)*(nr) + (k)*(nr)*(nmu))
#define DCRP(i,j,k,nr,nmu)  (((i)+1) + (j)*(nr) + (k)*(nr)*(nmu))
#define DCMUM(i,j,k,nr,nmu) ((i) + ((j)-1)*(nr) + (k)*(nr)*(nmu))
#define DCMUP(i,j,k,nr,nmu) ((i) + ((j)+1)*(nr) + (k)*(nr)*(nmu))
#define DERM(i,j,k,nr,nmu)  ((i) + (j)*((nr)+1) + (k)*((nr)+1)*(nmu)) 
#define DERP(i,j,k,nr,nmu)  (((i)+1) + (j)*((nr)+1) + (k)*((nr)+1)*(nmu))
#define DEMUM(i,j,k,nr,nmu) ((i) + (j)*(nr) + (k)*(nr)*((nmu)+1))
#define DEMUP(i,j,k,nr,nmu) ((i) + ((j)+1)*(nr) + (k)*(nr)*((nmu)+1))
#define PC(i,j,k,nr,nmu)    (3*((i)+(j)*(nr)+(k)*(nr)*(nmu)))
#define PCRM(i,j,k,nr,nmu)    (3*((i)-1+(j)*(nr)+(k)*(nr)*(nmu)))
#define PCRP(i,j,k,nr,nmu)    (3*((i)+1+(j)*(nr)+(k)*(nr)*(nmu)))
#define PCMUM(i,j,k,nr,nmu)    (3*((i)+((j)-1)*(nr)+(k)*(nr)*(nmu)))
#define PCMUP(i,j,k,nr,nmu)    (3*((i)+((j)+1)*(nr)+(k)*(nr)*(nmu)))

#endif
// _ifdef_utils_H_
