// Implementation of the dustdisk class

// Includes, namespace, typedefs
#include <cmath>
#include "dustdisk.H"
#include <iostream>
#include "gsl/gsl_integration.h"

using namespace std;

// Utility function needed by the GSL to evluate an integral
double integrand(double z, void *f_) {
  double f = *((double *) f_);
  return exp(-0.5*SQR(z) - f*(exp(0.5*SQR(z)) - 1));
}

// The constructor
dustdisk::dustdisk(const grid *_grd, const double _M, const double _p,
		   const double _q, const double _eps, const double _T0,
		   const double _alpha, const double _Mdot,
		   const double _L, const double _rho_d,
		   const dvec &_a, const dvec &_f_a,
		   const double _rho_floor,
		   const Eigen::VectorXd &_rho_c) :
  gasdisk(_grd, _M, _p, _q, _eps, _T0, _alpha, _Mdot),
  na(_a.size()),
  L(_L),
  rho_d(_rho_d),
  a(_a),
  f_a(_f_a) {

  // Set vectors to correct size
  init_vectors();

  // Initialize opacity and beta_0
  for (vecsz k=0; k<na; k++) {
    kappa_d[k] = 3.0 / (4.0 * rho_d * a[k]);
    beta_0[k] = 3.0*L / (16.0 * M_PI * rho_d * a[k] * constants::G * M *
			 constants::C);
  }

  // Set floor density; note that this doesn't change any density
  // values, it just computes a floor that the simulation routines can
  // make use of
  set_floor(_rho_floor);

  // Set initial densities
  if (_rho_c.size() == 0)
    set_rho_init();
  else
    rho_c = _rho_c;
}


// Constructor starting from an existing gas disk
dustdisk::dustdisk(const gasdisk *_gd, const double _L,
		   const double _rho_d,
		   const dvec &_a, const dvec &_f_a,
		   const double _rho_floor,
		   const Eigen::VectorXd &_rho_c) :
  gasdisk(_gd->grd_(), _gd->M_(), _gd->p_(), _gd->q_(),
	  _gd->eps_(), _gd->T0_(), _gd->alpha_(), _gd->Mdot_()),
  na(_a.size()),
  L(_L),
  rho_d(_rho_d),
  a(_a),
  f_a(_f_a) {
  
  // Set vectors to correct size
  init_vectors();

  // Initialize opacity and beta_0
  for (vecsz k=0; k<na; k++) {
    kappa_d[k] = 3.0 / (4.0 * rho_d * a[k]);
    beta_0[k] = 3.0*L / (16.0 * M_PI * rho_d * a[k] * constants::G * M *
			 constants::C);
  }

  // Set floor density; note that this doesn't change any density
  // values, it just computes a floor that the simulation routines can
  // make use of
  set_floor(_rho_floor);

  // Set initial densities
  if (_rho_c.size() == 0)
    set_rho_init();
  else
    rho_c = _rho_c;
}


// Constructor that takes total grain mass and index of grain mass
// distribution as inputs
dustdisk::dustdisk(const grid *_grd, const double _M, const double _p,
		   const double _q, const double _eps, const double _T0,
		   const double _alpha, const double _Mdot,
		   const double _L, const double _rho_d,
		   const dvec &_a, const double fdust, const double qd,
		   const double _rho_floor,
		   const Eigen::VectorXd &_rho_c) :
  gasdisk(_grd, _M, _p, _q, _eps, _T0, _alpha, _Mdot),
  na(_a.size()),
  L(_L),
  rho_d(_rho_d),
  a(_a) {

  // Set vectors to correct size
  init_vectors();

  // Set grain mass fractions
  f_a = compute_f_a(fdust, qd);

  // Initialize opacity and beta_0
  for (vecsz k=0; k<na; k++) {
    kappa_d[k] = 3.0 / (4.0 * rho_d * a[k]);
    beta_0[k] = 3.0*L / (16.0 * M_PI * rho_d * a[k] * constants::G * M *
			 constants::C);
  }

  // Set floor density; note that this doesn't change any density
  // values, it just computes a floor that the simulation routines can
  // make use of
  set_floor(_rho_floor);

  // Set initial densities
  if (_rho_c.size() == 0)
    set_rho_init();
  else
    rho_c = _rho_c;
}


// Constructor starting from an existing gas disk and taking a total
// dust mass fraction and index as inputs
dustdisk::dustdisk(const gasdisk *_gd, const double _L,
		   const double _rho_d,
		   const dvec &_a, const double fdust,
		   const double qd, const double _rho_floor,
		   const Eigen::VectorXd &_rho_c) :
  gasdisk(_gd->grd_(), _gd->M_(), _gd->p_(), _gd->q_(),
	  _gd->eps_(), _gd->T0_(), _gd->alpha_(), _gd->Mdot_()),
  na(_a.size()),
  L(_L),
  rho_d(_rho_d),
  a(_a) {
  
  // Set vectors to correct size
  init_vectors();

  // Set grain mass fractions
  f_a = compute_f_a(fdust, qd);
  
  // Initialize opacity and beta_0
  for (vecsz k=0; k<na; k++) {
    kappa_d[k] = 3.0 / (4.0 * rho_d * a[k]);
    beta_0[k] = 3.0*L / (16.0 * M_PI * rho_d * a[k] * constants::G * M *
			 constants::C);
  }

  // Set floor density; note that this doesn't change any density
  // values, it just computes a floor that the simulation routines can
  // make use of
  set_floor(_rho_floor);

  // Set initial densities
  if (_rho_c.size() == 0)
    set_rho_init();
  else
    rho_c = _rho_c;
}


// Method to set vectors and matrices to correct size for this grid;
// note that the diffusion matrix is set up to be nr * nmu by nr * nmu
// in total size, meaning that there are no ghost zones; we do not
// need them because we are setting the diffusion coefficient across
// the boundaries to zero. Also note that we use a 5 point stencil.
void dustdisk::init_vectors() {
  kappa_d.resize(na);
  beta_0.resize(na);
  rho_c.resize(nr*nmu*na);
}


// Method to calculate the mass fraction in each size bin
dvec dustdisk::compute_f_a(const double fdust, const double qd) const {

  // Initialize output vector
  dvec f_bin(a.size());

  // Handle trivial case with only one size bin
  if (a.size() == 1) {
    f_bin[0] = fdust;
    return f_bin;
  }

  // Case with > 1 bin; handle the first bin by extrapolating
  double alo = a[0] * sqrt(a[0] / a[1]);
  double ahi = sqrt(a[0] * a[1]);
  f_bin[0] = pow(ahi, 3*(2+qd)) - pow(alo, 3*(2+qd));
  double fdsum = f_bin[0];

  // Handle middle bins
  for (vecsz k=1; k<na-1; k++) {
    alo = sqrt(a[k-1] * a[k]);
    ahi = sqrt(a[k] * a[k+1]);
    f_bin[k] = pow(ahi, 3*(2+qd)) - pow(alo, 3*(2+qd));
    fdsum += f_bin[k];
  }

  // Handle last bin
  if (na > 2) {
    alo = sqrt(a[na-1] * a[na-2]);
    ahi = a[na-1] * sqrt(a[na-1] / a[na-2]);
    f_bin[na-1] = pow(ahi, 3*(2+qd)) - pow(alo, 3*(2+qd));
    fdsum += f_bin[na-1];
  }

  // Normalize
  for (vecsz k=0; k<na; k++) f_bin[k] *= fdust / fdsum;

  // Return
  return f_bin;
}  


// Method to initialize the density distribution
void dustdisk::set_rho_init() {
#if defined(_OPENMP)
#  pragma omp parallel
#endif
  {
    // Allocate GSL workspace
    gsl_integration_workspace *w = gsl_integration_workspace_alloc(1024);

    // Loop over cells
#if defined(_OPENMP)
#  pragma omp for
#endif
    for (vecsz j=0; j<nmu; j++) {
      for (vecsz i=0; i<nr; i++) {

	// Calculate total dust density at cell center
	double varpi = grd->r_c[i] * sqrt(1.0 - SQR(grd->mu_c[j]));
	double Sigma_g = eps * constants::SIGMA_MMSN *
	  pow(varpi/constants::AU, p);
	double Omega_mid = sqrt(constants::G*M/CUBE(varpi));
	double T = T0 * pow(varpi/constants::AU, q);
	double cs = sqrt(constants::KB*T/(constants::MU*constants::MH));
	double h_g = cs / Omega_mid;
	double rho_g_mid = Sigma_g / (sqrt(2.0*M_PI) * h_g);
      
	// Assign mass to individual species
	for (vecsz k=0; k<na; k++) {

	  // Get midplane stopping time and Schmidt number
	  double Ts_mid = rho_d * a[k] / (rho_g_mid * cs) * Omega_mid;
	  double Sc_mid = 1.0 + SQR(Ts_mid);
	  
	  // Get normalization for dust column
	  double f = Sc_mid*Ts_mid / alpha;
	  double res, err;
	  gsl_function F;
	  F.function = &integrand;
	  F.params = &f;
	  gsl_integration_qagiu(&F, 0.0, 1.0e-15, 1.0e-10, 1024, w, &res, &err);
	  double norm = 0.5 * f_a[k] * (Sigma_g / h_g) / res;
	  
	  // Set column in the cell
	  gsl_integration_qag(&F, grd->r_c[i]*grd->mu_e[j]/h_g,
			      grd->r_c[i]*grd->mu_e[j+1]/h_g,
			      1.0e-15, 1.0e-10, 1024,
			      GSL_INTEG_GAUSS61, w, &res, &err);
	  rho_c[DC(i,j,k,nr,nmu)] = norm * res /
	    (grd->r_c[i] * grd->dmu / h_g);
	}
      }
    }

    // Free GSL workspace
    gsl_integration_workspace_free(w);
  }
}

// Method to set the density floor
void dustdisk::set_floor(const double _rho_floor) {

  // If input floor value is positive, just set to this value;
  // otherwise calculate floor value from optical depth
  if (_rho_floor >= 0.0) rho_floor = _rho_floor;
  else {
    double kappa_d_tot = 0.0;
    for (vecsz k=0; k<na; k++) kappa_d_tot += kappa_d[k];
    rho_floor = -_rho_floor / (kappa_d_tot * (grd->r_e[nr-1] - grd->r_e[0]));
  }
}


// Method to floor the density
void dustdisk::apply_floor() {
#if defined(_OPENMP)
#  pragma omp parallel for
#endif
  for (vecsz i=0; i<nr*nmu*na; i++) rho_c[i] = max(rho_c[i], rho_floor);
}


// Utility method to calculate the projection of the dust density
// distribution onto the plane
dvec dustdisk::proj(const dvec &varpi_) const {

  // Get list of points to project; default is to project onto the
  // radial positions at cell centers, excluding parts of the grid
  // where part of the disc is missing due to the spherical coordinate
  // system
  dvec varpi;
  if (varpi_.size() > 0) varpi = varpi_;
  else {
    double varpi_max = grd->r_e[nr] * sqrt(1 - SQR(grd->mu_e[nmu]));
    for (vecsz i=0; i<grd->r_c.size(); i++) {
      if (grd->r_c[i] < varpi_max) varpi.push_back(grd->r_c[i]);
      else break;
    }
  }

  // Loop over projection points
  dvec col(na*varpi.size());
#if defined(_OPENMP)
#   pragma omp parallel for
#endif
  for (vecsz n=0; n<varpi.size(); n++) {

    // Find cell at which to start walk
    double x = grd->r2x(varpi[n]);
    vecsz i = (vecsz) floor((x - grd->x_e[0]) / grd->dx);

    // Start walking at midplane
    vecsz j = 0;
    double z = 0;
    
    // Walk upward, accumulating column, stopping when we exit the
    // grid
    while (i < nr && j < nmu) {

      // Get distance from current location to cell radial and angular
      // edges
      double dist_r = sqrt(SQR(grd->r_e[i+1]) - SQR(varpi[n])) - z;
      double dist_mu = varpi[n] *
	       grd->mu_e[j+1] / sqrt(1 - SQR(grd->mu_e[j+1])) - z;

      // Add column and move point
      if (dist_r < dist_mu) {
	for (vecsz k=0; k<na; k++)
	  col[k*varpi.size() + n] += rho_c[DC(i,j,k,nr,nmu)] * dist_r;
	z = sqrt(SQR(grd->r_e[i+1]) - SQR(varpi[n]));
	i++;
      } else {
	for (vecsz k=0; k<na; k++)
	  col[k*varpi.size() + n] += rho_c[DC(i,j,k,nr,nmu)] * dist_mu;
	z = varpi[n] * grd->mu_e[j+1] / sqrt(1 - SQR(grd->mu_e[j+1]));
	j++;
      }

    }
  }

  // Multiply all columns by two for symmetry
  for (vecsz i = 0; i<col.size(); i++) col[i] *= 2;

  // Return column
  return col;
}
