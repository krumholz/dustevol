"""
This module defines the python wrapper for the c++ simulation class
"""

import numpy as np
import numpy.ctypeslib as npct
import glob
from interface import loadlib
from dustdisk import dustdisk
from ctypes import c_double

##################################################################
# Wrapper for the simulation class
##################################################################

class simulation(object):
    """
    This class contains the machinery to do a simulation on a dust
    disk

    Attributes:

       (size parameters)
       nr : int
          number of cells in radial direction
       nmu : int
          number of cells in polar angle direction
       na : int
          number of dust grain size bins

       (control parameters)
       t : float
          the current simulation time
       ctr : int
          number of time steps taken
       chknum : int
          number of checkpoints written
       method : 'IMEX' | 'FIM'
          advance method: IMEX = implicit-explicit, FIM = fully
          implicit
       dt_min : float
          minimum time step allowed
       max_dt_fac : float
          maximum factor by which time step can increase per advance
       max_step : int
          maximum number of time steps to allow per call to run
       mat_tol : float
          tolerance for the implicit matrix solver
       Theta : float
          time-centering parameter for the implicit part of the
          update; 0 = forwards Euler update (unstable, use for testing
          purposes only), 0.5 = Crank-Nicolson update (2nd order), 1 =
          backwards Euler update (stable but 1st order); defaults to
          0.5
       r_flux_off : bool
          if True, calculation of radial fluxes is disabled; this
          option is intended for testing purposes
       tol : float
          tolerance for the non-linear iterative solver in the FIM method
       max_iter : int
          maximum number of iterations for the FIM solver
       drho_max : float
          maximum fractional change in the density for FIM time
          stepping
       anderson_ord : int
          Anderson acceleration order for the FIM solver; 0 = no
          acceleration
       cfl : float
          CFL number for the IMEX solver

       (internal data -- read-only)
       ppm_coef_r : array, shape (3, nr, nmu, na)
          coefficients for PPM reconstruction of cells in radial
          direction
       ppm_coef_mu : array, shape (3, nr, nmu, na)
          coefficients for PPM reconstruction of cells in mu
          direction
       tau_re : array, shape (nr+1, nmu)
          optical depth to radial cell edges
       tau_mue : array, shape (nr, nmu+1)
          optical depth to mu cell edges
       flux_re : array, shape (nr+1, nmu, na)
          mass flux for each dust species across radial cell
          faces
       flux_mue : array, shape (nr, nmu+1, na)
          mass flux for each dust species across angular cell
          faces
    """

    def __init__(self, dd=None, chkname=None, chknum=None):
        """
        Construct a simulation object from either a dust disk object
        or a checkpoint

        Parameters
           dd : class dust_disk
              a dust disk object around which the simulation is to be
              constructed; either this or check must be set
           chkname : str
              base name of the checkpoint file from which to restart;
              either this or dd must be set
           chknum : int
              number of checkpoint file from which to restart; it not
              set, the restart is from the most latest checkpoint
        """

        # Load library if needed
        self.c_simulation = None
        self.lib = loadlib()

        # If restarting from a checkpoint, get the checkpoint number
        # if one is not given
        if chkname is not None and chknum is None:
            fnames = glob.glob(chkname+"[0-9]*.npz")
            if len(fnames) == 0:
                raise IOError("checkpoint '{:s}'".
                              format(chkname)+" not found")
            fnames.sort()
            chknum = int(fnames[-1][len(chkname):-4])

        # If restarting, read the dusk disk file
        if chkname is not None:
            dd = dustdisk(savefile=chkname+"_dd{:05d}.npz".format(chknum))
            
        # Construct c++ simulation object
        self.c_simulation = self.lib.c_simulation_alloc(dd.c_dustdisk)

        # Save a pointer to the dustdisk, which we can use to access
        # its methods
        self.dd = dd

        # If restarting, read in metadata; if not just initialize
        # checkpoint counter
        if chkname is None:
            self.chknum = 0
        else:
            data = np.load(chkname+"{:05d}.npz".format(chknum), 
                           allow_pickle=True)
            self.chknum = data['chknum']
            self.Theta = data['Theta']
            self.tol = data['tol']
            self.max_dt_fac = data['max_dt_fac']
            self.drho_max = data['drho_max']
            self.dt_min = data['dt_min']
            self.cfl = data['cfl']
            self.r_flux_off = data['r_flux_off']
            try:
                # python 3 version
                self.method = str(data['method'], 'utf-8')
            except:
                # python 2 version
                self.method = str(data['method'])
            self.anderson_ord = data['anderson_ord']
            self.max_iter = data['max_iter']
            self.max_step = data['max_step']
            try:
                self.verbosity = str(data['verb'], 'utf-8')
            except:
                self.verbosity = str(data['verb'])
            self.time = data['time']
            self.step = data['step']


    def __del__(self):
        if self.c_simulation is not None:
            self.lib.c_simulation_free(self.c_simulation)

    # Methods to invoke c++ methods that are part of a time step
    def set_ppm_coef(self):
        self.lib.c_simulation_set_ppm_coef(self.c_simulation)
    def set_tau(self):
        self.lib.c_simulation_set_tau(self.c_simulation)
    def set_flux(self, dt, flux_mode = 'adv_diff'):
        """
        Calculate fluxes and store them in flux_re and flux_mue, where
        flux is defined here as the mass per unit area transported
        across the cell face in the input time dt; note that this
        routine requires that set_ppm_coef and set_tau have previously
        been called. The results of calling this method without doing
        so are undefined.

        Parameters:
           dt : float
              time step
           flux_mode : 'zero_flux' | 'adv_only' | 'diff_only' |
                       'adv_diff'
              type of flux computation: 'zero_flux' = all fluxes set
              to zero, 'adv_only' = compute advective fluxes only,
              'diff_only' = compute diffusive fluxes only, 'adv_diff'
              = compute sum of advective and diffusive fluxes

        Returns:
           vmax : double
              maximum velocity anywhere in the domain; only returned
              if flux_mode is set to 'adv_only' or 'adv_diff'
        """
        if flux_mode == 'zero_flux':
            ret = self.lib.c_simulation_set_flux(self.c_simulation, dt, 0)
            return
        elif flux_mode == 'adv_only':
            ret = self.lib.c_simulation_set_flux(self.c_simulation, dt, 1)
            return ret
        elif flux_mode == 'diff_only':
            ret = self.lib.c_simulation_set_flux(self.c_simulation, dt, 2)
            return
        elif flux_mode == 'adv_diff':
            ret = self.lib.c_simulation_set_flux(self.c_simulation, dt, 3)
            return ret
        else:
            raise ValueError('unknown flux_mode')
    def set_rhs_old(self):
        self.lib.c_simulation_set_rhs_old(self.c_simulation)
    def set_rhs(self):
        self.lib.c_simulation_set_rhs(self.c_simulation)

    def advance_diffusion(self, dt):
        """
        Advance the simulation through a time dt, doing only the
        diffusion update

        Parameters:
           dt : float
              size of time step

        Returns:
           success : bool
              True if advance succeeded, False if it failed
        """
        return self.lib.c_simulation_advance_diffusion(
            self.c_simulation, dt)
    
    def advance_advection(self, dt):
        """
        Advance the simulation through a time dt, doing only the
        advection update

        Parameters:
           dt : float
              size of time step

        Returns:
           dr_over_v : float
              minimum ratio of cell size to velocity at cell face over
              the entire computational grid
        """
        return self.lib.c_simulation_advance_advection(
            self.c_simulation, dt)
        
    def advance(self, dt):
        """
        Advance the simulation through a single time step

        Parameters:
           dt : float
              size of time step

        Returns:
           dt_new : float
              estimate of the new time step if the step succeeded; a
              negative return value indicates that the step failed to
              converge
        """
        if self.method == 'IMEX':
            return self.lib.c_simulation_advance_IMEX(
                self.c_simulation, dt)
        else:
            return self.lib.c_simulation_advance_FIM(
                self.c_simulation, dt)
        
    def run(self, trun, tsave=None, dt0=None):
        """
        Run the simulation for a time trun, updating the state of the
        dust densiy and the simulation time

        Parameters:
           trun : float
              amount of time to advance the simulation
           tsave : arraylike
              times at which to save a snapshot; if None, no snapshots
              are saved
           dt0 : float
              size of first time step; if left as None, this will be
              estimated automatically

        Returns:
           dt : float
              the size of the final time step
           hist : array, shape (nr,nmu,na,len(tsave))
              history array containing densities in every cell at times
              tsave; not returned if tsave is None, and if the
              simulation aborts before reaching some times in tsave,
              the portions of the history array corresponding to times
              that the simulation did not reach will be filled with
              NaN's
        """

        # Allocate array to hold history
        if tsave is not None:
            ts = np.atleast_1d(np.array(tsave))
            nhist = ts.size
            if nhist > 1:
                if np.any(ts[1:] <= ts[:-1]):
                    raise ValueError("tsave must be strictly increasing")
            if np.any(ts > trun):
                raise ValueError("all elements of tsave must be <= trun")
            hist = np.zeros(self.nr*self.nmu*self.na*nhist,
                            dtype=c_double)
        else:
            nhist = 0
            ts = np.zeros(1)
            hist = np.zeros(1)

        # Run simulation
        if dt0 is None: dt0 = -1.0
        dt = self.lib.c_simulation_run(self.c_simulation,
                                     trun, nhist, ts, hist, dt0)

        # Reshape and fill any emptry entries in the history output
        if nhist > 0:
            hist = np.transpose(hist.reshape(nhist,self.na,self.nmu,self.nr))
            if self.time < trun:
                nsave = np.sum(tsave <= self.time)
                hist[:,:,:,nsave:] = np.nan

        # Return
        if nhist > 0:
            return dt, hist
        else:
            return dt

    # Method to write out the state of a simulation so we can read it
    # back in later
    def checkpoint(self, chkname='sim', nd=5):
        """
        Write a checkpoint from which a simulation can be restarted

        Parameters
           chkname : str
              file name for checkpoint; the checkpoint will consist of
              the two files chknameNNNNN.npz and
              chkname_ddNNNNN.npz, where the value of NNNNN is the
              value of the checkpoint counter, and the nubmer of
              digits is given by nd
           nd : int
              number of digits used in the file name for the
              checkpoint counter

        Returns
           Nothing
        """
        fstr = "{{:0{:d}}}".format(nd)
        np.savez(chkname+fstr.format(self.chknum),
                 chknum=self.chknum+1,
                 Theta=self.Theta,
                 tol=self.tol,
                 max_dt_fac=self.max_dt_fac,
                 drho_max=self.drho_max,
                 dt_min=self.dt_min,
                 cfl=self.cfl,
                 r_flux_off=self.r_flux_off,
                 method=self.method,
                 anderson_ord=self.anderson_ord,
                 max_iter=self.max_iter,
                 max_step=self.max_step,
                 verb=self.verbosity,
                 time=self.time,
                 step=self.step)
        self.dd.write_state(chkname+'_dd'+fstr.format(self.chknum))
        self.chknum += 1

        
    # Methods to compute velocities
    def vr(self):
        v = np.zeros((self.nr+1)*self.nmu*self.na, dtype=c_double)
        self.lib.c_simulation_vr(self.c_simulation, v)
        return np.transpose(v.reshape((self.na, self.nmu,self.nr+1)))
    def vmu(self):
        v = np.zeros(self.nr*(self.nmu+1)*self.na, dtype=c_double)
        self.lib.c_simulation_vmu(self.c_simulation, v)
        return np.transpose(v.reshape((self.na, self.nmu+1,self.nr)))
        
    # Access to data and control parameters
    @property
    def nr(self):
        return self.lib.c_simulation_nr(self.c_simulation)
    @nr.setter
    def nr(self, val):
        raise AttributeError("nr is read-only")
    @property
    def nmu(self):
        return self.lib.c_simulation_nmu(self.c_simulation)
    @nmu.setter
    def nmu(self, val):
        raise AttributeError("nmu is read-only")
    @property
    def na(self):
        return self.lib.c_simulation_na(self.c_simulation)
    @na.setter
    def na(self, val):
        raise AttributeError("na is read-only")
    @property
    def ppm_coef_r(self):
        return np.transpose(
            npct.as_array(self.lib.c_simulation_ppm_coef_r(self.c_simulation),
                          shape=(self.na,self.nmu,self.nr,3)))
    @ppm_coef_r.setter
    def ppm_coef_r(self, val):
        raise AttributeError("ppm_coef_r is read-only")
    @property
    def ppm_coef_mu(self):
        return np.transpose(
            npct.as_array(self.lib.c_simulation_ppm_coef_mu(self.c_simulation),
                          shape=(self.na,self.nmu,self.nr,3)))
    @ppm_coef_mu.setter
    def ppm_coef_mu(self, val):
        raise AttributeError("ppm_coef_mu is read-only")
    @property
    def tau_re(self):
        return np.transpose(
            npct.as_array(self.lib.c_simulation_tau_re(self.c_simulation),
                          shape=(self.nmu,self.nr+1)))
    @tau_re.setter
    def tau_re(self, val):
        raise AttributeError("tau_re is read-only")
    @property
    def tau_mue(self):
        return np.transpose(
            npct.as_array(self.lib.c_simulation_tau_mue(self.c_simulation),
                          shape=(self.nmu+1,self.nr)))
    @tau_mue.setter
    def tau_mue(self, val):
        raise AttributeError("tau_mue is read-only")
    @property
    def flux_re(self):
        return np.transpose(
            npct.as_array(self.lib.c_simulation_flux_re(self.c_simulation),
                          shape=(self.na,self.nmu,self.nr+1)))
    @flux_re.setter
    def flux_re(self, val):
        raise AttributeError("flux_re is read-only")
    @property
    def flux_mue(self):
        return np.transpose(
            npct.as_array(self.lib.c_simulation_flux_mue(self.c_simulation),
                          shape=(self.na,self.nmu+1,self.nr)))
    @flux_mue.setter
    def flux_mue(self, val):
        raise AttributeError("flux_mue is read-only")
    @property
    def rhs_old(self):
        rhs = np.zeros(self.nr*self.nmu*self.na, dtype=c_double)
        self.lib.c_simulation_rhs_old(self.c_simulation, rhs)
        rhs = np.transpose(rhs.reshape(self.na,self.nmu,self.nr))
        return rhs
    @rhs_old.setter
    def rhs_old(self, val):
        raise AttributeError("rhs_old is read-only")
    @property
    def rhs(self):
        rhs_ = np.zeros(self.nr*self.nmu*self.na, dtype=c_double)
        self.lib.c_simulation_rhs(self.c_simulation, rhs_)
        rhs_ = np.transpose(rhs.reshape(self.na,self.nmu,self.nr))
        return rhs_
    @rhs.setter
    def rhs(self, val):
        raise AttributeError("rhs is read-only")
    @property
    def tol(self):
        return self.lib.c_simulation_tol(self.c_simulation)
    @tol.setter
    def tol(self, val):
        self.lib.c_simulation_set_tol(self.c_simulation, val)
    @property
    def mat_tol(self):
        return self.lib.c_simulation_mat_tol(self.c_simulation)
    @tol.setter
    def mat_tol(self, val):
        self.lib.c_simulation_set_mat_tol(self.c_simulation, val)
    @property
    def max_dt_fac(self):
        return self.lib.c_simulation_max_dt_fac(self.c_simulation)
    @max_dt_fac.setter
    def max_dt_fac(self, val):
        self.lib.c_simulation_set_max_dt_fac(self.c_simulation, val)
    @property
    def dt_min(self):
        return self.lib.c_simulation_dt_min(self.c_simulation)
    @dt_min.setter
    def dt_min(self, val):
        self.lib.c_simulation_set_dt_min(self.c_simulation, val)
    @property
    def Theta(self):
        return self.lib.c_simulation_Theta(self.c_simulation)
    @Theta.setter
    def Theta(self, val):
        self.lib.c_simulation_set_Theta(self.c_simulation, val)
    @property
    def drho_max(self):
        return self.lib.c_simulation_drho_max(self.c_simulation)
    @drho_max.setter
    def drho_max(self, val):
        self.lib.c_simulation_set_drho_max(self.c_simulation, val)
    @property
    def cfl(self):
        return self.lib.c_simulation_cfl(self.c_simulation)
    @cfl.setter
    def cfl(self, val):
        self.lib.c_simulation_set_cfl(self.c_simulation, val)
    @property
    def max_iter(self):
        return self.lib.c_simulation_max_iter(self.c_simulation)
    @max_iter.setter
    def max_iter(self, val):
        self.lib.c_simulation_set_max_iter(self.c_simulation, val)
    @property
    def max_step(self):
        return self.lib.c_simulation_max_step(self.c_simulation)
    @max_step.setter
    def max_step(self, val):
        self.lib.c_simulation_set_max_step(self.c_simulation, val)
    @property
    def verbosity(self):
        v = self.lib.c_simulation_verbosity(self.c_simulation)
        if v == 0:
            return 'None'
        elif v == 1:
            return 'Medium'
        elif v == 2:
            return 'High'
        else:
            return 'Unknown'
    @verbosity.setter
    def verbosity(self, val):
        if val == 'None':
            v = 0
        elif val == 'Medium':
            v = 1
        elif val == 'High':
            v = 2
        else:
            v = val
        if v != 0 and v != 1 and v != 2:
            raise ValueError(
                "Valid values of verbosity are 'None', 'Medium',"
                "'High', 0, 1, or 2")
        self.lib.c_simulation_set_verbosity(self.c_simulation, v)
    @property
    def r_flux_off(self):
        return self.lib.c_simulation_r_flux_off(self.c_simulation)
    @r_flux_off.setter
    def r_flux_off(self, val):
        self.lib.c_simulation_set_r_flux_off(self.c_simulation, val)
    @property
    def anderson_ord(self):
        return self.lib.c_simulation_anderson_ord(self.c_simulation)
    @anderson_ord.setter
    def anderson_ord(self, val):
        self.lib.c_simulation_set_anderson_ord(self.c_simulation, val)
    @property
    def method(self):
        if self.lib.c_simulation_method(self.c_simulation) == 0:
            return "FIM"
        else:
            return "IMEX"
    @method.setter
    def method(self, val):
        if val == "FIM":
            meth = 0
        elif val == "IMEX":
            meth = 1
        else:
            raise ValueError(
                "Valid values of method are 'FIM' (fully implicit) "
                "and 'IMEX' (implicit-explicit)")
        self.lib.c_simulation_set_method(self.c_simulation, meth)
    @property
    def time(self):
        return self.lib.c_simulation_time(self.c_simulation)
    @time.setter
    def time(self, val):
        self.lib.c_simulation_set_time(self.c_simulation, val)
    @property
    def step(self):
        return self.lib.c_simulation_step(self.c_simulation)
    @step.setter
    def step(self, val):
        self.lib.c_simulation_set_step(self.c_simulation, val)

