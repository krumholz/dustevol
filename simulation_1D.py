"""
This module defines the python wrapper to the c++ simulation_1D class
"""

import numpy as np
import numpy.ctypeslib as npct
from interface import loadlib
from ctypes import c_double, c_size_t

class simulation_1D(object):
    """
    This class contains the machinery to do simulations of the
    simplified 1D system

    Attributes:
       nx : float
          number of cells
       t : float
          the current simulation time
       step : int
          number of time steps taken
       cfl : float
          CFL number
       Theta : float
          time centering parameter Theta
       verbosity : integer
          verbosity level
       max_step : int
          maximum number of time steps to take
       ppm : array
          current contents of the PPM coefficient array
       tau : array
          current contents of the optical depth array
    """

    def __init__(self, dd):
        """
        Construct a simulation object
        
        Parameters
           dd : class dustdisk_1D
              the dustdisk_1D object to evolve
        """

        # Load library if needed
        self.c_simulation = None
        self.lib = loadlib()
        
        # Construct c++ simulation object
        self.c_simulation = self.lib.c_simulation_1D_alloc(dd.c_dustdisk)

        # Save a pointer to the dustdisk, which we can use to access
        # its methods
        self.dd = dd
        
    def __del__(self):
        if self.c_simulation is not None:
            self.lib.c_simulation_1D_free(self.c_simulation)
            self.c_simulation = None

    # Methods to invoke c++ methods that are part of a time step
    def set_ppm_coef(self):
        self.lib.c_simulation_1D_set_ppm_coef(self.c_simulation)
    def set_tau(self):
        self.lib.c_simulation_1D_set_tau(self.c_simulation)
    def set_flux(self, dt):
        """
        Compute the time-averaged flux across cell edges

        Parameters
           dt : float
              size of time step
        """
        self.lib.c_simulation_1D_set_flux(self.c_simulation, dt)
    def advance_diffusion(self, dt):
        """
        Advance the simulation through a single diffusion step

        Parameters:
           dt : float
              the amount of time to advance
        """
        self.lib.c_simulation_1D_advance_diffusion(self.c_simulation, dt)
    def advance_advection(self, dt):
        """
        Advance the simulation through a single advection step,
        returning the maximum advection velocity

        Parameters:
           dt : float
              the amount of time to advance

        Returns:
           vmax : float
              maximum velocity at any cell interface
        """
        return self.lib. \
            c_simulation_1D_advance_advection(self.c_simulation, dt)
    def advance(self, dt):
        """
        Advance the simulation one time step

        Parameters:
           dt : float
              the amount of time to advance

        Returns:
           dt_new : float
              the time step for the next advance
        """
        return self.lib. \
            c_simulation_1D_advance(self.c_simulation, dt)

    def run(self, trun, tsave=None, dt0=None):
        """
        Run the simulation for a time trun, updating the state of the
        dust densiy and the simulation time

        Parameters:
           trun : float
              amount of time to advance the simulation
           tsave : arraylike
              times at which to save a snapshot; if None, no snapshots
              are saved
           dt0 : float
              size of first time step; if left as None, this will be
              estimated automatically

        Returns:
           dt : float
              the size of the final time step
           hist : array, shape (nx,len(tsave))
              history array containing densities in every cell at times
              tsave; not returned if tsave is None, and if the
              simulation aborts before reaching some times in tsave,
              the portions of the history array corresponding to times
              that the simulation did not reach will be filled with
              NaN's
           shifthist : array, shape(len(tsave))
              history array containing the number of cells by which the
              grid has been shifted left at each output time
        """

        # Allocate array to hold history
        if tsave is not None:
            ts = np.atleast_1d(np.array(tsave))
            nhist = ts.size
            if nhist > 1:
                if np.any(ts[1:] <= ts[:-1]):
                    raise ValueError("tsave must be strictly increasing")
            if np.any(ts > trun):
                raise ValueError("all elements of tsave must be <= trun")
            hist = np.zeros(self.nx*nhist, dtype=c_double)
            shifthist = np.zeros(nhist, dtype=c_size_t)
        else:
            nhist = 0
            ts = np.zeros(1)
            hist = np.zeros(1)
            shifthist = np.zeros(1, dtype=c_size_t)

        # Run simulation
        if dt0 is None: dt0 = -1.0
        dt = self.lib.c_simulation_1D_run(
            self.c_simulation, trun, nhist, ts, hist, shifthist, dt0)

        # Reshape and fill any emptry entries in the history output
        if nhist > 0:
            hist = np.transpose(hist.reshape(nhist,self.nx))
            if self.time < trun:
                nsave = np.sum(tsave <= self.time)
                hist[:,nsave:] = np.nan
                shifthist[nsave:] = -1

        # Return
        if nhist > 0:
            return dt, hist, shifthist
        else:
            return dt
        
    # Access to data and control parameters
    @property
    def nx(self):
        return self.dd.nx
    @nx.setter
    def nx(self, val):
        raise AttributeError("nx is read-only")
    @property
    def time(self):
        return self.lib.c_simulation_1D_time(self.c_simulation)
    @time.setter
    def time(self, val):
        self.lib.c_simulation_1D_set_time(self.c_simulation, val)
    @property
    def step(self):
        return self.lib.c_simulation_1D_step(self.c_simulation)
    @step.setter
    def step(self, val):
        self.lib.c_simulation_1D_set_step(self.c_simulation, val)
    @property
    def Theta(self):
        return self.lib.c_simulation_1D_Theta(self.c_simulation)
    @Theta.setter
    def Theta(self, val):
        self.lib.c_simulation_1D_set_Theta(self.c_simulation, val)
    @property
    def max_step(self):
        return self.lib.c_simulation_1D_max_step(self.c_simulation)
    @max_step.setter
    def max_step(self, val):
        self.lib.c_simulation_1D_set_max_step(self.c_simulation, val)
    @property
    def cfl(self):
        return self.lib.c_simulation_1D_cfl(self.c_simulation)
    @cfl.setter
    def cfl(self, val):
        self.lib.c_simulation_1D_set_cfl(self.c_simulation, val)
    @property
    def rho_floor(self):
        return self.lib.c_simulation_1D_rho_floor(self.c_simulation)
    @rho_floor.setter
    def rho_floor(self, val):
        self.lib.c_simulation_1D_set_rho_floor(self.c_simulation, val)

    # Access to internal data
    @property
    def ppm(self):
        return npct.as_array(self.lib.c_simulation_1D_get_ppm_coef(
            self.c_simulation), shape=(self.nx*3,))
    @ppm.setter
    def ppm(self, val):
        raise AttributeError("ppm is read-only")
    @property
    def tau(self):
        return npct.as_array(self.lib.c_simulation_1D_get_tau(
            self.c_simulation), shape=(self.nx,))
    @tau.setter
    def tau(self, val):
        raise AttributeError("tau is read-only")
    @property
    def flux(self):
        return npct.as_array(self.lib.c_simulation_1D_get_flux(
            self.c_simulation), shape=(self.nx+1,))
    @flux.setter
    def flux(self, val):
        raise AttributeError("flux is read-only")
    @property
    def verbosity(self):
        v = self.lib.c_simulation_1D_verbosity(self.c_simulation)
        if v == 0:
            return 'None'
        elif v == 1:
            return 'Medium'
        elif v == 2:
            return 'High'
        else:
            return 'Unknown'
    @verbosity.setter
    def verbosity(self, val):
        if val == 'None':
            v = 0
        elif val == 'Medium':
            v = 1
        elif val == 'High':
            v = 2
        else:
            v = val
        if v != 0 and v != 1 and v != 2:
            raise ValueError(
                "Valid values of verbosity are 'None', 'Medium',"
                "'High', 0, 1, or 2")
        self.lib.c_simulation_1D_set_verbosity(self.c_simulation, v)
