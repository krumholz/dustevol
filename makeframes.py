import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import os.path as osp
from glob import glob
from simulation import simulation
import argparse
import numpy as np
import astropy.units as u

# Parse arguments
parser = argparse.\
         ArgumentParser(description="script to make movie frames"
                        " from simulations")
parser.add_argument("target_dir", help="directory containing data to "
                    "be processed")
parser.add_argument("-o", "--output_dir", default=None,
                    help="name of directory into which to write "
                    "output; default is same as target_dir")
parser.add_argument("-p", "--paramfile", default="parameters.txt",
                    help="name of parameter file for run")
parser.add_argument("-n", "--name", default=None,
                    help="base name of output file to be processed; "
                    "default action is to read this from the specified "
                    "parameter file; if this option is set, the "
                    "parameter file is ignored")
parser.add_argument("-nf", "--nframe", default=None, type=int,
                    help="number of frames to generate")
parser.add_argument("-t", "--tmax", default=None, type=float,
                    help="maximum time of frames")
parser.add_argument("-c", "--checkrange", default=None, type=int, nargs=2,
                    help="range of checkpoint file numbers to process")
parser.add_argument("-sk", "--skip", default=None, type=int,
                    help="process only every N checkpoints")
parser.add_argument("-sz", "--size", default=(5,8), nargs=2, type=float,
                    help="image size in inches (same format as figsize)")
parser.add_argument("-rl", "--rlim", default=None, nargs=2, type=float,
                    help="axis limits in the radial direction")
parser.add_argument("-zl", "--zlim", default=None, nargs=2, type=float,
                    help="axis limits in the vertical direction")
parser.add_argument("-rhol", "--rholim", default=None, nargs=2, type=float,
                    help="lower and upper limit in log density in "
                    "color maps")
parser.add_argument("-cl", "--collim", default=None, nargs=2, type=float,
                    help="log column densty limits")
parser.add_argument("-lr", "--linear_r", default=False, action="store_true",
                    help="make radial axis linear instead of logarithmic")
parser.add_argument("-lz", "--linear_z", default=False, action="store_true",
                    help="make vertical axis coordinate z instead of "
                    "mu")
parser.add_argument("-ovr", "--overwrite", default=False,
                    action="store_true", help="overwrite existing "
                    "image files")
args = parser.parse_args()

# Define astrophysical units
yr = 365.25*24.*3600.
AU = 1.5e13
Msun = 1.99e33
Lsun = 3.83e33

# Get the base name of the run, either from the arguments or the
# parameter file
if args.name is not None:
    basename = args.name
else:
    basename = None
    fp = open(osp.join(args.target_dir, args.paramfile), "r")
    for p in fp:
        # Strip comments and whitespace
        line = p.split("#")[0].strip()
        # Skip empty lines
        if len(line) == 0: continue
        # Split by the = sign
        tokens = line.split("=")
        # Look for the name and tcheck keywords
        if tokens[0].strip() == "name":
            basename = tokens[1].strip()
        elif tokens[0].strip() == "tcheck":
            tcheck = float(tokens[1].strip())
    fp.close()
    if basename is None:
        raise ValueError("unable to find name keyword in {:s}".
                         format(args.paramfile))
    elif tcheck is None:
        raise ValueError("unable to find tcheck keyword in {:s}".
                         format(args.paramfile))

# Read list of output files, and assign a number and time to each
files = glob(osp.join(args.target_dir,
                      basename+'[0-9][0-9][0-9][0-9][0-9].npz'))
files.sort()
filenames = []
filenums = []
filetimes = []
for f in files:
    fnum = int(f[-9:-4])
    filenums.append(fnum)
    filetimes.append(fnum * tcheck)
filenums = np.array(filenums)
filetimes = np.array(filetimes)

# Drop files outside specified time or number range
if args.tmax is not None:
    files = [ft[0] for ft in zip(files, filetimes) if ft[1] <= args.tmax]
elif args.checkrange is not None:
    files = [fn[0] for fn in zip(files, filenums) if
             fn[1] >= args.checkrange[0] and fn[1] < args.checkrange[1]]
filenums = []
filetimes = []
for f in files:
    fnum = int(f[-9:-4])
    filenums.append(fnum)
    filetimes.append(fnum * tcheck)
filenums = np.array(filenums)
filetimes = np.array(filetimes)

# If generating only a limited number of frames, or processing only
# certain files, drop the ones we're not keeping
if args.skip > 1:
    files = files[::args.skip]
elif args.nframe is not None:
    skip_interval = float(filenums[-1]) / (args.nframe-1)
    idx_to_keep = list(
        np.array(np.round(np.arange(args.nframe) * skip_interval),
                          dtype='int'))
    files_to_keep = []
    for i in idx_to_keep:
        idx = np.argmin(np.abs(filenums-i))
        files_to_keep.append(files[idx])
    files = files_to_keep

# Set matplotlib options
plt.ioff()
plt.rc('text', usetex=True)
plt.rc('font', family='serif', size=12)

# Set output directory name
if args.output_dir is None:
    outdir = args.target_dir
else:
    outdir = args.output_dir

# Loop over files and make frames
for f in files:
    
    # Generate output file name
    outnum = f[-9:-4]
    outfile = osp.join(outdir, basename+'_frame'+outnum+'.png')

    # Skip if file already exists
    if osp.isfile(outfile) and not args.overwrite:
        continue

    # Print status
    print("Generating image "+outfile+" from snapshot "+f+"...")

    # Read data
    sim = simulation(chkname=osp.join(args.target_dir, basename),
                     chknum=int(outnum))

    # If we have any un-set limits, set them from the first file
    if args.rlim is None:
        args.rlim = np.array([sim.dd.r_e[0], sim.dd.r_e[-1]]) / \
                    (u.AU/u.cm).to('')
    if args.zlim is None:
        if args.linear_z:
            args.zlim = [0, int(np.round(
                sim.dd.r_e[-1]*sim.dd.mu_e[-1]/(u.AU/u.cm).to('')))]
        else:
            args.zlim = [0, sim.dd.mu_e[-1]]
    if args.rholim is None:
        logrhomin = np.floor(np.log10(sim.dd.rho_floor))
        logrhomax = np.ceil(np.log10(np.amax(sim.dd.rho_c)))
        args.rholim = [10.**logrhomin, 10.**logrhomax]
    else:
        logrhomin = np.log10(args.rholim[0])
        logrhomax = np.log10(args.rholim[1])
    if args.collim is None:
        p = sim.dd.proj()
        args.collim = np.array(
            [np.floor(np.log10(np.amin(p))),
             np.ceil(np.log10(np.amax(p)))])

    # Make plot
    plt.clf()
    sim.dd.make_plot(rlog=not args.linear_r,
                     mu=not args.linear_z,
                     logrhomin=logrhomin,
                     logrhomax=logrhomax,
                     rlim=args.rlim,
                     zlim=args.zlim,
                     collim=args.collim,
                     figsize=args.size,
                     title='$t = {:5.1f}$ kyr'.format(
                         sim.time/(u.kyr/u.s).to('')))

    # Write output
    plt.savefig(outfile)
    plt.close()
