import numpy as np
from simulation import simulation
from dustdisk import dustdisk
import os.path as osp
import glob
import argparse

# Define astrophysical units
yr = 365.25*24.*3600.
AU = 1.5e13
Msun = 1.99e33
Lsun = 3.83e33

# Read name of parameter file
parser = argparse.\
         ArgumentParser(description="dustevol simulation script")
parser.add_argument("paramfile", help="name of parameter file for run")
parser.add_argument("--norestart", default=False, action="store_true",
                    help="ignore checkpoint files; start a new run "
                    "rather than restarting an existing one")
args = parser.parse_args()

# Ingest parameter file and turn it into a dict of parameters
fp = open(args.paramfile)
paramfile = fp.read().splitlines()
fp.close()
params = {}
for p in paramfile:
    # Strip comments and whitespace
    line = p.split("#")[0].strip()
    # Skip empty lines
    if len(line) == 0: continue
    # Split by the = sign
    tokens = line.split("=")
    if len(tokens) != 2:
        raise ValueError("badly formatted parameter file line: "+p)
    # Store in dict
    params[tokens[0].strip()] = tokens[1].strip()
    
# Read a checkpoint if available; if not, start fresh
try:

    # If we been told to ignore checkpoints, raise an error here to
    # force start of new run
    if args.norestart:
        raise IOError
    
    # Read checkpoint
    sim = simulation(chkname=params["name"])

    print("Restarted from checkpoint {:d}".format(sim.chknum-1))
                       
except IOError:

    # Extract parameters from parameter file and convert to cgs units
    nr = int(params['nr'])
    nmu = int(params['nmu'])
    rmin = float(params['rmin']) * AU
    rmax = float(params['rmax']) * AU
    mumax = float(params['mumax'])
    M = float(params['M']) * Msun
    p = float(params['p'])
    q = float(params['q'])
    eps = float(params['eps'])
    T0 = float(params['T0'])
    alpha = float(params['alpha'])
    try:
        Mdot = float(params['Mdot']) * Msun / yr
    except KeyError:
        Mdot = 0.0
    L = float(params['L']) * Lsun
    rho_d = float(params['rho_d'])
    a = np.array([float(a_) for a_ in params["a"].split()])
    gtype = str(params['gtype'])
    f_dust = float(params['f_dust'])
    qd = float(params['qd'])

    # Create initial dust disk and set up simulation
    dd = dustdisk(nr, nmu, rmin, rmax, mumax, M, p, q, eps, T0, 
                  alpha, Mdot, L, rho_d, a, None, gtype=gtype,
                  f_dust=f_dust, qd=qd)
    sim = simulation(dd)

    # Write out checkpoint for starting state
    sim.checkpoint(chkname=params['name'])

# Set control variables
sim.dt_min = float(params["dtmin"]) * yr
sim.cfl = float(params["cfl"])
sim.max_step = int(params["max_step"])
sim.verbosity = params["verbosity"]
    
# Run simulation
while sim.time < float(params["tmax"])*yr and \
      sim.step < int(params["max_step"]):
    tnext = sim.time+float(params["tcheck"])*yr
    sim.run(tnext)
    print("Writing checkpoint {:d}".format(sim.chknum))
    sim.checkpoint(chkname=params["name"])
    if sim.time != tnext:
        break
