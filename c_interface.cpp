// This module defines c interfaces to the c++ classes, in order to
// allow python code to make use of them

#include "grid.H"
#include "grid_1D.H"
#include "gasdisk.H"
#include "dustdisk.H"
#include "dustdisk_1D.H"
#include "simulation.H"
#include "simulation_1D.H"
#include "utils.H"
#include <iostream>

// C interfaces to wrap the grid class and make it accessible to
// python
extern "C" {

  ////////////////////////////////////////////////////////////////////
  // grid class
  ////////////////////////////////////////////////////////////////////
  
  // Creation and destruction
  grid *c_grid_linear_alloc(const vecsz _nr, const vecsz _nmu,
			    const double _r_min, const double _r_max,
			    const double _mu_max) {
    return (grid *) new grid_linear(_nr, _nmu, _r_min, _r_max, _mu_max);
  }
  grid *c_grid_log_alloc(const vecsz _nr, const vecsz _nmu,
			 const double _r_min, const double _r_max,
			 const double _mu_max) {
    return (grid *) new grid_log(_nr, _nmu, _r_min, _r_max, _mu_max);
  }
  grid *c_grid_powerlaw_alloc(const vecsz _nr, const vecsz _nmu,
			      const double _r_min, const double _r_max,
			      const double _mu_max, const double _pidx) {
    return (grid *) new grid_powerlaw(_nr, _nmu, _r_min, _r_max, _mu_max,
				      _pidx);
  }
  void c_grid_free(grid *grd) { delete grd; }

  // Access to coordinateds and metrics
  double c_grid_r_e(const grid *grd, const vecsz i) { return grd->r_e_(i); }
  double c_grid_r_c(const grid *grd, const vecsz i) { return grd->r_c_(i); }
  double c_grid_x_e(const grid *grd, const vecsz i) { return grd->r_e_(i); }
  double c_grid_x_c(const grid *grd, const vecsz i) { return grd->r_c_(i); }
  double c_grid_mu_e(const grid *grd, const vecsz j) { return grd->r_e_(j); }
  double c_grid_mu_c(const grid *grd, const vecsz j) { return grd->r_c_(j); }
  double c_grid_xbar(const grid *grd, const vecsz i) { return grd->xbar_(i); }
  double c_grid_x2bar(const grid *grd, const vecsz i) { return grd->x2bar_(i); }
  double c_grid_mubar(const grid *grd, const vecsz j) { return grd->mubar_(j); }
  double c_grid_mu2bar(const grid *grd, const vecsz j) { return grd->mu2bar_(j); }
  double c_grid_dxdr_e(const grid *grd, const vecsz i) { return grd->dxdr_e_(i); }
  double c_grid_dxdr_c(const grid *grd, const vecsz i) { return grd->dxdr_c_(i); }
  double c_grid_V(const grid *grd, const vecsz i, const vecsz j) {
    return grd->V_(i,j); }
  double c_grid_A_r(const grid *grd, const vecsz i, const vecsz j) {
    return grd->A_r_(i,j); }
  double c_grid_A_mu(const grid *grd, const vecsz i, const vecsz j) {
    return grd->A_mu_(i,j); }
  
  // Access to data
  grid_type c_grid_gtype(const grid *grd) { return grd->gtype_(); }
  double c_grid_pidx(const grid_powerlaw *grd) { return grd->pidx_(); }
  vecsz c_grid_nr(const grid *grd) { return grd->nr_(); }
  vecsz c_grid_nmu(const grid *grd) { return grd->nmu_(); }
  double c_grid_dx(const grid *grd) { return grd->dx_(); }
  double c_grid_dmu(const grid *grd) { return grd->dmu_(); }

  // Read only-access to vectors; these functions return const
  // pointers to the data
  const double *c_grid_r_e_vec(const grid *grd) {
    return grd->r_e_().data(); }
  const double *c_grid_r_c_vec(const grid *grd) {
    return grd->r_c_().data(); }
  const double *c_grid_x_e_vec(const grid *grd) {
    return grd->x_e_().data(); }
  const double *c_grid_x_c_vec(const grid *grd) {
    return grd->x_c_().data(); }
  const double *c_grid_mu_e_vec(const grid *grd) {
    return grd->mu_e_().data(); }
  const double *c_grid_mu_c_vec(const grid *grd) {
    return grd->mu_c_().data(); }
  const double *c_grid_xbar_vec(const grid *grd) {
    return grd->xbar_().data(); }
  const double *c_grid_x2bar_vec(const grid *grd) {
    return grd->x2bar_().data(); }
  const double *c_grid_intx_vec(const grid *grd) {
    return grd->intx_().data(); }
  const double *c_grid_intx2_vec(const grid *grd) {
    return grd->intx2_().data(); }
  const double *c_grid_intx_c_vec(const grid *grd) {
    return grd->intx_c_().data(); }
  const double *c_grid_intx2_c_vec(const grid *grd) {
    return grd->intx2_c_().data(); }
  const double *c_grid_mubar_vec(const grid *grd) {
    return grd->mubar_().data(); }
  const double *c_grid_mu2bar_vec(const grid *grd) {
    return grd->mu2bar_().data(); }
  const double *c_grid_dxdr_e_vec(const grid *grd) {
    return grd->dxdr_e_().data(); }
  const double *c_grid_dxdr_c_vec(const grid *grd) {
    return grd->dxdr_c_().data(); }
  const double *c_grid_V_vec(const grid *grd) {
    return grd->V_().data(); }
  const double *c_grid_A_r_vec(const grid *grd) {
    return grd->A_r_().data(); }
  const double *c_grid_A_mu_vec(const grid *grd) {
    return grd->A_mu_().data(); }


  ////////////////////////////////////////////////////////////////////
  // gasdisk class
  ////////////////////////////////////////////////////////////////////
  
  // Creation and destruction
  gasdisk *c_gasdisk_alloc(const grid *_grd,
			   const double _M, const double _p,
			   const double _q, const double _eps,
			   const double _T0, const double _alpha,
			   const double _Mdot) {
    return (gasdisk *) new gasdisk(_grd, _M, _p, _q, _eps, _T0, _alpha,
				   _Mdot); }
  void c_gasdisk_free(gasdisk *gd) { delete gd; }

  // Access to grid point values
  double c_gasdisk_rhog_c(const gasdisk *gd, const vecsz i, const vecsz j) {
    return gd->rhog_c_(i,j); }
  double c_gasdisk_rhog_re_(const gasdisk *gd, const vecsz i, const vecsz j) {
    return gd->rhog_re_(i,j); }
  double c_gasdisk_rhog_mue_(const gasdisk *gd, const vecsz i, const vecsz j) {
    return gd->rhog_mue_(i,j); }
  double c_gasdisk_cs_re_(const gasdisk *gd, const vecsz i, const vecsz j) {
    return gd->cs_re_(i,j); }
  double c_gasdisk_cs_mue_(const gasdisk *gd, const vecsz i, const vecsz j) {
    return gd->cs_mue_(i,j); }
  double c_gasdisk_Omega_re_(const gasdisk *gd, const vecsz i, const vecsz j) {
    return gd->Omega_re_(i,j); }
  double c_gasdisk_Omega_mue_(const gasdisk *gd, const vecsz i, const vecsz j) {
    return gd->Omega_mue_(i,j); }
  double c_gasdisk_eta_re_(const gasdisk *gd, const vecsz i, const vecsz j) {
    return gd->eta_re_(i,j); }
  double c_gasdisk_eta_mue_(const gasdisk *gd, const vecsz i, const vecsz j) {
    return gd->eta_mue_(i,j); }
  double c_gasdisk_vg_re_(const gasdisk *gd, const vecsz i, const vecsz j) {
    return gd->vg_re_(i,j); }
  double c_gasdisk_vg_mue_(const gasdisk *gd, const vecsz i, const vecsz j) {
    return gd->vg_mue_(i,j); }

  // Access to private data
  double c_gasdisk_M(const gasdisk *gd) { return gd->M_(); }
  double c_gasdisk_p(const gasdisk *gd) { return gd->p_(); }
  double c_gasdisk_q(const gasdisk *gd) { return gd->q_(); }
  double c_gasdisk_eps(const gasdisk *gd) { return gd->eps_(); }
  double c_gasdisk_T0(const gasdisk *gd) { return gd->T0_(); }
  double c_gasdisk_alpha(const gasdisk *gd) { return gd->alpha_(); }
  double c_gasdisk_Mdot(const gasdisk *gd) { return gd->Mdot_(); }

  // Read-only access to vectors; these functions return const pointers
  // to the data
  const double *c_gasdisk_rhog_c_vec(const gasdisk *gd) {
    return gd->rhog_c_().data(); }
  const double *c_gasdisk_rhog_re_vec(const gasdisk *gd) {
    return gd->rhog_re_().data(); }
  const double *c_gasdisk_rhog_mue_vec(const gasdisk *gd) {
    return gd->rhog_mue_().data(); }
  const double *c_gasdisk_cs_re_vec(const gasdisk *gd) {
    return gd->cs_re_().data(); }
  const double *c_gasdisk_cs_mue_vec(const gasdisk *gd) {
    return gd->cs_mue_().data(); }
  const double *c_gasdisk_Omega_re_vec(const gasdisk *gd) {
    return gd->Omega_re_().data(); }
  const double *c_gasdisk_Omega_mue_vec(const gasdisk *gd) {
    return gd->Omega_mue_().data(); }
  const double *c_gasdisk_eta_re_vec(const gasdisk *gd) {
    return gd->eta_re_().data(); }
  const double *c_gasdisk_eta_mue_vec(const gasdisk *gd) {
    return gd->eta_mue_().data(); }
  const double *c_gasdisk_vg_re_vec(const gasdisk *gd) {
    return gd->vg_re_().data(); }
  const double *c_gasdisk_vg_mue_vec(const gasdisk *gd) {
    return gd->vg_mue_().data(); }

  // Access to the model function
  void c_gasdisk_model(const gasdisk *gd,
		       const double r, const double mu,
		       double *rho, double *cs, double *Omega,
		       double *eta, double *vg) {
    return gd->model(r, mu, *rho, *cs, *Omega, *eta, *vg);
  }
  
  ////////////////////////////////////////////////////////////////////
  // dustdisk class
  ////////////////////////////////////////////////////////////////////

  // Creation and destruction
  dustdisk *c_dustdisk_alloc(const gasdisk *_gd,
			     const double _L, const double _rho_d,
			     const double *_a, const double *_f_a,
			     const vecsz _na,
			     const double _rho_floor) {
    std::vector<double> a(_a, _a+_na);
    std::vector<double> f_a(_f_a, _f_a+_na);
    return (dustdisk *)
      new dustdisk(_gd, _L, _rho_d, a, f_a, _rho_floor);
  }
  dustdisk *c_dustdisk_copyinit(const double *_rho_c,
				const gasdisk *_gd,
				const double _L, const double _rho_d,
				const double *_a, const double *_f_a,
				const vecsz _na,
				const double _rho_floor) {
    std::vector<double> a(_a, _a+_na);
    std::vector<double> f_a(_f_a, _f_a+_na);
    Eigen::Map<const Eigen::VectorXd>
      rho_c_map(_rho_c, _gd->nr_()*_gd->nmu_()*_na);
    Eigen::VectorXd rho_c = rho_c_map;
    return (dustdisk *)
      new dustdisk(_gd, _L, _rho_d, a, f_a, _rho_floor, rho_c);
  }
  dustdisk *c_dustdisk_alloc_qd(const gasdisk *_gd,
				const double _L, const double _rho_d,
				const double *_a, const double fdust,
				const double qd, const vecsz _na,
				const double _rho_floor) {
    std::vector<double> a(_a, _a+_na);
    return (dustdisk *)
      new dustdisk(_gd, _L, _rho_d, a, fdust, qd, _rho_floor);
  }
  dustdisk *c_dustdisk_copyinit_qd(const double *_rho_c,
				   const gasdisk *_gd,
				   const double _L, const double _rho_d,
				   const double *_a, const double fdust,
				   const double qd, const vecsz _na,
				   const double _rho_floor) {
    std::vector<double> a(_a, _a+_na);
    Eigen::Map<const Eigen::VectorXd>
      rho_c_map(_rho_c, _gd->nr_()*_gd->nmu_()*_na);
    Eigen::VectorXd rho_c = rho_c_map;
    return (dustdisk *)
      new dustdisk(_gd, _L, _rho_d, a, fdust, qd, _rho_floor, rho_c);
  }  
  void c_dustdisk_free(dustdisk *dd) { delete dd; }

  // Access to scalar data
  vecsz c_dustdisk_na(const dustdisk *dd) { return dd->na_(); }
  double c_dustdisk_L(const dustdisk *dd) { return dd->L_(); }
  double c_dustdisk_rho_d(const dustdisk *dd) { return dd->rho_d_(); }
  double c_dustdisk_rho_floor(const dustdisk *dd) {
    return dd->rho_floor_(); }
  void c_dustdisk_set_floor(dustdisk *dd, double floor) {
    dd->set_floor(floor); }
  void c_dustdisk_apply_floor(dustdisk *dd) { dd->apply_floor(); }

  // Access to vector data
  const double *c_dustdisk_a(const dustdisk *dd) {
    return dd->a_().data(); }
  const double *c_dustdisk_f_a(const dustdisk *dd) {
    return dd->f_a_().data(); }
  const double *c_dustdisk_kappa_d(const dustdisk *dd) {
    return dd->kappa_d_().data(); }
  const double *c_dustdisk_beta_0(const dustdisk *dd) {
    return dd->beta_0_().data(); }

  // Access to eigen structures; note that this requires that the
  // caller pass in an array to be filled, which must point to enough
  // allocated memory to hold the result
  void c_dustdisk_rho_c(const dustdisk *dd, double *outbuf) {
    const Eigen::VectorXd& rho_c = dd->rho_c_();
    for (Eigen::Index i=0; i<rho_c.rows(); i++) outbuf[i] = rho_c[i];
  }

  // Convenience methods
  void c_dustdisk_proj(const dustdisk *dd, const vecsz nvarpi,
		       const double *varpi_, double *col_) {
    dvec varpi(nvarpi);
    for (vecsz n=0; n<nvarpi; n++) varpi[n] = varpi_[n];
    dvec col = dd->proj(varpi);
    for (vecsz n=0; n<nvarpi*dd->na_(); n++) col_[n] = col[n];
  }
  void c_compute_f_a(const dustdisk *dd, const double f_dust,
		     const double qd, double *f_a_) {
    dvec f_a = dd->compute_f_a(f_dust, qd);
    for (vecsz i=0; i<f_a.size(); i++) f_a_[i] = f_a[i];
  }
  
  
  ////////////////////////////////////////////////////////////////////
  // simulation class
  ////////////////////////////////////////////////////////////////////

  // Creation and destruction
  simulation *c_simulation_alloc(dustdisk *_dd) {
    return (simulation *) new simulation(*_dd); }
  void c_simulation_free(simulation *s) { delete s; }

  // Access to control parameters
  double c_simulation_tol(const simulation *s) { return s->tol_(); }
  double c_simulation_mat_tol(const simulation *s) { return s->mat_tol_(); }
  double c_simulation_max_dt_fac(const simulation *s) {
    return s->max_dt_fac_(); }
  double c_simulation_dt_min(const simulation *s) { return s->dt_min_(); }
  double c_simulation_Theta(const simulation *s) { return s->Theta_(); }
  double c_simulation_drho_max(const simulation *s) { return s->drho_max_(); }
  double c_simulation_cfl(const simulation *s) { return s->cfl_(); }
  vecsz c_simulation_max_iter(const simulation *s) { return s->max_iter_(); }
  vecsz c_simulation_max_step(const simulation *s) { return s->max_step_(); }
  verbosity c_simulation_verbosity(const simulation *s) {
    return s->verbosity_(); }
  bool c_simulation_r_flux_off(const simulation *s) {
    return s->r_flux_off_(); }
  vecsz c_simulation_anderson_ord(const simulation *s) {
    return s->anderson_ord_(); }
  advance_meth c_simulation_method(const simulation *s) {
    return s->method(); }

  // Setting control parameters
  void c_simulation_set_tol(simulation *s, const double tol) {
    s->set_tol(tol); }
  void c_simulation_set_mat_tol(simulation *s, const double tol) {
    s->set_mat_tol(tol); }
  void c_simulation_set_max_dt_fac(simulation *s, const double dt_fac) {
    s->set_max_dt_fac(dt_fac); }
  void c_simulation_set_dt_min(simulation *s, const double dt_min) {
    s->set_dt_min(dt_min); }
  void c_simulation_set_Theta(simulation *s, const double Theta) {
    s->set_Theta(Theta); }
  void c_simulation_set_drho_max(simulation *s, const double drho_max) {
    s->set_drho_max(drho_max); }
  void c_simulation_set_cfl(simulation *s, const double cfl) {
    s->set_clf(cfl); }
  void c_simulation_set_max_iter(simulation *s, const vecsz max_iter) {
    s->set_max_iter(max_iter); }
  void c_simulation_set_max_step(simulation *s, const vecsz max_step) {
    s->set_max_step(max_step); }
  void c_simulation_set_verbosity(simulation *s, const verbosity verb) {
    s->set_verbosity(verb); }
  void c_simulation_set_r_flux_off(simulation *s, const bool setting) {
    s->set_r_flux_off(setting); }
  void c_simulation_set_anderson_ord(simulation *s, const vecsz ord) {
    s->set_anderson_ord(ord); }
  void c_simulation_set_method(simulation *s, const advance_meth meth) {
    s->set_method(meth); }

  // Methods that are part of a time step
  void c_simulation_set_ppm_coef(simulation *s) { s->set_ppm_coef(); }
  void c_simulation_set_tau(simulation *s) { s->set_tau(); }
  double c_simulation_set_flux(simulation *s, const double dt,
			       const flux_mode mode) {
    return s->set_flux(dt, mode); }
  void c_simulation_set_rhs_old(simulation *s) { s->set_rhs_old(); }
  void c_simulation_set_rhs(simulation *s) { s->set_rhs(); }
  double c_simulation_advance_FIM(simulation *s, const double dt) {
    return s->advance_FIM(dt); }
  bool c_simulation_advance_diffusion(simulation *s, const double dt) {
    return s->advance_diffusion(dt); }
  double c_simulation_advance_advection(simulation *s, const double dt) {
    return s->advance_advection(dt); }
  double c_simulation_advance_IMEX(simulation *s, const double dt) {
    return s->advance_IMEX(dt); }

  // Main time step procedure; note that in C the caller has to pass
  // in a buffer to a block of memory that is large enough to hold the
  // full requested history. The history is transcribed into this
  // buffer from the C++ vector of Eigen vectors that holds it in the
  // C++ code.
  double c_simulation_run(simulation *s, const double tstop,
			  const vecsz nsave, const double *tsave_, 
			  double *hist_, const double dt0) {
    dvec tsave(tsave_, tsave_+nsave);
    vvec hist;
    double dtfin = s->run(tstop, tsave, hist, dt0);
    vecsz hsize = s->nr_()*s->nmu_()*s->na_();
    for (vecsz i=0; i<hist.size(); i++)
      for (vecsz j=0; j<hsize; j++) hist_[i*hsize+j] = hist[i][j];
    return dtfin;
  }

  // Access to the clock and step counter
  double c_simulation_time(simulation *s) { return s->time(); }
  void c_simulation_set_time(simulation *s, const double t) {
    s->set_time(t); }
  vecsz c_simulation_step(simulation *s) { return s->step(); }
  void c_simulation_set_step(simulation *s, const vecsz step) {
    s->set_step(step); }

  // Access to dimensions
  vecsz c_simulation_nr(const simulation *s) { return s->nr_(); }
  vecsz c_simulation_nmu(const simulation *s) { return s->nmu_(); }
  vecsz c_simulation_na(const simulation *s) { return s->na_(); }

  // Access to vector data
  const double *c_simulation_ppm_coef_r(const simulation *s) {
    return s->ppm_coef_r_().data(); }
  const double *c_simulation_ppm_coef_mu(const simulation *s) {
    return s->ppm_coef_mu_().data(); }
  const double *c_simulation_tau_re(const simulation *s) {
    return s->tau_re_().data(); }
  const double *c_simulation_tau_mue(const simulation *s) {
    return s->tau_mue_().data(); }
  const double *c_simulation_flux_re(const simulation *s) {
    return s->flux_re_().data(); }
  const double *c_simulation_flux_mue(const simulation *s) {
    return s->flux_mue_().data(); }

  // Access to eigen structures; note that this requires that the
  // caller pass in an array to be filled, which must point to enough
  // allocated memory to hold the result
  void c_simulation_rhs_old(const simulation *s, double *outbuf) {
    const Eigen::VectorXd& rhs_old = s->rhs_old_();
    for (Eigen::Index i=0; i<rhs_old.rows(); i++) outbuf[i] = rhs_old[i];
  }
  void c_simulation_rhs(const simulation *s, double *outbuf) {
    const Eigen::VectorXd& rhs = s->rhs_();
    for (Eigen::Index i=0; i<rhs.rows(); i++) outbuf[i] = rhs[i];
  }

  // Utility routines to aid in analysis; user must provide a buffer
  // large enough to hold output
  void c_simulation_vr(simulation *s, double *outbuf) {
    dvec vr;
    s->vr_(vr);
    memcpy(outbuf, vr.data(), vr.size());
  }
  void c_simulation_vmu(simulation *s, double *outbuf) {
    dvec vmu;
    s->vmu_(vmu);
    memcpy(outbuf, vmu.data(), vmu.size());
  }

  ////////////////////////////////////////////////////////////////////
  // 1D grids
  ////////////////////////////////////////////////////////////////////
  grid_1D *c_grid_1D_alloc(const vecsz _nx, const double _xmin,
			   const double _xmax) {
    return (grid_1D *) new grid_1D(_nx, _xmax, _xmin); }
  void c_grid_1D_free(grid_1D *grd) { delete grd; }
  vecsz c_grid_1D_nx(const grid_1D *grd) { return grd->nx_(); }
  const double *c_grid_1D_xe_vec(const grid_1D *grd) {
    return grd->x_e_().data(); }
  const double *c_grid_1D_xc_vec(const grid_1D *grd) {
    return grd->x_c_().data(); }

  ////////////////////////////////////////////////////////////////////
  // 1D disks
  ////////////////////////////////////////////////////////////////////
  dustdisk_1D *c_dustdisk_1D_alloc(const grid_1D *_grd,
				   const double _chi) {
    return (dustdisk_1D *) new dustdisk_1D(_grd, _chi); }
  dustdisk_1D *c_dustdisk_1D_copyinit(const  grid_1D *_grd,
				      const double _chi,
				      const double *_rho_c) {
    dvec rho_c;
    rho_c.assign(_rho_c, _rho_c + _grd->nx_());
    return (dustdisk_1D *) new dustdisk_1D(_grd, _chi, rho_c);
  }
  void c_dustdisk_1D_free(dustdisk_1D *dd) { delete dd; }
  double c_dustdisk_1D_chi(dustdisk_1D *dd) { return dd->chi_(); }
  vecsz c_dustdisk_1D_shift(dustdisk_1D *dd) { return dd->shift_(); }
  const double *c_dustdisk_1D_rho_c(dustdisk_1D *dd) {
    return dd->rho_c_().data(); }
  void c_dustdisk_1D_do_shift(dustdisk_1D *dd, const double rho_floor) {
    dd->do_shift(rho_floor); }
  
  ////////////////////////////////////////////////////////////////////
  // 1D simulations
  ////////////////////////////////////////////////////////////////////

  // Allocation and de-allocation
  simulation_1D *c_simulation_1D_alloc(dustdisk_1D *_dd) {
    return (simulation_1D *) new simulation_1D(*_dd); }
  void c_simulation_1D_free(simulation_1D *_sim) { delete _sim; }

  // Procedures that are part of a time step
  void c_simulation_1D_set_ppm_coef(simulation_1D *_sim) {
    _sim->set_ppm_coef(); }
  void c_simulation_1D_set_tau(simulation_1D *_sim) {
    _sim->set_tau(); }
  void c_simulation_1D_set_flux(simulation_1D *_sim, const double dt) {
    _sim->set_flux(dt); }
  const double *c_simulation_1D_get_ppm_coef(simulation_1D *_sim) {
    return _sim->ppm_().data(); }
  const double *c_simulation_1D_get_tau(simulation_1D *_sim) {
    return _sim->tau_().data(); }
  const double *c_simulation_1D_get_flux(simulation_1D *_sim) {
    return _sim->flux_().data(); }
  void c_simulation_1D_advance_diffusion(simulation_1D *_sim,
					 const double dt) {
    _sim->advance_diffusion(dt); }
  double c_simulation_1D_advance_advection(simulation_1D *_sim,
					   const double dt) {
    return _sim->advance_advection(dt); }

  // Advance and run routines
  double c_simulation_1D_advance(simulation_1D *_sim, const double dt) {
    return _sim->advance(dt); }
  double c_simulation_1D_run(simulation_1D *s, const double tstop,
			     const vecsz nsave, const double *tsave_, 
			     double *hist_, vecsz *shifthist_,
			     const double dt0) {
    dvec tsave(tsave_, tsave_+nsave);
    ddvec hist;
    ivec shifthist;
    double dtfin = s->run(tstop, tsave, hist, shifthist, dt0);
    vecsz nx = s->dd_()->grd_()->nx_();
    for (vecsz i=0; i<hist.size(); i++) {
      for (vecsz j=0; j<nx; j++) hist_[i*nx+j] = hist[i][j];
      shifthist_[i] = shifthist[i];
    }
    return dtfin;
  }
  
  // Control parameters and time stepping
  double c_simulation_1D_Theta(simulation_1D *_sim) { return _sim->Theta_(); }
  vecsz c_simulation_1D_max_step(simulation_1D *_sim) {
    return _sim->max_step_(); }
  double c_simulation_1D_cfl(simulation_1D *_sim) { return _sim->cfl_(); }
  double c_simulation_1D_rho_floor(simulation_1D *_sim) {
      return _sim->rho_floor_(); }
  double c_simulation_1D_time(simulation_1D *_sim) { return _sim->time(); }
  vecsz c_simulation_1D_step(simulation_1D *_sim) { return _sim->step(); }
  verbosity c_simulation_1D_verbosity(const simulation_1D *s) {
    return s->verbosity_(); }
  void c_simulation_1D_set_Theta(simulation_1D *_sim, const double Theta) {
    _sim->set_Theta(Theta); }
  void c_simulation_1D_set_max_step(simulation_1D *_sim,
				    const vecsz max_step) {
    _sim->set_max_step(max_step); }
  void c_simulation_1D_set_cfl(simulation_1D *_sim, const double cfl) {
    _sim->set_cfl(cfl); }
  void c_simulation_1D_set_rho_floor(simulation_1D *_sim,
				     const double rho_floor) {
    _sim->set_rho_floor(rho_floor); }
  void c_simulation_1D_set_time(simulation_1D *_sim, const double t) {
    _sim->set_time(t); }
  void c_simulation_1D_set_step(simulation_1D *_sim, const vecsz ctr) {
    _sim->set_step(ctr); }
  void c_simulation_1D_set_verbosity(simulation_1D *s, const verbosity verb) {
    s->set_verbosity(verb); }
}
