// This module implements the low-level PPM reconstruction method

#include "ppm.H"

// A little utility routine; this just implement the solution to the
// simple 3 x 3 matrix equation:
// [ 1   a12   a13 ] [ x1 ]   [ b1 ]
// [ 1   a22   a23 ] [ x2 ] = [ b2 ]
// [ 1   a32   a33 ] [ x3 ]   [ b3 ]
static inline
void mat3sol(double a12, double a13, double a22, double a23,
	     double a32, double a33, double b1, double b2,
	     double b3, double x[3]) {
  x[0] = (a23*a32*b1 - a22*a33*b1 - a13*a32*b2 + a12*a33*b2 +
	  a13*a22*b3 - a12*a23*b3) /
    (a13*a22 - a12*a23 - a13*a32 + a23*a32 + a12*a33 - a22*a33);
  x[1] = (a33*(b1 - b2) + a13*(b2 - b3) + a23*(-b1 + b3)) /
    (a13*(a22 - a32) + a23*a32 - a22*a33 + a12*(-a23 + a33));
  x[2] = (a22*b1 - a32*b1 - a12*b2 + a32*b2 + a12*b3 - a22*b3) /
    (a13*a22 - a12*a23 - a13*a32 + a23*a32 + a12*a33 - a22*a33);
}

// This routine computes PPM coefficients. Inputs and outputs are as
// follows:
// x[3]   : coordinate positions x_{i-1}, x_i, x_{i+1}
// xb[3]  : volume averages of coordinates in cells i-1, i,
//          and i+1, i.e., <x>_{i-1>, <x>_i, and <x>_{i+1}
// x2b[3] : volume averages of the square of the coordinates
//          in cells i-1, i, and i+1, i.e., <x^2>_{i-1},
//          <x^2>_i, and <x^2>_{i+1}
// q[3]   : mean values of the quantity to be reconstructed
//          in cells i-1, i, and i+1
// c[3]   : pointer to an array of 3 elements that will
//          hold the reconstruction coefficients
void get_ppm_coef(const double x[3], const double xb[3],
		  const double x2b[3], const double q[3],
		  double c[3]) {
  
  // If the input q value is an extremum, just do a piecewise constant
  // reconstruction
  if ((q[2]-q[1]) * (q[1]-q[0]) < 0) {
    c[0] = q[1];
    c[1] = 0.0;
    c[2] = 0.0;
    return;
  }

  // Do reconstruction without limiting; this is the solution to
  // Skinner+'s equation 15
  mat3sol(xb[0], x2b[0], xb[1], x2b[1], xb[2], x2b[2],
	  q[0], q[1], q[2], c);

  // If the reconstruction we got back has zero parabolic component
  //  and is just linear, then we're done
  if (c[2] == 0.0) return;
  
  // Apply first slope-limiting step: check if the parabola we have
  // reconstructed has an extremum in either the left interval
  // [x_{i-1}, x_i] or the right interval [x_i, x_{i+1}]. If either is
  // the case, we need to limit the slope.
  double x_ext = -c[1] / (2.0*c[2] + constants::min);
  if (x_ext <= x[0] || x_ext >= x[2]) return;

  // If we're here, there is an extremum between x_{i-1} and x_{i+1},
  // so we need to slope limit; we do this by setting the value on the
  // left or right face (whichever side the extremum is on) equal to
  // the value that matches that of a parabola that has values equal
  // to the cell means at x_i and x_{i+-1}, and has zero derivative at
  // x_{i+-1}, then finding a new set of coefficients constrained to
  // produce that value at the cell edge.
  double xL = (x[0] + x[1]) / 2;    // Location of left face
  double xR = (x[1] + x[2]) / 2;    // Location of right face
  double qL, qR;                    // Left and right face values
  if (x_ext < x[1]) {
    // Need to reset the left face value; leave the right face value
    // as what it was in the original reconstruction
    qL = (3*q[0] + q[1]) / 4;
    qR = c[0] + c[1]*xR + c[2]*xR*xR;
  } else {
    // Need to reset the right face value, leaving the left face value
    // unchanged
    qL = c[0] + c[1]*xL + c[2]*xL*xL;
    qR = (3*q[2] + q[1]) / 4;
  }

  // Calculate a new parabolic fit from the new constraint equations;
  // this is Skinner+ 2018's equation 16
  mat3sol(xL, SQR(xL), xb[1], x2b[1], xR, SQR(xR), qL, q[1], qR, c);

  // Last slope-limiting step: check if our new reconstruction has an
  // extremum in the interval [x_{i-1/2}, x_{i+1/2}]. If so, we need
  // to limit again.
  x_ext = -c[1] / (2.0*c[2] + constants::min);
  if (x_ext <= xL || x_ext >= xR) return;

  // If we're here, we need to apply the final slope limiting step: we
  // find a new and final constraint by requiring that (1) the volume
  // average of the reconstruction in cell i continue to match the input
  // value, (2) the derivative of the reconstruction at face
  // x_{i+-1/2} is zero (with + applying if the extremum was at x_ext
  // > x_i, and the - applying if the opposite), and (3) the value of
  // the reconstruction at whichever face is having the derivative set
  // to zero be left unchanged. The formulae below result form solving
  // the linear system imposed by these constraints.
  if (x_ext < x[1]) {

    // Extremum is on the left side of the cell, so force the
    // derivative at the left cell face to be zero
    double denom = x2b[1] - 2*xb[1]*xL + SQR(xL);
    c[2] = (q[1] - qL) / denom;
    c[1] = -2 * xL * c[2];
    c[0] = (qL*x2b[1] - 2*qL*xb[1]*xL + q[1]*SQR(xL)) / denom;
    
  } else {

    // Extremum is on the right side of the cell, so force the
    // derivative at the right cell face to be zero
    double denom = x2b[1] - 2*xb[1]*xR + SQR(xR);
    c[2] = (q[1] - qR) / denom;
    c[1] = -2 * xR * c[2];
    c[0] = (qR*x2b[1] - 2*qR*xb[1]*xR + q[1]*SQR(xR)) / denom;
    
  }    
  
  return;
}
