// Implementation of the grid class

// Includes and namespaces
#include <cmath>
#include <iostream>
#include "grid.H"

using namespace std;

// Constructor for the grid class
grid::grid(const vecsz _nr, const vecsz _nmu, const double _rmin,
	   const double _rmax, const double _mumax,
	   const grid_type _gtype) :
  gtype(_gtype), nr(_nr), nmu(_nmu), rmin(_rmin), rmax(_rmax),
  mumax(_mumax) {

  // Set vectors to required size
  init_vectors();
}      
 
// Constructors for sub-classes of grid
grid_linear::grid_linear(const vecsz _nr, const vecsz _nmu,
			 const double _rmin, const double _rmax,
			 const double _mumax) :
  grid(_nr, _nmu, _rmin, _rmax, _mumax, GRID_LINEAR) {
  compute_coords();
}
grid_log::grid_log(const vecsz _nr, const vecsz _nmu,
		   const double _rmin, const double _rmax,
		   const double _mumax) :
  grid(_nr, _nmu, _rmin, _rmax, _mumax, GRID_LOG) {
  compute_coords();
}
grid_powerlaw::grid_powerlaw(const vecsz _nr, const vecsz _nmu,
			     const double _rmin, const double _rmax,
			     const double _mumax, const double _pidx) :
  grid(_nr, _nmu, _rmin, _rmax, _mumax, GRID_POWERLAW),
  pidx(_pidx) {
  compute_coords();
}


// Function to initialize vector sizes
void grid::init_vectors() {
  r_e.resize(nr+1);
  r_c.resize(nr);
  x_e.resize(nr+1);
  x_c.resize(nr);
  mu_e.resize(nmu+1);
  mu_c.resize(nmu);
  xbar.resize(nr);
  x2bar.resize(nr);
  intx.resize(nr);
  intx2.resize(nr);
  intx_c.resize(nr);
  intx2_c.resize(nr);
  mubar.resize(nmu);
  mu2bar.resize(nmu);
  dxdr_e.resize(nr+1);
  dxdr_c.resize(nr);
  V.resize(nr*nmu);
  A_r.resize((nr+1)*nmu);
  A_mu.resize(nr*(nmu+1));
}


// Function to initialize all coordinate quantities
void grid::compute_coords() {

  // Set grid spacing
  double xmin = r2x(rmin);
  double xmax = r2x(rmax);
  dx = (xmax - xmin) / nr;
  dmu = mumax / nmu;

  // Set cell center and edge locations
  for (vecsz i=0; i<=nr; i++) x_e[i] = xmin + i*dx;
  for (vecsz i=0; i<nr; i++) x_c[i] = xmin + (i+0.5)*dx;
  for (vecsz j=0; j<=nmu; j++) mu_e[j] = j * dmu;
  for (vecsz j=0; j<nmu; j++) mu_c[j] = (j+0.5) * dmu;

  // Compute mean mu coordinates
  for (vecsz j=0; j<nmu; j++) {
    mubar[j] = muV(mu_e[j], dmu);
    mu2bar[j] = mu2V(mu_e[j], dmu);
  }
  
  // Initialize cell radial positions
  for (vecsz i=0; i<=nr; i++) r_e[i] = x2r(x_e[i]);
  for (vecsz i=0; i<nr; i++) r_c[i] = x2r(x_c[i]);
  
  // Initialize pre-computed cell-center quantities
  for (vecsz i=0; i<nr; i++) {
    double r0 = r_e[i];
    double dr = r_e[i+1] - r_e[i];
    xbar[i] = xV(r0, dr);
    x2bar[i] = x2V(r0, dr);
    intx[i] = dr * xL(r0, dr);
    intx2[i] = dr * x2L(r0, dr);
    dr = r_c[i] - r_e[i];
    intx_c[i] = dr * xL(r0, dr);
    intx2_c[i] = dr * x2L(r0, dr);
    dxdr_c[i] = dxdr(r_c[i]);
  }

  // Initialize pre-computed cell-edge quantities
  for (vecsz i=0; i<=nr; i++) {
    double r0 = r_e[i];
    dxdr_e[i] = dxdr(r0);
  }

  // Initialize volumes and areas
#if defined(_OPENMP)
#  pragma omp parallel for
#endif
  for (vecsz j=0; j<nmu; j++) {
    
    // Volumes
    for (vecsz i=0; i<nr; i++) {
      V[IC(i,j,nr)] = (2.0/3.0) * M_PI *
	(CUBE(r_e[i+1]) - CUBE(r_e[i])) *
	(mu_e[j+1] - mu_e[j]);
    }
      
    // Radial face areas
    for (vecsz i=0; i<=nr; i++) {
      A_r[IERM(i,j,nr)] = 2.0 * M_PI * SQR(r_e[i]) *
	(mu_e[j+1] - mu_e[j]);
    }
  }

#if defined(_OPENMP)
#  pragma omp parallel for
#endif
  for (vecsz j=0; j<=nmu; j++) {

    // Angular face areas
    for (vecsz i=0; i<nr; i++) {
      A_mu[IEMUM(i,j,nr)] = M_PI * (SQR(r_e[i+1]) - SQR(r_e[i]));
    }    
  }

  // Minimum cell size
  dmin = dmu*r_c[0];
  for (vecsz i=0; i<nr; i++) dmin = min(dmin, r_e[i+1]-r_e[i]);
}
